package com.app.notesapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Abdul Ghani on 5/16/2017.
 */

public class StoragePreference {

    private static String MY_PREFERENCE_NAME = "cabigate_preference";
    public static String notAvailable = "N/A";
    private static String USER_ID = "userid";
    private static String Font_Size="fontsize";

    public static void setUserId(Context context, String userId) {
        SharedPreferences sharedPreference = context.getSharedPreferences(MY_PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(USER_ID, userId);
        editor.apply();
    }

    public static String getUserId(Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFERENCE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_ID, notAvailable);
    }


}
