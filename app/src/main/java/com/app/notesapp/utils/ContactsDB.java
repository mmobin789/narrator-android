package com.app.notesapp.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.app.notesapp.interfaces.ContactsDBListner;
import com.app.notesapp.models.SyncContact;

import java.util.ArrayList;

/**
 * Created by Abdul Ghani on 8/24/2017.
 */

public class ContactsDB extends SQLiteOpenHelper {

    private static final String DBName = "contactsDB";
    private static final int DBVersion = 1;
    private static final String CONTACT = "contact";
    private static final String NAME = "username";
    private static final String NUMBER = "number";
    private static final String STATUS = "status";
    private static final String EMAIL = "email";


    private static ContactsDBListner dbListner = null;

    public ContactsDB(Context context) {
        super(context, DBName, null, DBVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
      db.execSQL("CREATE TABLE IF NOT EXISTS " + CONTACT + "(" + NAME + " TEXT," + NUMBER + " TEXT,"+ STATUS + " INTEGER,"
                + EMAIL + " TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void writeContacts(ArrayList<SyncContact> syncContacts) {
        SQLiteDatabase db = getWritableDatabase();
        for (SyncContact syncContact : syncContacts) {
            ContentValues cv = new ContentValues();
            cv.put(NAME, syncContact.getName());
            cv.put(NUMBER, syncContact.getNumber());
            cv.put(STATUS, syncContact.getStatus());
            cv.put(EMAIL, syncContact.getEmail());
            db.insert(CONTACT, null, cv);
        }
        db.close();
        if(ContactsDB.dbListner!=null){
            ContactsDB.dbListner.onDataPopulated(getContacts());
        }
        Log.i("working123"," "+syncContacts.size());
        for(int i=0;i<syncContacts.size();i++)
        {
            Log.i("appname",""+syncContacts.get(i).getName());
            Log.i("appemail",""+syncContacts.get(i).getEmail());
            Log.i("appnumber",""+syncContacts.get(i).getNumber());
            Log.i("appstatus",""+syncContacts.get(i).getStatus());
        }
    }

    public ArrayList<SyncContact> getContacts() {
        ArrayList<SyncContact> syncContacts = null;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(CONTACT, null, null, null, null, null, STATUS + " DESC"+" , "+NAME+" ASC");
        if (cursor.getCount() > 0) {
            syncContacts = new ArrayList<>();
            while (cursor.moveToNext()) {
                syncContacts.add(new SyncContact(cursor.getString(0), cursor.getString(1), cursor.getInt(2),cursor.getString(3)));
            }
            Log.i("working1234"," "+syncContacts.size());
        }
        cursor.close();


        return syncContacts;
    }

    private void deleteContacts() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(CONTACT, null, null);
    }

    public void setContacts(ArrayList<SyncContact> contacts) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        if (DatabaseUtils.queryNumEntries(sqLiteDatabase, CONTACT) > 0) {
            deleteContacts();
        }
        sqLiteDatabase.close();
        writeContacts(contacts);
    }
    public static void setListner(ContactsDBListner dbListner){
        ContactsDB.dbListner = dbListner;
    }
    public static void removeListner(){
        ContactsDB.dbListner = null;
    }
}
