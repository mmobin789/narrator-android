package com.app.notesapp.utils;

import android.app.Dialog;

import java.util.TimerTask;

/**
 * Created by Danish on 7/31/2017.
 */

public class DialogBoxTimer extends TimerTask {
private Dialog dialog;

    public DialogBoxTimer(Dialog dialog) {
        this.dialog = dialog;
    }

    @Override
    public void run() {

        if(dialog.isShowing())
        {
            dialog.dismiss();
        }
    }
}
