package com.app.notesapp.utils;

import com.app.notesapp.vo.SubjectsDataVO;

import java.util.ArrayList;
import java.util.HashMap;

public class StaticData {

    public static String LOGIN = "LOGIN";
    public static String ID = "ID";
    public static ArrayList<String> words = new ArrayList<>();
    public static String ADDRESS = "";
    public static String GEO_LAT = "";
    public static String GEO_LONG = "";

    public  static StringBuilder builder=new StringBuilder();
    public static boolean colorCheck=false;
    public static int viewPosition=0;
    public static int list_ID=0;

    public static int DeleteCheck=0;
    public static int MicCheck=0;

    public static boolean timecheck2=true;

    public static int AdapterMicCheck=0;

    public static String Token="";

    public static String ALL_LISTS = "ALL_LISTS";
    public static String ALL_LISTS_DATA = "ALL_LISTS_DATA";
    public static String SERVER_URL = "http://rockymountainag-app.com/rockymountainag_APIs/Main_Call_API.php";
     public static String key_uid="id";
    public static String key_name = "name";
    public static String id = "";
    public static String name = "";
    public static String USERID = "";
    public static String EQUIPMENTID = "";

    public static String dob="";
    public static boolean crossOffCheck=false;

    public static class REQUESTRESPONSE_KEYS {

        public static final String ERROR = "error";
        public static final String MESSAGE = "description";
        public static final String DATA = "data";
        public static final String VERIFICATIONCODE = "verificationCode";

    }

    public static final class USER_TABLE_KEYS {

        public static String USER_ID = "user_id";
        public static String USER_NAME = "user_name";
        public static String USER_EMAIL = "user_email";
        public static String USER_PASSWORD = "user_password";
        public static String USER_LOGIN_TYPE = "user_login_type";
    }


    public static final class USER_DATA {

        public static String USER_ID = "USER_ID";
        public static String USER_NAME = "USER_NAME";
        public static String USER_EMAIL = "USER_EMAIL";
        public static String USER_NUMBER = "USER_NUMBER";
        public static String USER_LOGIN_STATUS = "USER_LOGIN_STATUS";
        public static String USER_PASSWORD = "USER_PASSWORD";
    }

    public static final class SERVER_KEYS {

        public static String ERROR = "error";
        public static String DESCRIPTION = "description";
    }

    public static final class SAVED_LETTERS_DATA {

        public static HashMap<String, SubjectsDataVO> SAVED_WORDES;
    }
}
