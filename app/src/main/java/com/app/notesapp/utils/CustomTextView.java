package com.app.notesapp.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.TextView;

import com.app.noteapp.R;


/**
 * Created by ehsan on 20-Jul-17.
 */

public class CustomTextView extends android.support.v7.widget.AppCompatTextView{


    String color = "";
    Paint paint;

    int colorCode = 0;
    Boolean isCrossingLine;

    public void setTextCrossLine(Boolean enabled) {
        isCrossingLine = enabled;

    }


    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
        init(attrs);
    }

    private void init(AttributeSet attributeSet) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attributeSet, R.styleable.customTextView);
        setTextCrossLine(typedArray.getBoolean(R.styleable.customTextView_textCrossLine, false));
        if (isCrossingLine)
            setStrikeThroughColor(typedArray.getColor(R.styleable.customTextView_setTextCrossLineColor, Color.BLACK));


        typedArray.recycle();
    }

    public void setStrikeThroughColor(int strikeThroughColor) {
        this.colorCode = strikeThroughColor;
        paint.setColor(colorCode);

    }

    public void setStrikeThroughColor(String strikeThroughColor) {

        this.color = strikeThroughColor;

        paint.setColor(Color.parseColor(strikeThroughColor));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(isCrossingLine) {
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrikeThruText(true);
            paint.setStrokeWidth(getResources().getDisplayMetrics().density);
            paint.setFlags(Paint.ANTI_ALIAS_FLAG);
            canvas.drawLine(0, getHeight() / 2, getWidth(), getHeight() / 2, paint);
        }

        super.onDraw(canvas);

    }

    public static void TextSize(TextView view)
    {

    }

}