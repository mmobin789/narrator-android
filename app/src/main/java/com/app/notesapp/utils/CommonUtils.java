package com.app.notesapp.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.noteapp.R;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by Haris Azam on 8/31/2015.
 */
public class CommonUtils {

    private static Context context;

    public static CommonUtils getInstance() {
        CommonUtils utils = null;
        if (utils == null) {
            utils = new CommonUtils();
        }
        return utils;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public final boolean isInternetAvailable() {
        ConnectivityManager connectivityManager;
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        // ARE WE CONNECTED TO THE NET
        try {
            if (connectivityManager.getActiveNetworkInfo() != null
                    && connectivityManager.getActiveNetworkInfo().isAvailable()
                    && connectivityManager.getActiveNetworkInfo().isConnected()) {

                return Boolean.TRUE;
            } else {
                return Boolean.FALSE;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Boolean.FALSE;
    }

    public static boolean getActiveInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetInfo == null)
            return false;
        else if (!activeNetInfo.isAvailable()) {
            return false;
        }
        return true;
    }

    public static boolean isGPSEnable(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static void bottomup(final Context context, final View view) {
        Animation anim = AnimationUtils.loadAnimation(context, R.anim.fadein);
        view.startAnimation(anim);

        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {


            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public static void leftToRight(final Context context, final View view, final DrawerLayout drawer,final View view2) {
        Animation anim = AnimationUtils.loadAnimation(context, R.anim.slide_left_right);
        view.startAnimation(anim);

        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {


            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public static void rightToLeft(final Context context, final View view, final DrawerLayout drawer) {
        Animation anim = AnimationUtils.loadAnimation(context, R.anim.slide_right_left);
        view.startAnimation(anim);

        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {


            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onAnimationEnd(Animation animation) {

                view.setVisibility(View.GONE);
                drawer.closeDrawers();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    public static void leftToRight2(final Context context, final View view) {
        Animation anim = AnimationUtils.loadAnimation(context, R.anim.slide_right_left);
        view.startAnimation(anim);

        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public static void leftToRight3(final Context context, final View view) {
        Animation anim = AnimationUtils.loadAnimation(context, R.anim.slide_left_right);
        anim.setDuration(300);
        view.startAnimation(anim);

        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public static void bounce(final Context context, final View view) {
        Animation anim = AnimationUtils.loadAnimation(context, R.anim.resizing);
        view.startAnimation(anim);

        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onAnimationEnd(Animation animation) {
                Animation anim = AnimationUtils.loadAnimation(context, R.anim.resizinglarge);
                view.startAnimation(anim);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public static void ZoomIn(final Context context, final View view) {
        Animation anim = AnimationUtils.loadAnimation(context, R.anim.zoomin);
        view.startAnimation(anim);

        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onAnimationEnd(Animation animation) {
                ZoomOut(context,view);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public static void ZoomOut(final Context context, final View view) {
        Animation anim = AnimationUtils.loadAnimation(context, R.anim.zoomout);
        view.startAnimation(anim);

        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onAnimationEnd(Animation animation) {
           ZoomIn(context,view);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    public static CommonUtils getInstance(Context _context) {
        CommonUtils utils = null;
        if (utils == null) {
            context = _context;
            utils = new CommonUtils();
        }
        return utils;
    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdfDate.setTimeZone(TimeZone.getTimeZone("MST"));
        Date now = new Date();
        return sdfDate.format(now);
    }

    public boolean serverPingRequest() {
        ConnectivityManager connMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connMan.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            try {
                URL urlServer = new URL(context.getResources().getString(R.string.web_url));
                HttpURLConnection urlConnection = (HttpURLConnection) urlServer.openConnection();
                urlConnection.connect();
                Log.e("http status code", String.valueOf(urlConnection.getResponseCode()));
                return urlConnection.getResponseCode() == 200;
            } catch (MalformedURLException e1) {
                return false;
            } catch (IOException e) {
                return false;
            }
        }
        return false;
    }

    public void showMessage(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void showShortMessage(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void setSharedPerf(String key, String val) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, val);
        editor.apply();
    }

    public String getSharedPref(String key) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getString(key, "");
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static SpannableStringBuilder getBoldText(Activity context, String text, int start, int end, int color) {
        text = text.trim();
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
        spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, color)), start, end, 0);
        return spannableStringBuilder;
    }

    public static SpannableStringBuilder forgotBoldText(Activity context, String text, int start, int end, int color) {
        text = text.trim();
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
        spannableStringBuilder.setSpan(getTypefaceBold(context), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, color)), start, end, 0);

        return spannableStringBuilder;
    }

    public Typeface getTypeface(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Century Gothic.ttf");
    }


    public static String getFacebookKeyHash(Context context) {
        String keyhash = "";

        try {
            @SuppressLint("PackageManagerGetSignatures") PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                keyhash = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.i("KeyHash:", keyhash);
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException ignored) {

        }
        return keyhash;
    }

    public static void setFontAwesome(View[] views) {
        Typeface openSans = Typeface.createFromAsset(views[0].getContext().getAssets(), "fonts/fontAwesome.ttf");

        for (View view : views) {
            if (view instanceof TextView)
                ((TextView) view).setTypeface(openSans);
            if (view instanceof EditText)
                ((EditText) view).setTypeface(openSans);
            if (view instanceof Button)
                ((Button) view).setTypeface(openSans);
        }
    }

    public static Typeface getTypefaceBold(Context context) {
      //  return Typeface.createFromAsset(context.getAssets(), "fonts/gothicb.ttf");
        return Typeface.createFromAsset(context.getAssets(), "fonts/CosmicSans.ttf");
    }

    public Typeface getKeyboardTypeface(Context context) {
      //  return Typeface.createFromAsset(context.getAssets(), "fonts/design.graffiti.comicsansmsgras.ttf");
        return Typeface.createFromAsset(context.getAssets(), "fonts/CosmicSans.ttf");
    }

    public Typeface getCosmicSansFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/CosmicSans.ttf");
    }

    public Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

    public String getCountryCode(String countryCodeName) {
        return (countryCodeName.substring(countryCodeName.indexOf("+") - 1, countryCodeName.length())).replace(" ", "");
    }

    public String getCountryName(String countryCodeName) {
        return countryCodeName.substring(0, countryCodeName.indexOf("+") - 1);
    }

    public void getAppName() {
        Spanned text = Html.fromHtml("This mixes <b>bold</b> and <i>italic</i> stuff");
    }

    public String getEmailName(Context context) {
        String email = getEmail(context);
        String[] parts = email.split("@");

        if (parts.length > 1) {
            return parts[0];
        }

        return "Enter Name";
    }

    public String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);

        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }

    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

    public static void vibrate(Context context, int length) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(length);
    }

    public  static void smallText(TextView v)
    {
       v.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    }
    public  static void mediumText(TextView v)
    {
        v.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
    }
    public  static void largeText(TextView v)
    {
        v.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
    }

/*public static void changeTextSize(View view,int size){
        if (view != null) {
            ViewGroup group = (ViewGroup) view;

            for (int idx = 0; idx < group.getChildCount(); idx++) {
                if(group.getChildAt(idx) instanceof ViewGroup){
                    changeTextSize(group.getChildAt(idx),size);
                }
                if(group.getChildAt(idx) instanceof TextView){
                    ((TextView) group.getChildAt(idx)).setTextSize(TypedValue.COMPLEX_UNIT_SP,size);
                }
            }
        }
    }*/
}
