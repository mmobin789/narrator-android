package com.app.notesapp.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.noteapp.R;
import com.app.notesapp.Sockets.SocketService;
import com.app.notesapp.api.ApiNarrator;
import com.app.notesapp.interfaces.CrossOffListner;
import com.app.notesapp.interfaces.DeleteItemListner;
import com.app.notesapp.interfaces.OnSwipeTouchListener;
import com.app.notesapp.models.GsonListItems;
import com.app.notesapp.models.StrikeItem;
import com.app.notesapp.utils.CommonUtils;
import com.app.notesapp.utils.StaticData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Danish on 8/2/2017.
 */

public class ListItemsAdapter extends RecyclerView.Adapter<ListItemsAdapter.ViewHolder> implements CrossOffListner {


    private List<GsonListItems> list ;
    private Context context;
    RelativeLayout item, delete;
    TextView deleteItem;
    private int sizeCheck;
public int cardViewHeight=0;
    public boolean check = true;

    public ListItemsAdapter(List<GsonListItems> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public void addList(List<GsonListItems> list){
        this.list = list;
    }
    @Override
    public ListItemsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_items_card_row, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListItemsAdapter.ViewHolder holder, int i) {



        if (list.get(i).getStatus() == 1) {
            holder.listItemName.setPaintFlags(holder.listItemName.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            if (list.get(i).getCrossOffBy() == Integer.parseInt(StaticData.id)) {
                holder.listItemName.setTextColor(ContextCompat.getColor(context, R.color.crossoff_register_keyboardNum_color));
            } else {
             //   holder.listItemName.setTextColor(ContextCompat.getColor(context, R.color.crossoff_register_keyboardTxt_color));
                holder.listItemName.setTextColor(ContextCompat.getColor(context, R.color.darkgray));
            }
        } else {
            holder.listItemName.setTextColor(ContextCompat.getColor(context, R.color.crossoff_register_keyboardTxt_color));
            holder.listItemName.setPaintFlags(holder.listItemName.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }
        holder.listItemName.setText(list.get(i).getItemName());
        Log.i("gsonlist6"," "+list.get(i).getItemName());
        if(i==0)
        {
            holder.seperator.setVisibility(View.GONE);
        }
        else{
            holder.seperator.setVisibility(View.VISIBLE);
        }

//        listItemName.setText(list.get(holder.getAdapterPosition()).getItemName());
//        if (list.get(holder.getAdapterPosition()).getStatus() == 1) {
//
//            itemCross(holder.getAdapterPosition());
//
//        } else {
//            listItemName.setTextColor(ContextCompat.getColor(context, R.color.crossoff_register_keyboardTxt_color));
//            listItemName.setPaintFlags(listItemName.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
//        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView listItemName;
       public CardView cv;
        View seperator;

        public ViewHolder(View view) {
            super(view);

            cv = itemView.findViewById(R.id.card_view_referrals);
            listItemName = itemView.findViewById(R.id.item_name_main);
            listItemName.setTypeface(CommonUtils.getInstance().getTypeface(context));
            item = itemView.findViewById(R.id.rel_card_item);
        seperator=itemView.findViewById(R.id.view_1);
            Log.i("cardview",""+cardViewHeight);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    cardViewHeight);
            cv.setLayoutParams(params);
            Log.i("cardview3",""+cardViewHeight);


            Log.i("cardview1",""+cardViewHeight);
       RelativeLayout.LayoutParams param2 = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                   LinearLayout.LayoutParams.MATCH_PARENT);
       param2.setMargins(0,0,0,context.getResources().getDimensionPixelOffset(R.dimen._54sdp));
      param2.addRule(RelativeLayout.CENTER_HORIZONTAL);
            listItemName.setLayoutParams(param2);


          listItemName.setTypeface(CommonUtils.getInstance().getCosmicSansFont(context));
            listItemName.setOnTouchListener(new OnSwipeTouchListener(context) {


                @Override
                public void onRightToLeftSwipe() {// uncross

//                    if (check) {

                    StaticData.crossOffCheck=true;
                        int position = getAdapterPosition();
                        if (list.get(position).getCrossOffBy() == Integer.parseInt(StaticData.id)) {
                            list.get(position).setStatus(0);
                            String listId = list.get(position).getListId() + "";
                            String itemId = list.get(position).getId() + "";
                            notifyItemChanged(position);
                            SocketService.getServerSocket().listToCrossOf(StaticData.id, listId, itemId, 0);
                            Log.i("position", "" + position);
                        } else {
                            Toast.makeText(context, R.string.not_allowed, Toast.LENGTH_SHORT).show();
                            CommonUtils.vibrate(context, 500);
                        }

//                    }
//                    check = false;
//                    listItemName.setClickable(false);
//
//                    listItemName.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            listItemName.setClickable(true);
//                        }
//                    }, 100);


//                    notifyItemChanged(getAdapterPosition());

                }

                @Override
                public void onLeftToRightSwipe() {//cross

                    StaticData.timecheck2=false;
                    StaticData.colorCheck=true;
//                    if (check) {
                        int position = getAdapterPosition();
                        if (list.get(position).getStatus() != 1) {
                            list.get(position).setStatus(1);
                            list.get(position).setCrossOffBy(Integer.parseInt(StaticData.id));
                            String listId = list.get(position).getListId() + "";
                            String itemId = list.get(position).getId() + "";
                            notifyItemChanged(position);
                            SocketService.getServerSocket().listToCrossOf(StaticData.id, listId, itemId, 1);
                        } else {
                              Toast.makeText(context, R.string.not_allowed, Toast.LENGTH_SHORT).show();
                            CommonUtils.vibrate(context, 500);
                        }

//                    }
//                    check = false;
                   /* listItemName.setClickable(false);
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            check=true;
                            listItemName.setClickable(true);
                        }
                    }, 1000);*/
//                    notifyItemChanged(getAdapterPosition());

//                    listItemName.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            check = true;
//                            listItemName.setClickable(true);
//                        }
//                    }, 500);
                }

                public void onDoubleTap() {

                    if(StaticData.DeleteCheck==2)
                    {
                        ApiNarrator.deleteListItem(list.get(getAdapterPosition()).getId(), new DeleteItemListner() {
                            @Override
                            public void onItemDeleted() {
                                list.remove(getAdapterPosition());
                                notifyItemRemoved(getAdapterPosition());
                                Toast.makeText(context, "Item Deleted", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onItemDeletedFailure() {
                            }
                        });
                    }
                    else{
                        Toast.makeText(context,"You Don't have Permission for Item Deletion",Toast.LENGTH_LONG).show();
                    }



                }
            });

            listItemName.setTextSize(sizeCheck);
        }
    }

    @Override
    public void onCrossOffAction(StrikeItem strikeItem) {
        Log.i("Adapter", strikeItem.toString());

        if (list != null && list.size() > 0) {
            int listId = list.get(0).getListId();
            Log.i("inside", listId + " " + strikeItem.getListId());
            if (listId == strikeItem.getListId()) {
                for (int i = 0; i < list.size(); i++) {
                    if (strikeItem.getItemId() == list.get(i).getId()) {
                        list.get(i).setStatus(strikeItem.getMessage());
                        list.get(i).setCrossOffBy(strikeItem.getCrossOffBy());
                        notifyItemChanged(i);
                        break;
                    }
                }
            }
        }
    }

    public void changeSize(int size) {
        sizeCheck = size;
    }

    public void cardViewHeight(int height)
    {
        cardViewHeight=height;
        Log.i("cardview2",""+height);
    }


//    public void itemCross(int i) {
//        listItemName.setPaintFlags(listItemName.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//        if (list.get(i).getCrossOffBy() == Integer.parseInt(StaticData.id)) {
//            listItemName.setTextColor(ContextCompat.getColor(context, R.color.crossoff_register_keyboardNum_color));
//        } else {
//            listItemName.setTextColor(ContextCompat.getColor(context, R.color.crossoff_register_keyboardTxt_color));
//        }
//
//    }
}
