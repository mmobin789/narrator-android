package com.app.notesapp.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.noteapp.R;
import com.app.notesapp.activities.SendActivityRecyclerview;
import com.app.notesapp.activities.SubjectDataActivity;
import com.app.notesapp.api.ApiNarrator;
import com.app.notesapp.interfaces.CopyListener;
import com.app.notesapp.interfaces.DeleteListListener;
import com.app.notesapp.interfaces.ForgoPasswordListener;
import com.app.notesapp.models.MainActivityList;
import com.app.notesapp.utils.CommonUtils;
import com.app.notesapp.utils.StaticData;
import com.app.notesapp.vo.WordsDataVO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Danish on 8/2/2017.
 */

public class MainActivityAdapter extends RecyclerView.Adapter<MainActivityAdapter.ViewHolder> implements View.OnClickListener {

    private ArrayList<MainActivityList> list_model;
    Context context;
    Dialog option;
    ArrayList<WordsDataVO>arrayList;
    ImageView img_msg, img_send, img_delete;
    boolean timeCheck=true;
    public int list_ID;
    public int adapterposition = -1;
    public String list_Name;
    public StringBuilder items_Name;
    private int sizeCheck = 14;
    private int Speed = 0;
    private static final int PHONE = 0;
    private static final int EMAIL = 1;
    private static final int SEEN = 1;
    private static final int UNSEEN = 0;
    private int statusCode = 2;
    View view;
    Dialog CopyDialog;

    private ImageView share, send, delete;
    RecyclerView recyclerView;
    TextView copyBtn,cancelBtn;
EditText etCopy;
    String copyListName;
    String listNameCheck="";
   public boolean clickCheck=true;
    public MainActivityAdapter(ArrayList<MainActivityList> list_model, Context context) {
        this.list_model = list_model;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.acitvity_main_card_row, viewGroup, false);
        recyclerView = (RecyclerView) viewGroup;

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int i) {

        if(i==0)
        {
            holder.seperatorline.setVisibility(View.GONE);
        }
        else{
            holder.seperatorline.setVisibility(View.VISIBLE);
        }

        if(list_model.get(i).getCrossOffStatus()==0)
        {
            holder.txt_time.setTextColor(ContextCompat.getColor(context, R.color.textColor));

        }
        else {
            holder.txt_time.setTextColor(ContextCompat.getColor(context, R.color.textstrips));
        }
        if(list_model.get(i).getSeen()==0)
        {
            holder.relative.setBackgroundColor(ContextCompat.getColor(context, R.color.listcolor));
        }else
        {
           holder.relative.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        }
        if (list_model.get(holder.getAdapterPosition()).getIsOptionLayout() == 1) {
            CommonUtils.leftToRight3(context, holder.linear);
            holder.linear.setVisibility(View.VISIBLE);
        } else {
            holder.linear.setVisibility(View.GONE);
        }

        holder.list_name.setTextSize(sizeCheck);
        listChecks(holder);
     /*  if (timeCheck) {
           holder.txt_time.setTextColor(ContextCompat.getColor(context, R.color.textstrips));
       }
       else if(StaticData.crossOffCheck)
       {
           holder.txt_time.setTextColor(ContextCompat.getColor(context, R.color.textstrips));
       }
       else{
           holder.txt_time.setTextColor(ContextCompat.getColor(context, R.color.textColor));
       }*/
        if(list_model.get(i).getListName().length()>12)
        {
            listNameCheck=list_model.get(i).getListName().substring(0,12).concat("...");
        }

        else{
            listNameCheck=list_model.get(i).getListName();
        }
        holder.list_name.setText(listNameCheck);
        try{
            holder.txt_time.setText(list_model.get(i).getListCreationTime().substring(0, 10));
        }
       catch (Exception e)
       {
           Log.i("timeerror",e.toString());
       }

        if (list_model.get(holder.getAdapterPosition()).getListStatus() ==UNSEEN) {
            holder.list_name.setTextColor(ContextCompat.getColor(context, R.color.textColor));
        }
        else if(list_model.get(holder.getAdapterPosition()).getListStatus()==SEEN)
        {
            holder.list_name.setTextColor(ContextCompat.getColor(context, R.color.textstrips));
        }
        else{
            holder.list_name.setTextColor(ContextCompat.getColor(context, R.color.textstrips));
        }
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                if (list_model.get(holder.getAdapterPosition()).getId() >= 0) {
                    if (list_model.get(holder.getAdapterPosition()).getListStatus() != -1) {

                  send.setVisibility(View.INVISIBLE);
                        Log.i("123asd",""+list_model.get(holder.getAdapterPosition()).getListStatus());
                    } else {
                   send.setVisibility(View.VISIBLE);
                        Log.i("123asd2",""+list_model.get(holder.getAdapterPosition()).getListStatus());
                    }
                }


                list_ID = list_model.get(holder.getAdapterPosition()).getId();
                ApiNarrator.retrieveItems2(context, list_ID);
                setOptionLayout(list_ID);
                list_Name = list_model.get(holder.getAdapterPosition()).getListName();
                adapterposition = holder.getAdapterPosition();
                notifyDataSetChanged();

                return false;
            }
        });
    }

    public void updateList(ArrayList<MainActivityList> listItems) {
        this.list_model= listItems;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return list_model.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView list_name;
        LinearLayout linear;
        ImageView sendMessage, sendEmail;
        TextView phoneCounter, emailCounter;
        TextView shareEmail;
        TextView txt_time;
        RelativeLayout relative;
        boolean clickCheck=true;
        View seperatorline;

        public ViewHolder(final View view) {
            super(view);
            list_name = itemView.findViewById(R.id.listname_main);
            sendEmail = itemView.findViewById(R.id.img_arrow_send);
            sendMessage = itemView.findViewById(R.id.img_message);
            phoneCounter = itemView.findViewById(R.id.phone_counter);
            emailCounter = itemView.findViewById(R.id.email_counter);
            relative=itemView.findViewById(R.id.rel_container);
            txt_time = itemView.findViewById(R.id.txt_time);
            shareEmail = itemView.findViewById(R.id.shareEmail);
            seperatorline=itemView.findViewById(R.id.view_1);


         list_name.setTypeface(CommonUtils.getInstance().getCosmicSansFont(context));
            txt_time.setTypeface(CommonUtils.getInstance().getCosmicSansFont(context));
shareEmail.setTypeface(CommonUtils.getInstance().getCosmicSansFont(context));

            linear = itemView.findViewById(R.id.rel_options);
            relative = itemView.findViewById(R.id.rel_container);
            share = itemView.findViewById(R.id.share);
            send = itemView.findViewById(R.id.send);
            delete = itemView.findViewById(R.id.delete);


            CommonUtils.setFontAwesome(new View[]{sendEmail});
         /*   list_name.setTypeface(CommonUtils.getInstance().getTypeface(context));*/




                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (clickCheck) {


                            clickCheck = false;
                            final int position = getAdapterPosition();
                            StaticData.viewPosition=getAdapterPosition();
                            if (list_model.get(position).getId() >= 0) {
                                if (list_model.get(position).getListStatus() != -1) {
                                    ApiNarrator.statusList(list_model.get(position).getId(), Integer.parseInt(StaticData.id), new ForgoPasswordListener() {
                                                @Override
                                                public void OnSuccess(int data, int code) {

                                                    statusCode = code;
                                                    Log.i("asd", "code" + " " + statusCode);
                                                    if (code != -1) {
                                                        //  sendMessage.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.msg_read));
                                                        list_model.get(position).setListStatus(code);
                                                        notifyItemChanged(position);
                                                    }
                                                    Log.i("textsizecheck", "" + sizeCheck);
                                                    Intent intent = new Intent(context, SubjectDataActivity.class);
                                                    intent.putExtra("ID", list_model.get(position).getId());
                                                    intent.putExtra("SUBJECT", list_model.get(position).getListName());
                                                    intent.putExtra("statusCode", statusCode);
                                                    intent.putExtra("txtsize", sizeCheck);
                                                    intent.putExtra("micCheck", StaticData.AdapterMicCheck);
                                                    intent.putExtra("listeningSpeed", Speed);
                                                    context.startActivity(intent);
                                                }

                                                @Override
                                                public void OnFail(String error) {
                                                }
                                            }
                                    );
                                } else {

                                    Log.i("asd", "code2" + " " + statusCode);
                                    Toast.makeText(context, "My List", Toast.LENGTH_SHORT).show();
                                    Intent in = new Intent(context, SubjectDataActivity.class);
                                    in.putExtra("ID", list_model.get(getAdapterPosition()).getId());
                                    in.putExtra("SUBJECT", list_model.get(getAdapterPosition()).getListName());
                                    in.putExtra("statusCode", statusCode);
                                    in.putExtra("micCheck", StaticData.AdapterMicCheck);
                                    in.putExtra("txtsize", sizeCheck);

                                    context.startActivity(in);
                                }
                            }
                        } else {
                            Log.i("clickCheck", "Can not perform click at this time");
                        }
                    }
                });



            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Option_Send();
                }
            });

            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if(send.getVisibility()==View.VISIBLE)
                    {
                        sendSMS();
                    }
                    else{

                        Log.i("send","nothing");
                    }
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ApiNarrator.deleteList(context, list_ID, new DeleteListListener() {
                        @Override
                        public void OnDeleteList() {
                            notifyItemRemoved(adapterposition);
                            ApiNarrator.lists.remove(adapterposition);
                        }

                        @Override
                        public void OnDeleteListFailure() {

                        }
                    });
                }
            });




         //   txt_time.setTextSize(sizeCheck);
          //  shareEmail.setTextSize(sizeCheck);
            // CommonUtils.changeTextSize(cv, size);
//            notifyDataSetChanged();
            ////

        /*    view.setOnTouchListener(new OnSwipeTouchListener(context)
            {

                @Override
                public void onDoubleTap() {

                    ApiNarrator.deleteList(context, list_ID, new DeleteListListener() {
                        @Override
                        public void OnDeleteList() {
                            notifyItemRemoved(adapterposition);
                            ApiNarrator.lists.remove(adapterposition);
                        }

                        @Override
                        public void OnDeleteListFailure() {

                        }
                    });
                }
*/






        }


    }

    public void Option_Dialog(View view) {
       /* option = new Dialog(context);
        option.requestWindowFeature(Window.FEATURE_NO_TITLE);
        option.setContentView(R.layout.option_popup);
        img_send = option.findViewById(R.id.img_send_icon);
        img_msg = option.findViewById(R.id.img_msg_icon);
        img_delete = option.findViewById(R.id.img_delete_icon);
        option.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        option.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        option.setCancelable(true);
        option.show();*/

        MainActivityList list = list_model.get(adapterposition);

        Log.i("adapterissue1", "" + recyclerView.findViewHolderForAdapterPosition(adapterposition).getAdapterPosition());
        Log.i("adapterissue2", "" + adapterposition);
        Log.i("adapterissue3", "" + recyclerView.findViewHolderForAdapterPosition(adapterposition).getLayoutPosition());

        notifyItemChanged(adapterposition);
      /*  img_send.setOnClickListener(this);
        img_msg.setOnClickListener(this);
        img_delete.setOnClickListener(this);*/
    }

    @Override
    public void onClick(View v) {

        if (v == img_send) {
            Option_Send();
            if (option.isShowing()) {
                option.dismiss();
            }

        } else if (v == img_msg) {
            sendSMS();
            if (option.isShowing()) {
                option.dismiss();
            }
        } else if (v == img_delete) {
            ApiNarrator.deleteList(context, list_ID, new DeleteListListener() {
                @Override
                public void OnDeleteList() {
                    notifyItemRemoved(adapterposition);
                    ApiNarrator.lists.remove(adapterposition);
                }

                @Override
                public void OnDeleteListFailure() {

                }
            });
            if (option.isShowing()) {
                option.dismiss();
            }
//            ApiNarrator.items_name_list.remove(adapterposition);

        }

    }


    public void Option_Send() {
      /*  Intent in = new Intent(context, SendActivityRecyclerview.class);
        in.putExtra("List_Name", list_Name);
        in.putExtra("Items_Name", items_Name.toString());
        in.putExtra("list_id", list_ID);
        context.startActivity(in);*/
        arrayList=new ArrayList<>();

       JSONArray array = new JSONArray();
       try{
           for(int i=0;i<ApiNarrator.smsList.size();i++)
           {
             arrayList.add(new WordsDataVO("1", ApiNarrator.smsList.get(i).getItemName() + ""));
               Log.i("copylist","acbd"+arrayList.get(i));
///Json: [{"item_name":"hello","status":"1"},{"item_name":"how are you","status":"1"},{"item_name":"I am fine","status":"1"}]
           }
       }
       catch (Exception e)
       {
           Log.i("JsonArrayexception",e.toString());
       }
       try{
           for(int i=0;i<arrayList.size();i++)
           {
               array.put(new JSONObject(arrayList.get(i).toString()));
           }

       }
       catch (Exception e)
       {
           Log.i("JsonArrayexception2",e.toString());
       }

      foo_DialogBox_Copy(array);

        resetList();
    }

    public void sendSMS() {

     /*   try{
            Intent smsIntent = new Intent(Intent.ACTION_VIEW);
            smsIntent.setType("vnd.android-dir/mms-sms");
            smsIntent.putExtra("sms_body", "List Name" + "\n" + list_Name + "\n" + "Item Names" + "\n" + items_Name);
            context.startActivity(smsIntent);
            Log.i("list3", "" + list_Name);
        }catch (Exception e){
            Toast.makeText(context,"Not Available",Toast.LENGTH_SHORT).show();
        }*/
     Intent inn = new Intent(context, SendActivityRecyclerview.class);
        inn.putExtra("list_id",list_ID);
        inn.putExtra("List_Name",list_Name);
        context.startActivity(inn);

    }

    private void listChecks(ViewHolder viewHolder) {
        MainActivityList list = list_model.get(viewHolder.getAdapterPosition());
        setLayoutToDefault(viewHolder);
        checkPhoneCount(list, viewHolder);
        checkEmailCount(list, viewHolder);
        receiveByPhone(list, viewHolder);
        receivedByEmail(list, viewHolder);
    }

    private void setLayoutToDefault(ViewHolder viewHolder) {
        viewHolder.phoneCounter.setVisibility(View.GONE);
        viewHolder.sendMessage.setVisibility(View.GONE);
        viewHolder.sendEmail.setVisibility(View.GONE);
        viewHolder.emailCounter.setVisibility(View.GONE);
        viewHolder.shareEmail.setVisibility(View.GONE);
        timeCheck = false;
    }

    private void checkPhoneCount(MainActivityList list, ViewHolder viewHolder) {
        if (list.getPhoneCount() > 0) {
            viewHolder.phoneCounter.setVisibility(View.VISIBLE);
            viewHolder.phoneCounter.setText(String.valueOf(list.getPhoneCount()));
            viewHolder.sendMessage.setVisibility(View.VISIBLE);
            viewHolder.sendMessage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_email));


            RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            params.setMargins(0,0,context.getResources().getDimensionPixelOffset(R.dimen._33sdp),0);
            viewHolder.sendEmail.setLayoutParams(params);

            RelativeLayout.LayoutParams emailCountParams=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            emailCountParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            emailCountParams.setMargins(0,0,context.getResources().getDimensionPixelOffset(R.dimen._28sdp),0);
            viewHolder.emailCounter.setLayoutParams(emailCountParams);

            timeCheck = true;
        }
        else{

            RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            params.setMargins(0,0,context.getResources().getDimensionPixelOffset(R.dimen._8sdp),0);
            viewHolder.sendEmail.setLayoutParams(params);

            RelativeLayout.LayoutParams emailCountParams=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            emailCountParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            emailCountParams.setMargins(0,0,context.getResources().getDimensionPixelOffset(R.dimen._2sdp),0);
            viewHolder.emailCounter.setLayoutParams(emailCountParams);

        }
    }

    private void checkEmailCount(MainActivityList list, ViewHolder viewHolder) {
        if (list.getEmailCount() > 0) {

            viewHolder.sendEmail.setVisibility(View.VISIBLE);
            viewHolder.sendEmail.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.send_orange));
            viewHolder.emailCounter.setVisibility(View.VISIBLE);
            viewHolder.emailCounter.setText(String.valueOf(list.getEmailCount()));

        RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            params.addRule(RelativeLayout.LEFT_OF,R.id.phone_counter);
            params.setMargins(0,0,context.getResources().getDimensionPixelOffset(R.dimen._5sdp),0);
            viewHolder.sendMessage.setLayoutParams(params);

            RelativeLayout.LayoutParams phoneCountParams=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            phoneCountParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            phoneCountParams.setMargins(0,0,context.getResources().getDimensionPixelOffset(R.dimen._2sdp),0);
            viewHolder.phoneCounter.setLayoutParams(phoneCountParams);

            if(viewHolder.sendMessage.getVisibility()!=View.VISIBLE){
                RelativeLayout.LayoutParams emailCountParams=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                emailCountParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                emailCountParams.setMargins(0,0,context.getResources().getDimensionPixelOffset(R.dimen._5sdp),0);
                viewHolder.emailCounter.setLayoutParams(emailCountParams);
            }
            else
            {
                RelativeLayout.LayoutParams emailCountParams=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                emailCountParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                emailCountParams.setMargins(0,0,context.getResources().getDimensionPixelOffset(R.dimen._28sdp),0);
                viewHolder.emailCounter.setLayoutParams(emailCountParams);
            }

            timeCheck = true;
        }
        else{

            RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            params.setMargins(0,0,context.getResources().getDimensionPixelOffset(R.dimen._8sdp),0);
            viewHolder.sendMessage.setLayoutParams(params);

            RelativeLayout.LayoutParams emailCountParams=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            emailCountParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            emailCountParams.setMargins(0,0,context.getResources().getDimensionPixelOffset(R.dimen._2sdp),0);
            viewHolder.phoneCounter.setLayoutParams(emailCountParams);
        }
    }

    private void receiveByPhone(MainActivityList list, ViewHolder viewHolder) {


        if (list.getSharedCount() == PHONE) {
            if (list.getListStatus() == UNSEEN) {
                viewHolder.sendMessage.setVisibility(View.VISIBLE);
                viewHolder.sendMessage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_email));
            } else if (list.getListStatus() == SEEN) {
                viewHolder.sendMessage.setVisibility(View.VISIBLE);
                viewHolder.sendMessage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.msg_read));
            }
            timeCheck = true;
        }
    }

    private void receivedByEmail(MainActivityList list, ViewHolder viewHolder) {
        if (list.getSharedCount() == EMAIL) {
            if (list.getSeen() == SEEN) {
                viewHolder.sendEmail.setVisibility(View.VISIBLE);
                viewHolder.sendEmail.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.recieve_orange));
            } else if (list.getSeen() == UNSEEN) {
                viewHolder.sendEmail.setVisibility(View.VISIBLE);
                viewHolder.sendEmail.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.send_gray));
            }
            if (!list.getShareEmail().equals("")) {
                viewHolder.shareEmail.setVisibility(View.VISIBLE);
                viewHolder.shareEmail.setText(list.getShareEmail());
            }
           timeCheck = true;
        }
    }

    public void changeSize(int size, int speed) {
        sizeCheck = size;
        Speed = speed;
notifyDataSetChanged();
        Log.i("textsizefoo", "" + sizeCheck);

    }

    private void setOptionLayout(int id) {
        for (MainActivityList list : list_model) {
            if (list.getId() == id) {
                list.setIsOptionLayout(1);
            } else {
                list.setIsOptionLayout(-1);
            }
        }
    }
    public void resetList(){
        for (MainActivityList list : list_model) {
                list.setIsOptionLayout(-1);
        }
        notifyDataSetChanged();
    }

    public void foo_DialogBox_Copy(final JSONArray array) {
        CopyDialog = new Dialog(context);
        CopyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        CopyDialog.setContentView(R.layout.copylistdialog);
        CopyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        etCopy=CopyDialog.findViewById(R.id.et_listName_dialog);
    copyBtn = CopyDialog.findViewById(R.id.btn_copy);
cancelBtn=CopyDialog.findViewById(R.id.btn_copycancel);

        etCopy.setText(list_Name+"(Copy)");
        CopyDialog.setCancelable(false);
        CopyDialog.show();

cancelBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                CopyDialog.dismiss();
                return false;
            }
        });
 copyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                copyListName=etCopy.getText().toString().toLowerCase().trim();

                if(!copyListName.isEmpty())

                {
                    if(copyListName.equals(list_Name))
                    {
                        Toast.makeText(context,"List Already Exist",Toast.LENGTH_LONG).show();
                    }
                    else{

                        ApiNarrator.copyList(list_ID, copyListName, array, new CopyListener() {
                            @Override
                            public void onCopySuccess() {
                                ApiNarrator.retrieveList(context);
                                notifyDataSetChanged();
                                CopyDialog.dismiss();
                            }

                            @Override
                            public void onCopyFailed() {

                            }
                        });
                    }
                }
                else
                {
                    Toast.makeText(context,"Please Enter List Name",Toast.LENGTH_LONG).show();
                }



            }
        });
    }

/*public void drawer_check(ViewHolder holder)
{
if(clickCheck)
{
    holder.itemView.setClickable(false);
}
else {
    holder.itemView.setClickable(true);
}
}
public void foo_drawer(boolean check)
{
    if(check)
    {
        clickCheck=true;
    }
    else if(!check)
    {
        clickCheck=false;
    }
}*/
   /* public void sendListCheck(final int adapterposition)
    {
        if (list_model.get(adapterposition).getId() >= 0) {
            if (list_model.get(adapterposition).getListStatus() != -1) {

                Log.i("asd","sent failed");
            } else {


            }
        }
    }*/
}
