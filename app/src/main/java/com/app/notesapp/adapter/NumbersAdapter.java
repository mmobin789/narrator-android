package com.app.notesapp.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.accessibility.AccessibilityManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.noteapp.R;
import com.app.notesapp.activities.SendActivityRecyclerview;
import com.app.notesapp.api.ApiNarrator;
import com.app.notesapp.interfaces.ForgoPasswordListener;
import com.app.notesapp.models.SyncContact;
import com.app.notesapp.utils.CommonUtils;
import com.app.notesapp.utils.StaticData;

import java.util.ArrayList;

/**
 * Created by Danish on 8/24/2017.
 */

public class NumbersAdapter extends RecyclerView.Adapter<NumbersAdapter.ContactViewHolder> {

    public ArrayList<SyncContact> syncContacts;
    private Context mContext;
    private Dialog dialog;
    private int position=0;
    private boolean emailCheck;
    public NumbersAdapter(ArrayList<SyncContact> syncContacts, Context mContext) {
        this.syncContacts = syncContacts;
        this.mContext = mContext;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.activity_send_row, null);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        holder.tvContactName.setText(syncContacts.get(position).getName());
        holder.tvEmail.setText(syncContacts.get(position).getEmail());
        if (syncContacts.get(position).getStatus() == 1) {
            holder.contactIcon.setImageResource(R.drawable.ic_app_icon);
        } else {
            holder.contactIcon.setImageResource(R.drawable.ic_msg_icon);
        }

    }

    @Override
    public int getItemCount() {
        return syncContacts.size();
    }

    public void updateList(ArrayList<SyncContact> syncContacts) {
        this.syncContacts = syncContacts;
        notifyDataSetChanged();
    }


    public class ContactViewHolder extends RecyclerView.ViewHolder {

        TextView tvContactName;
        ImageView contactIcon;
        TextView tvEmail;


        public ContactViewHolder(View itemView) {
            super(itemView);
            tvContactName = itemView.findViewById(R.id.textView);
            tvEmail=itemView.findViewById(R.id.txtEmail);
            tvEmail.setTypeface(CommonUtils.getInstance().getCosmicSansFont(mContext));
            tvContactName.setTypeface(CommonUtils.getInstance().getCosmicSansFont(mContext));
            contactIcon = itemView.findViewById(R.id.contact_img);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
         position=getAdapterPosition();


                    if(syncContacts.get(position).getEmail().equals("No Email"))
                    {
                        emailCheck=false;
                    }
                    else{
                        emailCheck=true;
                    }


                    shareDialogBox(position);
                }
            });
        }
    }

    private void shareList(String phone) {
        if (!phone.startsWith("+92") && !phone.contains("+44")) {
            phone = phone.replaceFirst("0", "");
            phone = "+92" + phone;
        }
        if (!phone.startsWith("+44") && !phone.contains("+92")) {
            phone = phone.replaceFirst("0", "");
            phone = "+44" + phone;
        }

        Log.i("phone", phone);
        ApiNarrator.shareList(Integer.parseInt(StaticData.id), phone, ((SendActivityRecyclerview) mContext).list_id, new ForgoPasswordListener() {
            @Override
            public void OnSuccess(int data, int code) {

                if (code == 1) {
                    Toast.makeText(mContext, "List Sent", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "List not Sent", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void OnFail(String error) {

            }
        });
    }

    private void shareOnEmail(String email)
    {

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
Toast.makeText(mContext,"Email Format is Wrong", Toast.LENGTH_LONG).show();
        } else {
            ApiNarrator.emailShare(Integer.parseInt(StaticData.id), email, ((SendActivityRecyclerview) mContext).list_id, new ForgoPasswordListener() {
                @Override
                public void OnSuccess(int data, int code) {

                    if (code == 1) {
                   dialog.dismiss();
                        Toast.makeText(mContext, "List Sent", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, "List not Sent", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void OnFail(String error) {

                }
            });
        }
        dialog.dismiss();
    }

        public void shareDialogBox(final int position) {
            ImageView imgEmail,imgPhone,sendMsg;
            TextView tvCancel,sendEmail,txtMessage,txtSendPhone;
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.sharedialogbox);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        tvCancel = dialog.findViewById(R.id.tv_cancel_dialog);
        imgPhone=dialog.findViewById(R.id.imgPhone);
            sendEmail=dialog.findViewById(R.id.txtsendEmail);
            imgEmail=dialog.findViewById(R.id.imgEmail);
            sendMsg=dialog.findViewById(R.id.imgSendMsg);
            txtSendPhone=dialog.findViewById(R.id.txtsendPhone);
            txtMessage=dialog.findViewById(R.id.txtMessage);

 LinearLayout.LayoutParams params=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.weight=5;

            LinearLayout.LayoutParams imgparams=new LinearLayout.LayoutParams(mContext.getResources().getDimensionPixelOffset(R.dimen._30sdp),mContext.getResources().getDimensionPixelOffset(R.dimen._50sdp));
            imgparams.weight=5;

            if(!emailCheck&&syncContacts.get(position).getStatus() == 1)
            {
                imgEmail.setVisibility(View.GONE);
                sendEmail.setVisibility(View.GONE);
                txtMessage.setLayoutParams(params);
                imgPhone.setLayoutParams(imgparams);

            }
            else if(!emailCheck&&syncContacts.get(position).getStatus() == 0)
            {
                LinearLayout.LayoutParams imgMsgparams=new LinearLayout.LayoutParams(mContext.getResources().getDimensionPixelOffset(R.dimen._35sdp),mContext.getResources().getDimensionPixelOffset(R.dimen._45sdp));
                imgMsgparams.setMargins(mContext.getResources().getDimensionPixelOffset(R.dimen._35sdp),0,0,0);
                imgMsgparams.weight=8;

                LinearLayout.LayoutParams txtMsgparams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                txtMsgparams.gravity= Gravity.CENTER_HORIZONTAL;

                sendMsg.setLayoutParams(imgMsgparams);
                txtMessage.setLayoutParams(txtMsgparams);
                txtSendPhone.setVisibility(View.GONE);
                imgPhone.setVisibility(View.GONE);
                imgEmail.setVisibility(View.GONE);
                sendEmail.setVisibility(View.GONE);
            }
            else if(emailCheck&&syncContacts.get(position).getStatus() == 0)
            {
                LinearLayout.LayoutParams imgEmailparams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,mContext.getResources().getDimensionPixelOffset(R.dimen._50sdp));
                imgEmailparams.weight=5;
                imgEmailparams.setMargins(mContext.getResources().getDimensionPixelOffset(R.dimen._10sdp),0,0,0);

                LinearLayout.LayoutParams txtEmailparams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                txtEmailparams.weight=5;

                imgEmail.setLayoutParams(imgEmailparams);
                sendEmail.setLayoutParams(txtEmailparams);

                txtSendPhone.setVisibility(View.GONE);
                imgPhone.setVisibility(View.GONE);
            }
            else{
                imgEmail.setVisibility(View.VISIBLE);
              sendEmail.setVisibility(View.VISIBLE);
            }
        dialog.setCancelable(false);
        dialog.show();

     tvCancel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dialog.dismiss();
                return false;
            }
        });

        imgPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareList(syncContacts.get(position).getNumber().trim().replace(" ", ""));
                dialog.dismiss();
            }
        });
            imgEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                  shareOnEmail(syncContacts.get(position).getEmail());
                }
            });

            sendMsg.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View view) {

                                               try{
                                                   Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                                   smsIntent.setType("vnd.android-dir/mms-sms");
                                                   smsIntent.putExtra("sms_body", "List Name" + "\n" + ((SendActivityRecyclerview)mContext).list_name + "\n" + "Item Names" + "\n" +((SendActivityRecyclerview)mContext).items_Name);
                                              mContext.startActivity(smsIntent);
                                               }catch (Exception e){
                                                   Toast.makeText(mContext,"Not Available",Toast.LENGTH_SHORT).show();
                                               }
                                           }
                                       }
            );
    }

}