package com.app.notesapp.exception_handling;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.app.noteapp.R;

public class ShowExceptionActivity extends Activity {

	TextView error;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		setContentView(R.layout.activity_show_exception);

		error = findViewById(R.id.error);

		error.setText(getIntent().getStringExtra("error"));
	}
}
