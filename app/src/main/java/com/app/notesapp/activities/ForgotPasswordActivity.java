package com.app.notesapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.noteapp.R;
import com.app.notesapp.api.ApiNarrator;
import com.app.notesapp.interfaces.ForgoPasswordListener;

/**
 * Created by Danish on 8/11/2017.
 */

public class ForgotPasswordActivity extends Activity implements View.OnClickListener {

    EditText et_email_1,et_email_2,et_code,et_new_pass,et_pass_con;
    TextView tv_submit_1,tv_submit_2,tv_submit_3;
    LinearLayout rel_card_1,rel_card_2,rel_card_3;
    String email_1,email_2,new_pass,new_pass_con;
    int check_data;
    int check_code;
    ImageView back_img;
    String email;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initviews();


    }

    public void initviews()
    {
        et_email_1=findViewById(R.id.et_email);
        tv_submit_1=findViewById(R.id.tv_submit1);
        email_1=et_email_1.getText().toString();
        tv_submit_1.setOnClickListener(this);
        back_img=findViewById(R.id.backbtn);
        back_img.setOnClickListener(this);

    }

 /*   public void foo_thread(final String validation)
    {
        handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                if(validation.contains("Submit"))
                {
                    btn_forgot.setText(validation);
                }
                else
                {
                    et_forgot.setHint(validation);
                }

            }
        };
        handler.postDelayed(r, 2000);
    }*/
    @Override
    public void onClick(View view) {


        if(view==tv_submit_1)
        {

            email=et_email_1.getText().toString();
            if(android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
            {
                ApiNarrator.forgotpass_1(email, new ForgoPasswordListener() {
                    @Override
                    public void OnSuccess(int data,int code) {

                        check_data=data;
                        check_code=code;

              if(check_data==1&&check_code==200)
              {
                  Intent in = new Intent(ForgotPasswordActivity.this,VerifycodeActivity.class);
                  in.putExtra("Email",email);
                  startActivity(in);
              }
                    }

                    @Override
                    public void OnFail(String error) {

                        Log.i("forgot_error",error);
                    }

                });

            }
            else if(email.isEmpty())
            {
           et_email_1.setHint("Email Address Required");

              /*  foo_thread("Enter Email Address");*/

            }

            else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
             et_email_1.setText("");
                et_email_1.setHint("Enter Valid Email Address");

             /*   foo_thread("Enter Email Address");*/
            }
        }
        else if(view==back_img)
        {
            onBackPressed();
        }
    }
}
