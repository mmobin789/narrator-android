package com.app.notesapp.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.noteapp.R;
import com.app.notesapp.Sockets.ServerSocket;
import com.app.notesapp.adapter.NumbersAdapter;
import com.app.notesapp.api.ApiNarrator;
import com.app.notesapp.interfaces.ContactsDBListner;
import com.app.notesapp.interfaces.ForgoPasswordListener;
import com.app.notesapp.models.SyncContact;
import com.app.notesapp.utils.ContactsDB;
import com.app.notesapp.utils.SimpleDividerItemDecoration;
import com.app.notesapp.utils.StaticData;

import java.util.ArrayList;

/**
 * Created by Danish on 8/24/2017.
 */

public class SendActivityRecyclerview extends Activity implements ContactsDBListner {

    RecyclerView recyclerView;
    EditText et_search;
    ArrayList<SyncContact> contactVOList;
    TextView btn_email, topText;
    NumbersAdapter contactAdapter;
    private Context context = this;
    ImageView bk_img;
    public int list_id;
    public String list_name;
    public StringBuilder items_Name;

    ///// Phone Dialog/////

    StringBuilder item_name;
    Dialog dialogMsg;
    TextView btn_msg_send, btn_msg_cancel;
    EditText et_number;
    String number;
    private Button syncButton;


    ////End////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_activity);


        Intent iin = getIntent();
        Bundle b = iin.getExtras();
        list_id = b.getInt("list_id");
        list_name = b.getString("List_Name");

        StaticData.list_ID=list_id;

        recyclerView = findViewById(R.id.recyclerview_send);
        et_search = findViewById(R.id.et_search);
        bk_img = findViewById(R.id.backBTN);
        syncButton = findViewById(R.id.sync_btn);
        ContactsDB.setListner(this);
        getAllContacts(null);

        et_search.setFocusable(false);

        bk_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        syncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rotateSyncButton();
                ServerSocket.sendContactsToServer();
            }
        });
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_search.setFocusableInTouchMode(true);
                et_search.setFocusable(true);
            }
        });
        if(contactAdapter!=null)
        {
            et_search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    filter(s.toString());
                }
            });
        }
        else{
            Log.i("contactSearch","Error while searching");
        }

        ApiNarrator.retrieveItems2(SendActivityRecyclerview.this,list_id);
        items_Name = new StringBuilder();
        for (int i = 0; i < ApiNarrator.smsList.size(); i++) {
     items_Name.append("\n");
            items_Name.append(ApiNarrator.smsList.get(i).getItemName());
            Log.i("aray12"," "+ApiNarrator.smsList.get(i).getItemName());

        }

        StaticData.builder=items_Name;

    }

    void filter(String text) {
        ArrayList<SyncContact> temp = new ArrayList<>();
        for (SyncContact syncContact : contactVOList) {
            if (syncContact.getName().toLowerCase().contains(text)) {
                temp.add(syncContact);
            }
        }

        contactAdapter.updateList(temp);
    }

    private void getAllContacts(ArrayList<SyncContact> syncContact) {
        ContactsDB contactsDB = new ContactsDB(context);
        if (syncContact == null) {
       //     ContactsDB contactsDB = new ContactsDB(context);
            contactVOList = contactsDB.getContacts();
        } else {
            contactVOList = syncContact;
        }
        if (contactVOList != null) {
            contactAdapter = new NumbersAdapter(contactVOList, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.addItemDecoration(new SimpleDividerItemDecoration(context));
            recyclerView.setAdapter(contactAdapter);
        } else {
            Toast.makeText(context, "Contacts not sync/Try Again", Toast.LENGTH_SHORT).show();
        }
    }


    private void email_api() {
        final Dialog emailDialog = new Dialog(this);
        emailDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        emailDialog.setContentView(R.layout.email_dialog);
        emailDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        final EditText et_email = emailDialog.findViewById(R.id.et_email_dialog);
        TextView tv_cancel = emailDialog.findViewById(R.id.btn_cancel);
        TextView tv_send = emailDialog.findViewById(R.id.btn_send);
        emailDialog.show();


        tv_cancel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                emailDialog.dismiss();
                return false;
            }
        });

        tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void fooMsgDialog() {

        item_name = new StringBuilder();

        if (ApiNarrator.smsList != null) {

            for (int i = 0; i < ApiNarrator.smsList.size(); i++) {
                item_name.append("\n");
                item_name.append(ApiNarrator.smsList.get(i).getItemName());
               /* Log.i("Array", ApiNarrator.items_name_list.get(i).getItemName());*/
                Log.i("Array2", item_name.toString());
            }

        }
        Log.i("abcd", item_name.toString());
        dialogMsg = new Dialog(this);
        dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMsg.setContentView(R.layout.msg_dialog);
        dialogMsg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        btn_msg_send = dialogMsg.findViewById(R.id.btn_send);
        btn_msg_cancel = dialogMsg.findViewById(R.id.btn_cancel);
        et_number = dialogMsg.findViewById(R.id.et_msg_dialog);
        dialogMsg.setCancelable(false);
        dialogMsg.show();

        btn_msg_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMsg.dismiss();
            }
        });

        btn_msg_send.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                number = et_number.getText().toString().trim();

                                                if (!number.isEmpty() && list_id > 0) {

                                                    ApiNarrator.shareList(Integer.parseInt(StaticData.id), number, list_id, new ForgoPasswordListener() {
                                                        @Override
                                                        public void OnSuccess(int data, int code) {

                                                            Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                                            smsIntent.setType("vnd.android-dir/mms-sms");
                                                            smsIntent.putExtra("address", number);
                                                            smsIntent.putExtra("sms_body", "List Name" + "\n" + list_name + "\n" + "Item Names" + "\n" + item_name.toString());
                                                            dialogMsg.dismiss();
                                                            startActivity(smsIntent);
                                                            Log.i("list3", "" + item_name);


                                                        }

                                                        @Override
                                                        public void OnFail(String error) {
                                                            Toast.makeText(SendActivityRecyclerview.this, "List not Sent", Toast.LENGTH_SHORT).show();
                                                        }
                                                    });


                                                } else {
                                                    et_number.setText("");
                                                    et_number.setHint("Please Enter Number Here");
                                                }


                                            }
                                        }
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ContactsDB.removeListner();
    }


    @Override
    public void onDataPopulated(final ArrayList<SyncContact> syncContacts) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getAllContacts(syncContacts);
                syncButton.clearAnimation();
            }
        });
    }

    void rotateSyncButton() {
        RotateAnimation rotate = new RotateAnimation(0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        rotate.setDuration(4000);
        rotate.setRepeatCount(Animation.INFINITE);
        rotate.setInterpolator(new LinearInterpolator());
        syncButton.startAnimation(rotate);
    }
}

