package com.app.notesapp.activities;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.app.noteapp.R;
import com.app.notesapp.fragments.PagerFragment;
import com.pixplicity.multiviewpager.MultiViewPager;

/**
 * Created by Danish on 7/24/2017.
 */

public class PagerActivity extends FragmentActivity {
    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);


        final MultiViewPager pager = findViewById(R.id.pager);

        final FragmentStatePagerAdapter adapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {

            @Override
            public int getCount() {
                return 7;
            }

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public Fragment getItem(int position) {
                fragment = PagerFragment.create(position);

                return fragment;
            }

        };
        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {

                Log.i("pagee", "" + position);
                Log.i("pageee", "" + PagerFragment.page);
                if (position==0) {
                    // ((PageFragment) fragment).img.setBackgroundColor(Color.parseColor("#000000"));
                    PagerFragment.list.get(position).setBackgroundColor(Color.parseColor("#000000"));
                    PagerFragment.list.get(1).setBackgroundColor(Color.parseColor("#ffffff"));
                    PagerFragment.list.get(2).setBackgroundColor(Color.parseColor("#ffffff"));
                    PagerFragment.list.get(3).setBackgroundColor(Color.parseColor("#ffffff"));
                    PagerFragment.list.get(4).setBackgroundColor(Color.parseColor("#ffffff"));
                }
                else if (position==1) {
                    PagerFragment.list.get(position).setBackgroundColor(Color.parseColor("#000000"));
                    PagerFragment.list.get(0).setBackgroundColor(Color.parseColor("#ffffff"));
                    PagerFragment.list.get(2).setBackgroundColor(Color.parseColor("#ffffff"));
                    PagerFragment.list.get(3).setBackgroundColor(Color.parseColor("#ffffff"));
                   PagerFragment.list.get(4).setBackgroundColor(Color.parseColor("#ffffff"));
                }
                else if(position==2)
                {
                    PagerFragment.list.get(position).setBackgroundColor(Color.parseColor("#000000"));
                    PagerFragment.list.get(0).setBackgroundColor(Color.parseColor("#ffffff"));
                    PagerFragment.list.get(1).setBackgroundColor(Color.parseColor("#ffffff"));
                    PagerFragment.list.get(3).setBackgroundColor(Color.parseColor("#ffffff"));
                    PagerFragment.list.get(4).setBackgroundColor(Color.parseColor("#ffffff"));
                }
                else if(position==3)
                {
                    PagerFragment.list.get(position).setBackgroundColor(Color.parseColor("#000000"));
                    PagerFragment.list.get(0).setBackgroundColor(Color.parseColor("#ffffff"));
                    PagerFragment.list.get(1).setBackgroundColor(Color.parseColor("#ffffff"));
                    PagerFragment.list.get(2).setBackgroundColor(Color.parseColor("#ffffff"));
                    PagerFragment.list.get(4).setBackgroundColor(Color.parseColor("#ffffff"));
                }
                else if(PagerFragment.page==4)
                {
                    PagerFragment.list.get(4).setBackgroundColor(Color.parseColor("#000000"));
                    PagerFragment.list.get(0).setBackgroundColor(Color.parseColor("#ffffff"));
                    PagerFragment.list.get(2).setBackgroundColor(Color.parseColor("#ffffff"));
                    PagerFragment.list.get(3).setBackgroundColor(Color.parseColor("#ffffff"));
                    PagerFragment.list.get(1).setBackgroundColor(Color.parseColor("#ffffff"));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

}