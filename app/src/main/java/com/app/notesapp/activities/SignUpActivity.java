package com.app.notesapp.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.noteapp.R;
import com.app.notesapp.api.ApiNarrator;
import com.app.notesapp.enums.AuthType;
import com.app.notesapp.models.Auth;
import com.app.notesapp.models.User;
import com.app.notesapp.utils.CommonUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SignUpActivity extends Activity implements ApiNarrator.OnAuthListener {
    EditText name, password, confirmPassword, email;
    TextView signUpBtn;
    EditText dateofbirth;
    private SimpleDateFormat dateFormatter;
    private DatePickerDialog dialog;
    Date date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        initViews();
        setDatePicker();
        getPlayStoreAccount();
    }

    private void initViews() {
        name = findViewById(R.id.name);
        password = findViewById(R.id.password);
        confirmPassword = findViewById(R.id.confirmPassword);
        email = findViewById(R.id.email);
        signUpBtn = findViewById(R.id.signUp);
        dateofbirth = findViewById(R.id.datePicker);
        dateofbirth.setInputType(InputType.TYPE_NULL);
        dateofbirth.requestFocus();

        name.setTypeface(CommonUtils.getInstance().getTypefaceBold(this), Typeface.NORMAL);
        password.setTypeface(CommonUtils.getInstance().getTypefaceBold(this), Typeface.NORMAL);
        email.setTypeface(CommonUtils.getInstance().getTypefaceBold(this), Typeface.NORMAL);
        confirmPassword.setTypeface(CommonUtils.getInstance().getTypefaceBold(this), Typeface.NORMAL);
        signUpBtn.setTypeface(CommonUtils.getInstance().getTypeface(this));
    }

    private void getPlayStoreAccount() {
        String possibleEmail = "";
        try {
            Account[] accounts =
                    AccountManager.get(this).getAccountsByType("com.google");

            for (Account account : accounts) {

                possibleEmail = account.name;

            }
        } catch (Exception e) {
            Log.i("Exception", "Exception:" + e);
        }
        email.setText(possibleEmail);
    }

    public void doSignUP(View v) {
        String nameS = name.getText().toString();
        String emailS = email.getText().toString().trim();
        String passwordS = password.getText().toString();
        String confirm = confirmPassword.getText().toString();
        String dob = dateofbirth.getText().toString();

        if (nameS.isEmpty()) {
            name.setHint("NAME REQUIRED");
        }
        if (emailS.isEmpty()) {
            email.setHint(" EMAIL REQUIRED");
        }
        if (dob.isEmpty()) {
            dateofbirth.setHint("DATE OF BIRTH REQUIRED");
        }

        if (passwordS.isEmpty()) {
            password.setHint("PASSWORD REQUIRED");
        } else if (confirm.isEmpty()) {
            confirmPassword.setHint("ENTER PASSWORD AGAIN");
        } else if (!confirm.equals(passwordS)) {
            confirmPassword.setText("");
            confirmPassword.setHint("Password didn't match".toUpperCase());
        }


        if (emailS.length() > 0 && nameS.length() > 0 && confirm.equals(passwordS) && dob.length() > 0) {

            if (android.util.Patterns.EMAIL_ADDRESS.matcher(emailS).matches()) {
                User user = new User();
                user.email = emailS;
                user.password = passwordS;
                user.name = nameS;
                user.dob = dob;
                user.phone = getIntent().getStringExtra("phone");
                // Log.i("phone", user.phone);
                signUpBtn.setText("signing up...".toUpperCase());
                ApiNarrator.auth(this, user, AuthType.REGISTER);
            } else {
                email.setText("");
                email.setHint("Email Address format is wrong");
            }

        }


    }

    private void updateUIOnFail(String error) {
        signUpBtn.setText(error.toUpperCase());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                signUpBtn.setText("sign up".toUpperCase());
            }
        }, 3000);

    }

    @Override
    public void onAuthSuccess(Auth auth) {
        if (auth.response.data == 1) {
            signUpBtn.setText("success".toUpperCase());
            Auth.saveUser(this, auth.response.user);
            onBackPressed();
            startActivity(new Intent(this, MainActivity.class));
        } else {
            updateUIOnFail("user already exists");
        }

    }

    @Override
    public void OnError(String error) {
        updateUIOnFail(error);

    }

    public void setDatePicker() {
        Calendar newCalendar = Calendar.getInstance();

        dialog = new DatePickerDialog(this, new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                try {
                    date = dateFormatter.parse("30-12-2017");
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                newDate.set(year, monthOfYear, dayOfMonth);
                if (newDate.after(date)) {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                } else {
                    dateofbirth.setText(dateFormatter.format(newDate.getTime()));
                }


            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        dateofbirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                dialog.show();
            }
        });
    }
}
