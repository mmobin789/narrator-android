package com.app.notesapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.noteapp.R;
import com.app.notesapp.api.ApiNarrator;
import com.app.notesapp.interfaces.ForgoPasswordListener;

/**
 * Created by Danish on 8/11/2017.
 */

public class ChangePasswordActivity extends Activity {

    EditText et_pass;
    TextView submitBtn;
    int userId;
    int check_data;
    int check_code;
    ImageView back_img;
    String password;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_newpassword);
        Intent iin = getIntent();
        Bundle b = iin.getExtras();
        userId=b.getInt("uid");

        Log.i("user_id","abc"+userId);
        et_pass=findViewById(R.id.et_password);
        submitBtn=findViewById(R.id.submitBtn);
        back_img=findViewById(R.id.backbtn);


        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

         password=et_pass.getText().toString();

                if(!password.isEmpty())
                {
                    ApiNarrator.forgotpass_3(userId,password, new ForgoPasswordListener() {
                        @Override
                        public void OnSuccess(int data, int code) {

                            check_data=data;
                            check_code=code;

                            if(check_code==200&&check_data==1)
                            {
                                et_pass.setText("");
                                et_pass.setHint("Password Updated");
                            }
                            Intent inn=new Intent(ChangePasswordActivity.this,LoginActivity.class);
                            startActivity(inn);
                        }

                        @Override
                        public void OnFail(String error) {

                        }
                    });

                    Log.i("Password",password);
                }
                else {
                    et_pass.setText("");
                    et_pass.setHint("Password is Required");
                }

            }
        });

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }
}
