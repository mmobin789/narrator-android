package com.app.notesapp.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.noteapp.R;
import com.app.notesapp.Recievers.NetworkChangeReceiver;
import com.app.notesapp.api.ApiNarrator;
import com.app.notesapp.enums.AuthType;
import com.app.notesapp.enums.LoginType;
import com.app.notesapp.interfaces.ForgoPasswordListener;
import com.app.notesapp.interfaces.OnSocialLoginListener;
import com.app.notesapp.models.Auth;
import com.app.notesapp.models.SocialUser;
import com.app.notesapp.models.User;
import com.app.notesapp.utils.CommonUtils;
import com.app.notesapp.utils.StaticData;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class LoginActivity extends FragmentActivity implements ApiNarrator.OnPhoneVerifyListener, GoogleApiClient.OnConnectionFailedListener, OnSocialLoginListener,
        GoogleApiClient.ConnectionCallbacks,
        LocationListener {

    EditText countryCodeEDT, phoneNumberEDT, passwordEDT, verifyEDT;
    LinearLayout keyboard1LL, keyboard2LL, keyboard3LL, keyboard4LL, keyboard5LL, keyboard6LL,
            keyboard7LL, keyboard8LL, keyboard9LL, keyboard0LL, keyboardnextLL, keyboardbackLL,
            customeKeyBoardLL;
    TextView loginLabel, signUpLabel, verify, reg, bodyTextTv, appNameTV, num9TV, num8TV, num7TV, num6TV, num5TV, num4TV, num3TV, num2TV, num1TV, num0TV;
    public static TextView login;

    boolean keyboardStatus = false;
    TextView btn_forgot;
    String phoneNumberText = "", phoneNumberCode = "";
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;
    static RelativeLayout internetcon;
    Dialog forgot_dialog;
    TextView tv_forgot, tv_cancel_dialog;
    EditText et_forgot;
    Handler handler;
    String countryCode;
    String countryName;
    String phone;
    String fullPhoneNo;
    String CountryZipCode = "";
    LocationManager locationManager;
    private BroadcastReceiver receiver;
    boolean phone_check;
    private static final int PERMISSION_REQUEST_CODE = 10;
    /////////////

    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    ////////////
    private BroadcastReceiver mNetworkReceiver;

    public void login(View v) {
        showSignUPUI(false);
        if (passwordEDT.getText().toString().isEmpty()) {
            passwordEDT.setHint("PASSWORD REQUIRED");
        }
        if (phoneNumberEDT.getText().toString().isEmpty()) {
            phoneNumberEDT.setHint("PHONE NUMBER REQUIRED");
        }
        if (countryCodeEDT.getText().toString().isEmpty()) {

            countryCodeEDT.setHint("COUNTRY REQUIRED");
        }
        if (passwordEDT.length() > 0 && phoneNumberEDT.length() > 0 && countryCodeEDT.length() > 0) {
            login.setText("LOGGING IN...");
            String phone = CountryZipCode + phoneNumberEDT.getText().toString();
            String password = passwordEDT.getText().toString();
            ApiNarrator.auth(new ApiNarrator.OnAuthListener() {
                @Override
                public void onAuthSuccess(Auth auth) {
                    if (auth.response.data == 1) {
                        login.setText("SUCCESS");
                        Auth.saveUser(LoginActivity.this, auth.response.user);
                        finish();
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    } else if (auth.response.data == 0) {
                        login.setText("Login Failed");
                    } else {
                        updateUIOnFail("INVALID CREDENTIALS");

                    }
                }

                @Override
                public void OnError(String error) {
                    updateUIOnFail(error);
                }
            }, new User(password, phone), AuthType.LOGIN);
        }


    }

    private void loginWithPhone(final ApiNarrator.OnPhoneVerifyListener listener, PhoneAuthCredential credential) {

        FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    verify.setText("success".toUpperCase());
                    listener.OnPhoneVerificationSuccess(task.getResult().getUser().getPhoneNumber());
                } else {
                    listener.OnPhoneVerificationFailed("Code Expired or Wrong Code".toUpperCase());
                }
            }


        });

    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            loginWithPhone(LoginActivity.this, phoneAuthCredential);
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            LoginActivity.this.OnPhoneVerificationFailed(e.toString());
        }

        @Override
        public void onCodeSent(String verificationID, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            LoginActivity.this.verificationID = verificationID;
            showVerifyUI(true);
            reg.setText("CODE SENT");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    reg.setText("resend code".toUpperCase());
                }
            }, 3000);
        }
    };

    public void back2Login(View v) {
        showLoginUI(true);
        passwordEDT.setVisibility(View.VISIBLE);
        findViewById(R.id.password_line).setVisibility(View.VISIBLE);
        login.setVisibility(View.VISIBLE);
        signUpLabel.setVisibility(View.VISIBLE);
        loginLabel.setVisibility(View.GONE);
        showSignUPUI(false);


    }

    public void reg(View v) {


        phoneNumberEDT.setHint("Enter Phone Number");
        phone = phoneNumberEDT.getText().toString();
        if (TextUtils.isEmpty(phoneNumberCode)) {
            countryCodeEDT.setHint("Choose your Country");


        }
        if (phone.isEmpty()) {
            phoneNumberEDT.setHint("Phone Number Required");

        }
        if (CountryZipCode.length() > 0 && phone.length() > 0) {
            fullPhoneNo = CountryZipCode + phone;

            Log.i("fullphone", fullPhoneNo);
            fun_checkNumber(fullPhoneNo);


        }

    }

    String verificationID = "";


    public void verify(View v) {


        if (verifyEDT.getText().toString().length() == 6) {
            verify.setText("verifying...".toUpperCase());
            String code = verifyEDT.getText().toString();
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationID, code);
            loginWithPhone(LoginActivity.this, credential);
        } else if (TextUtils.isEmpty(verifyEDT.getText())) {

            verifyEDT.setText("");
            verifyEDT.setHint("Required".toUpperCase());
        } else {
            verifyEDT.setText("");
            verifyEDT.setHint("Invalid".toUpperCase());
        }

    }

    private void showVerifyUI(boolean show) {
        int i;
        if (show)
            i = View.VISIBLE;
        else
            i = View.GONE;

        findViewById(R.id.verifyUI).setVisibility(i);
    }

    CallbackManager callbackManager;


    GoogleApiClient googleApiClient;

    public void loginGoogle(View v) {
        login.setText("logging in...".toUpperCase());
        loginType = LoginType.GOOGLE;
        Intent signInIntent = com.google.android.gms.auth.api.Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, 34);
    }

    private void initGoogleAuth() {
        // Configure sign-in to request the user's ID, email address, and basic
// profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(com.google.android.gms.auth.api.Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    TwitterAuthClient authClient;

    public void loginTwitter(View v) {
        login.setText("logging in...".toUpperCase());
        Twitter.initialize(this);
        loginType = LoginType.TWITTER;
        authClient = new TwitterAuthClient();
        authClient.authorize(this, new Callback<TwitterSession>() {
            @Override
            public void success(final Result<TwitterSession> resultT) {

                authClient.requestEmail(resultT.data, new Callback<String>() {
                    @Override
                    public void success(Result<String> result) {
                        SocialUser socialUser = new SocialUser();
                        socialUser.first_name = resultT.data.getUserName();
                        socialUser.id = resultT.data.getUserId() + "";
                        socialUser.email = result.data;


                        LoginActivity.this.OnSocialLoginSuccess(socialUser);
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        LoginActivity.this.OnSocialLoginError(exception.toString());
                    }
                });


            }

            @Override
            public void failure(TwitterException exception) {
                Log.e("twitterError", exception.getMessage());
                LoginActivity.this.OnSocialLoginError(exception.getMessage());

            }
        });
    }


    // Bundle bundle = new Bundle();
    //String id = null;

//
//        try {
//            id = object.getString("id");
//            bundle.putString("idFacebook", id);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        try {
//            URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
//            Log.i("profile_pic", profile_pic + "");
//            bundle.putString("profile_pic", profile_pic.toString());
//
//
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
//
//        if (object.has("first_name"))
//            try {
//                bundle.putString("first_name", object.getString("first_name"));
//                if (object.has("last_name"))
//                    bundle.putString("last_name", object.getString("last_name"));
//                if (object.has("email"))
//                    bundle.putString("email", object.getString("email"));
//                if (object.has("gender"))
//                    bundle.putString("gender", object.getString("gender"));
//                if (object.has("birthday"))
//                    bundle.putString("birthday", object.getString("birthday"));
//                if (object.has("location"))
//                    bundle.putString("location", object.getJSONObject("location").getString("name"));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }


    private void initFBLogin(final OnSocialLoginListener loginListener) {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                //     Toast.makeText(LoginActivity.this, loginResult.getAccessToken().getUserId(), Toast.LENGTH_SHORT).show();
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        SocialUser user = new Gson().fromJson(object + "", SocialUser.class);
                        loginListener.OnSocialLoginSuccess(user);


                    }
                });
                Bundle params = new Bundle();
                params.putString("fields", "id, first_name, last_name, email,gender, birthday, location");
                graphRequest.setParameters(params);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Log.e("fbError", error.getMessage());
                loginListener.OnSocialLoginError(error.getMessage());

            }
        });
    }

    public void loginFB(View v) {
        loginType = LoginType.FACEBOOK;
        login.setText("logging in...".toUpperCase());
        initFBLogin(this);

        LoginManager.getInstance().logInWithReadPermissions(
                this,
                Arrays.asList("public_profile", "email")
        );


    }

    LoginType loginType;

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void OnSocialLoginSuccess(SocialUser user) {
        User.socialUser = user;
        ApiNarrator.auth(new ApiNarrator.OnAuthListener() {
            @Override
            public void onAuthSuccess(Auth auth) {
                String msg = "";
                User.socialUser.id = auth.response.data + "";
                finish();
                if (auth.response.data == -1) {
                    msg = "Welcome Back".toUpperCase();
                } else {
                    msg = "Welcome to CrossOFF".toUpperCase();
                }
                Auth.saveUser(LoginActivity.this, null);
                login.setText(msg);
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }

            @Override
            public void OnError(String error) {
                updateUIOnFail(error);

            }
        }, null, AuthType.SOCIAL);


    }

    @Override
    public void OnSocialLoginError(String error) {

        updateUIOnFail(error);


    }

    private void updateUIOnFail(String error) {
        login.setText(error);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                login.setText("login".toUpperCase());
            }
        }, 3000);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (loginType) {
            case FACEBOOK:
                callbackManager.onActivityResult(requestCode, resultCode, data);
                break;
            case TWITTER:
                //     twitterLoginButton.onActivityResult(requestCode,resultCode,data);
                authClient.onActivityResult(requestCode, resultCode, data);
                break;
            case GOOGLE:
                GoogleSignInResult result = com.google.android.gms.auth.api.Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleGoogleLoginResult(this, result);
                break;

        }
    }

    private void handleGoogleLoginResult(OnSocialLoginListener loginListener, GoogleSignInResult result) {
        if (result.isSuccess()) {
            SocialUser socialUser = new SocialUser();
            socialUser.email = result.getSignInAccount().getEmail();
            socialUser.first_name = result.getSignInAccount().getDisplayName();
            socialUser.id = result.getSignInAccount().getId();
            loginListener.OnSocialLoginSuccess(socialUser);

        } else {
            loginListener.OnSocialLoginError("Login Failed".toUpperCase());
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
        if (!CommonUtils.isGPSEnable(this)) {
            Toast.makeText(this, "Enable GPS to detect country", Toast.LENGTH_LONG).show();
            CommonUtils.vibrate(this, 500);
        }
    }

    private void initViews() {
        initGoogleAuth();
        Spanned text = Html.fromHtml("<center>We will now send a one-time SMS<br> message to verify your phone <br>number. Carrier charges may apply<br> <br>Please confirm your country code,<br> then enter your phone number.</center>");

        internetcon = findViewById(R.id.internet);

        internetcon.setVisibility(View.GONE);

        mNetworkReceiver = new NetworkChangeReceiver();
        registerNetworkBroadcastForNougat();
        Sms_Reciever();


        //Location //

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds


        ////End
        countryCodeEDT = findViewById(R.id.countryCodeEDT);
        ((TextView) findViewById(R.id.socialLabel)).setTypeface(CommonUtils.getInstance().getTypefaceBold(LoginActivity.this), Typeface.NORMAL);
        countryCodeEDT.setTypeface(CommonUtils.getInstance().getTypefaceBold(LoginActivity.this), Typeface.NORMAL);
        CommonUtils.setFontAwesome(new View[]{findViewById(R.id.fb), findViewById(R.id.google), findViewById(R.id.twitter)});
        phoneNumberEDT = findViewById(R.id.phoneNumberEDT);
        passwordEDT = findViewById(R.id.password);
        signUpLabel = findViewById(R.id.label);
        loginLabel = findViewById(R.id.label2);
        tv_forgot = findViewById(R.id.tv_forgot);
        tv_forgot.setTypeface(CommonUtils.getInstance().getTypeface(LoginActivity.this));

        SpannableStringBuilder tv_fogrot_span = CommonUtils.forgotBoldText(this, tv_forgot.getText().toString(), 18, tv_forgot.length(), R.color.loginbutton);
        SpannableStringBuilder stringBuilder = CommonUtils.getBoldText(this, signUpLabel.getText().toString(), 24, signUpLabel.length(), R.color.registerBarcolor);
        stringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                login.setVisibility(View.GONE);
                passwordEDT.setVisibility(View.GONE);
                signUpLabel.setVisibility(View.GONE);
                findViewById(R.id.password_line).setVisibility(View.GONE);
                loginLabel.setVisibility(View.VISIBLE);

                showSignUPUI(true);
            }


            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        }, 24, signUpLabel.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


        tv_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(in);
              /*  forgot_dialog = new Dialog(LoginActivity.this);
                forgot_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                forgot_dialog.setContentView(R.layout.dialog_forgot);
                forgot_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                et_forgot=(EditText)forgot_dialog.findViewById(R.id.et_forgot);
                btn_forgot = (TextView) forgot_dialog.findViewById(R.id.tv_forgot);
                tv_cancel_dialog=(TextView)forgot_dialog.findViewById(R.id.tv_cancel_dialog);
                forgot_dialog.setCancelable(false);
                forgot_dialog.show();

                btn_forgot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(android.util.Patterns.EMAIL_ADDRESS.matcher(et_forgot.getText().toString()).matches())
                        {
                         btn_forgot.setText("Success");

                            foo_thread("Submit");

                        }
                        else if(et_forgot.getText().toString().isEmpty())
                        {
                            et_forgot.setHint("Email Address Required");

                            foo_thread("Enter Email Address");

                        }

                        else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(et_forgot.getText().toString()).matches()){
                            et_forgot.setText("");
                            et_forgot.setHint("Enter Valid Email Address");

                            foo_thread("Enter Email Address");
                        }

                    }
                });

                tv_cancel_dialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        forgot_dialog.dismiss();
                    }
                });
*/
            }
        });


        signUpLabel.setMovementMethod(LinkMovementMethod.getInstance());
        signUpLabel.setText(stringBuilder);
        verify = findViewById(R.id.verify);
        verifyEDT = findViewById(R.id.code);
        login = findViewById(R.id.login);
        reg = findViewById(R.id.reg);
        verify.setTypeface(CommonUtils.getInstance().getTypeface(LoginActivity.this));
        verifyEDT.setTypeface(CommonUtils.getInstance().getTypefaceBold(LoginActivity.this), Typeface.NORMAL);
        passwordEDT.setTypeface(CommonUtils.getInstance().getTypefaceBold(LoginActivity.this), Typeface.NORMAL);
        phoneNumberEDT.setTypeface(CommonUtils.getInstance().getTypefaceBold(LoginActivity.this), Typeface.NORMAL);
        keyboard1LL = findViewById(R.id.keyboard1LL);
        keyboard2LL = findViewById(R.id.keyboard2LL);
        keyboard3LL = findViewById(R.id.keyboard3LL);
        keyboard4LL = findViewById(R.id.keyboard4LL);
        keyboard5LL = findViewById(R.id.keyboard5LL);
        keyboard6LL = findViewById(R.id.keyboard6LL);
        keyboard7LL = findViewById(R.id.keyboard7LL);
        keyboard8LL = findViewById(R.id.keyboard8LL);
        keyboard9LL = findViewById(R.id.keyboard9LL);
        keyboard0LL = findViewById(R.id.keyboard0LL);
        keyboardbackLL = findViewById(R.id.keyboardbackLL);
        keyboardnextLL = findViewById(R.id.keyboardnextLL);
        num0TV = findViewById(R.id.num0TV);
        num0TV.setTypeface(CommonUtils.getInstance().getKeyboardTypeface(LoginActivity.this));
        num1TV = findViewById(R.id.num1TV);
        num1TV.setTypeface(CommonUtils.getInstance().getKeyboardTypeface(LoginActivity.this));
        num2TV = findViewById(R.id.num2TV);
        num2TV.setTypeface(CommonUtils.getInstance().getKeyboardTypeface(LoginActivity.this));
        num3TV = findViewById(R.id.num3TV);
        num3TV.setTypeface(CommonUtils.getInstance().getKeyboardTypeface(LoginActivity.this));
        num4TV = findViewById(R.id.num4TV);
        num4TV.setTypeface(CommonUtils.getInstance().getKeyboardTypeface(LoginActivity.this));
        num5TV = findViewById(R.id.num5TV);
        num5TV.setTypeface(CommonUtils.getInstance().getKeyboardTypeface(LoginActivity.this));
        num6TV = findViewById(R.id.num6TV);
        num6TV.setTypeface(CommonUtils.getInstance().getKeyboardTypeface(LoginActivity.this));
        num7TV = findViewById(R.id.num7TV);
        num7TV.setTypeface(CommonUtils.getInstance().getKeyboardTypeface(LoginActivity.this));
        num8TV = findViewById(R.id.num8TV);
        num8TV.setTypeface(CommonUtils.getInstance().getKeyboardTypeface(LoginActivity.this));
        num9TV = findViewById(R.id.num9TV);
        num9TV.setTypeface(CommonUtils.getInstance().getKeyboardTypeface(LoginActivity.this));

        //appNameTV = (TextView) findViewById(R.id.appNameTV);
        // appNameTV.setTypeface(CommonUtils.getInstance().getTypeface(LoginActivity.this));

        bodyTextTv = findViewById(R.id.bodyTextTv);
        login.setTypeface(CommonUtils.getInstance().getTypeface(LoginActivity.this));
        reg.setTypeface(CommonUtils.getInstance().getTypeface(LoginActivity.this));
        signUpLabel.setTypeface(CommonUtils.getInstance().getTypeface(LoginActivity.this));
        loginLabel.setTypeface(CommonUtils.getInstance().getTypeface(LoginActivity.this));
        bodyTextTv.setTypeface(CommonUtils.getInstance().getTypeface(LoginActivity.this));
        bodyTextTv.setText(text);

        customeKeyBoardLL = findViewById(R.id.customeKeyBoardLL);
        customeKeyBoardLL.setVisibility(View.GONE);

        keyboard1LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addText("1");
            }
        });
        keyboard2LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addText("2");
            }
        });
        keyboard3LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addText("3");
            }
        });
        keyboard4LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addText("4");
            }
        });
        keyboard5LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addText("5");
            }
        });
        keyboard6LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addText("6");
            }
        });
        keyboard7LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addText("7");
            }
        });
        keyboard8LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addText("8");
            }
        });
        keyboard9LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addText("9");
            }
        });
        keyboard0LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addText("0");
            }
        });
        keyboardbackLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeText();
            }
        });
        keyboardbackLL.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                phoneNumberEDT.getText().clear();
                return false;
            }
        });
        keyboardnextLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (phoneNumberEDT.getText().toString() != null && phoneNumberText != null && phoneNumberText.length() > 2 && phoneNumberEDT.getText().toString().length() > 2) {
                    CommonUtils.getInstance(LoginActivity.this).setSharedPerf(StaticData.USER_DATA.USER_LOGIN_STATUS, "LOGIN_TRUE");
                    CommonUtils.getInstance(LoginActivity.this).setSharedPerf(StaticData.USER_DATA.USER_NUMBER, "(" + CountryZipCode + ") " + phoneNumberText);
                    //  CommonUtils.getInstance(LoginActivity.this).setSharedPerf(StaticData.USER_DATA.USER_EMAIL, ""+CommonUtils.getInstance().getEmail(LoginActivity.this));
                    // CommonUtils.getInstance(LoginActivity.this).setSharedPerf(StaticData.USER_DATA.USER_NAME, "" + CommonUtils.getInstance().getEmailName(LoginActivity.this));

                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                } else {
                    Toast.makeText(LoginActivity.this, "Please Enter Complete Data..!", Toast.LENGTH_LONG).show();
                }
            }
        });

     /*   countryCodeEDT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //hideSoftKeyboard(countryCodeEDT);
                showCountryCodes();
                // animateViewOut(true);
            }
        });*/
        phoneNumberEDT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //hideSoftKeyboard(phoneNumberEDT);
                animateViewOut(true);
            }
        });

        passwordEDT.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    login.performClick();
                }
                return false;
            }
        });

       /* countryCodeEDT.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                countryCodeEDT.setSelection(countryCodeEDT.getText().length());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        phoneNumberEDT.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                phoneNumberEDT.setSelection(phoneNumberEDT.getText().length());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });*/


        verifyEDT.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });
    }

    /**
     * Hides the soft keyboard
     */
    public void foo_thread(final String validation) {
        handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                if (validation.contains("Submit")) {
                    btn_forgot.setText(validation);
                } else {
                    et_forgot.setHint(validation);
                }

            }
        };
        handler.postDelayed(r, 2000);
    }

    public void hideSoftKeyboard(View view) {

        InputMethodManager inputMethodManager = (InputMethodManager) LoginActivity.this.getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void addText(String add) {
        Log.e("ADD NOW SELECTION 1: ", "" + phoneNumberEDT.getSelectionStart());

        if (phoneNumberText.length() < 15) {

            phoneNumberText = phoneNumberText + add;
            phoneNumberEDT.setText(phoneNumberText);

        }
    }

    private void removeText() {

        if (phoneNumberText != null && phoneNumberText.length() > 0) {
            phoneNumberText = phoneNumberText.substring(0, phoneNumberText.length() - 1);
            phoneNumberEDT.setText(phoneNumberText);
        }
    }

    private void showSignUPUI(boolean show) {
        int i;
        if (show)
            i = View.VISIBLE;
        else
            i = View.GONE;

        reg.setVisibility(i);
    }

    /**
     * Shows the soft keyboard
     */
    public void showSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }

    private void animateViewOut(boolean status) {

        if (status && !keyboardStatus) {
            //   customeKeyBoardLL.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.FadeInUp)
                    .duration(100)
                    .playOn(customeKeyBoardLL);
            keyboardStatus = true;
        } else if (!status && keyboardStatus) {
            YoYo.with(Techniques.FadeOutDown)
                    .duration(100)
                    .playOn(customeKeyBoardLL);
            keyboardStatus = false;
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            animateViewOut(false);
            Toast.makeText(getBaseContext(), "Press once again to exit!",
                    Toast.LENGTH_SHORT).show();
        }
        back_pressed = System.currentTimeMillis();
    }


    //----------------------SHOW COUNTY CODES--------------
    public void showCountryCodes() {

        final String[] countryCode = getResources().getStringArray(R.array.country_codes);
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this, R.style.MyAlertDialogStyle);
        builder.setTitle("Select Country");

        builder.setItems(countryCode, new DialogInterface.OnClickListener() {
            // @Override
            public void onClick(DialogInterface dialog, int which) {
                String text = CommonUtils.getInstance().getCountryName(countryCode[which]);
                phoneNumberCode = CommonUtils.getInstance().getCountryCode(countryCode[which]);
                if (text.length() > 20) {
                    text = text.substring(0, 17);
                    text = text + "...";
                }
                countryCodeEDT.getText().clear();
                countryCodeEDT.setText(text);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    // @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void showLoginUI(boolean show) {
        int i;
        if (show)
            i = View.VISIBLE;
        else
            i = View.GONE;

        findViewById(R.id.loginUI).setVisibility(i);
    }

    @Override
    public void OnPhoneVerificationSuccess(String userPhoneNo) {
        finish();
        Toast.makeText(this, userPhoneNo, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, SignUpActivity.class);
        intent.putExtra("phone", userPhoneNo);
        startActivity(intent);


    }

    @Override
    public void OnPhoneVerificationFailed(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        verify.setText("FAILED");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                verify.setText("VERIFY");
            }
        }, 3000);

    }
    //----------------------SHOW COUNTY CODES--------------

    public static void dialog(boolean value) {

        if (value) {

            internetcon.setVisibility(View.GONE);
        } else {
            internetcon.setVisibility(View.VISIBLE);
        }

    }

    private void registerNetworkBroadcastForNougat() {
        registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    protected void unregisterNetworkChanges() {
        unregisterReceiver(mNetworkReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    public void fun_checkNumber(final String number) {
        ApiNarrator.check_phone(number, new ForgoPasswordListener() {
            @Override
            public void OnSuccess(int data, int code) {

                Log.i("data ", " " + data);
                if (data == 0) {
                    reg.setText("Sending Code...");
                    showLoginUI(false);

                    PhoneAuthProvider.getInstance().verifyPhoneNumber(fullPhoneNo, 60, TimeUnit.SECONDS, LoginActivity.this, callbacks);
                }
                if (data == 1) {
                    showVerifyUI(false);
                    phoneNumberEDT.setText("");
                    reg.setText("User Exist");
                }
            }

            @Override
            public void OnFail(String error) {

            }

        });

    }

    public void Sms_Reciever() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase("otp")) {
                    final String message = intent.getStringExtra("message");
                    verifyEDT.setText(message);


                    verify.performClick();

                    Log.i("message", "abc" + message);

                }
            }
        };
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

        //Disconnect from API onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, LoginActivity.this);
            mGoogleApiClient.disconnect();
        }
    }

    public String getCountryZipCode(String country) {


        String[] rl = this.getResources().getStringArray(R.array.country_codes);
        for (String aRl : rl) {
            String[] g = aRl.split(",");
            if (g[1].trim().equals(country)) {
                CountryZipCode = "+" + g[0];
                break;
            }
        }
        //  fullPhoneNo = CountryZipCode + phone;
        Log.i("Zip Code", " " + fullPhoneNo);
        return CountryZipCode;
    }

    @Override
    public void onLocationChanged(Location location) {
        getCountry(location);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, LoginActivity.this);

        } else {
            getCountry(location);
        }

    }

    private void getCountry(Location location) {
        try {
            Geocoder gcd = new Geocoder(LoginActivity.this, Locale.getDefault());
            List<Address> addresses = gcd.getFromLocation(location.getLatitude(),
                    location.getLongitude(), 1);
            if (addresses.size() > 0) {
                countryName = addresses.get(0).getCountryName();
                countryCode = addresses.get(0).getCountryCode();
                getCountryZipCode(countryCode);
                countryCodeEDT.setText(countryName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}