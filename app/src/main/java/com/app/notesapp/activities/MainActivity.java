package com.app.notesapp.activities;

import android.app.Dialog;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.noteapp.R;
import com.app.notesapp.Sockets.ServerSocket;
import com.app.notesapp.Sockets.SocketService;
import com.app.notesapp.adapter.MainActivityAdapter;
import com.app.notesapp.api.ApiNarrator;
import com.app.notesapp.interfaces.BirthdayListener;
import com.app.notesapp.interfaces.ConnectionListner;
import com.app.notesapp.interfaces.OnCustomTouchListener;
import com.app.notesapp.interfaces.OnSwipeTouchListener;
import com.app.notesapp.interfaces.SocketListener;
import com.app.notesapp.models.Auth;
import com.app.notesapp.models.MainActivityList;
import com.app.notesapp.models.StrikeItem;
import com.app.notesapp.models.SyncContact;
import com.app.notesapp.utils.CommonUtils;
import com.app.notesapp.utils.DialogBoxTimer;
import com.app.notesapp.utils.GifImageView;
import com.app.notesapp.utils.StaticData;
import com.bumptech.glide.Glide;
import com.github.pwittchen.swipe.library.Swipe;
import com.github.pwittchen.swipe.library.SwipeListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.skyfishjy.library.RippleBackground;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;

public class MainActivity extends BaseActivity implements RecognitionListener, ConnectionListner, SocketListener {

    public static final String PREFS_NAME = "Narator";
    public static final String PREFS_KEY = "txtSize";
    public static final String PREFS_KEY_2 = "texttype";
    public static final String PREFS_KEY_3 = "micBTNCheck";
    public static final String PREFS_KEY_4 = "timerCheck";
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;
    private static LinearLayout rel_main_nav;
    public LinearLayout relTextSize, relSpeed, relTwitter, relInstagram, relFb, relSizeContainer, relSpeedContainer;
    public LinearLayout relMic, relKeyboard, edittextContainer;
    public int timer = 2;
    //  private ListView mDrawerList;
    protected View leftMenu;
    int var = 0;
    boolean onResult = false;
    // used to store app title
    pl.droidsonroids.gif.GifImageView gif;
    pl.droidsonroids.gif.GifImageView gif2;
    boolean check = true;
    boolean check2 = true;
    Context context = this;
    TextView txt_size;
    // declare properties
    private DrawerLayout mDrawerLayout;
    private LinearLayout audioOverLayLL;
    private TextView toptextmainTV;
    private EditText subjectNameTV;
    private ImageView micBTN;
    private SpeechRecognizer speech = null;
    private Intent recognizerIntent;
    private ActionBarDrawerToggle mDrawerToggle;
//    private BroadcastReceiver mNetworkReceiver;
    private Swipe swipe;
    private RelativeLayout internetcon;
    private RippleBackground rippleBackground;
    private LinearLayout bottomPanel;
    private Dialog waiting;
    private RecyclerView mainactivityrecycler;
    private Dialog dialog_birthday;
    private TextView cancel_dialog;
    private ImageView img_birthday;
    private GifImageView gifImageView1,gifImageView2;
    private MainActivityAdapter adp;
    private TextView txtSize, txtSpeed;
    private int micCheck, timerCheck;
    private boolean sendContacts = true;
    ArrayList<MainActivityList> contactVOList;
    boolean listCheck=false;
    String listName="";
    boolean fooListCheck=false;
    boolean clickCheck=true;
    View line;
    public static String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }

    public static int getFontSize(Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(PREFS_KEY, 12);

    }

    public static String getFontText(Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(PREFS_KEY_2, "Small");

    }

    public static int getMic(Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(PREFS_KEY_3, 1);

    }

    public static int getTimer(Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(PREFS_KEY_4, 2);

    }

    public void logOut(View v) {
        Auth.clearLogin(this);
        finish();
        startActivity(new Intent(this, LoginActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setConnectionListner(this);
        swipeListener();


     //   getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        internetcon = findViewById(R.id.internet);
        bottomPanel = findViewById(R.id.bottomPanel);
        rel_main_nav = findViewById(R.id.rel_main_nav);
        ImageView imageView=findViewById(R.id.opengif);
        Glide.with(this)
                .asGif()
                .load(R.drawable.icon)
                .into(imageView);
        line=findViewById(R.id.view_1);
//        mNetworkReceiver = new NetworkChangeReceiver();
//        registerNetworkBroadcastForNougat();
        mDrawerLayout = findViewById(R.id.drawer_layout);
        LinearLayout opendrawerLL = findViewById(R.id.opendrawerLL);
        audioOverLayLL = findViewById(R.id.audioOverLayLL);
       // toptextmainTV = findViewById(R.id.toptextmainTV);
        micBTN = findViewById(R.id.micBTN);
        edittextContainer = findViewById(R.id.rel_edittextContainer);
        micBTN.setEnabled(true);
            CommonUtils.ZoomOut(this,micBTN);

      //  toptextmainTV.setTypeface(CommonUtils.getInstance().getTypeface(MainActivity.this));
        leftMenu = findViewById(R.id.leftMenuNew);
        //txt_size=leftMenu.findViewById(R.id.txt_size);

        relSizeContainer = leftMenu.findViewById(R.id.relSizeContainer);
        relSpeedContainer = leftMenu.findViewById(R.id.rel_speedContainer);

        relSpeedContainer.setVisibility(View.GONE);
        relSizeContainer.setVisibility(View.GONE);


        relMic = leftMenu.findViewById(R.id.rel_mic);
        relKeyboard = leftMenu.findViewById(R.id.rel_keyboard);
        relSpeed = leftMenu.findViewById(R.id.rel_speed);
        relTextSize = leftMenu.findViewById(R.id.rel_fontsize);
        relTwitter = leftMenu.findViewById(R.id.rel_twitter);
        relInstagram = leftMenu.findViewById(R.id.rel_instagram);
        relFb = leftMenu.findViewById(R.id.rel_fb);


        txtSize = leftMenu.findViewById(R.id.textSize);
        txtSpeed = leftMenu.findViewById(R.id.textSpeed);

        txtSize.setTypeface(CommonUtils.getInstance().getCosmicSansFont(MainActivity.this));
        txtSpeed.setTypeface(CommonUtils.getInstance().getCosmicSansFont(MainActivity.this));


        TouchGestures();
        micCheck = getMic(MainActivity.this);
        timer = getTimer(MainActivity.this);

        StaticData.AdapterMicCheck=micCheck;

        if (micCheck == 1) {
            micBTN.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.mic_icon_small));
            relMic.setBackgroundColor(getResources().getColor(R.color.crossoff_register_midleText_color));
        } else if (micCheck == 2) {
            micBTN.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.keyboardlarge));
            relKeyboard.setBackgroundColor(getResources().getColor(R.color.crossoff_register_midleText_color));
            relMic.setBackgroundColor(getResources().getColor(R.color.crossoff_menu_settings_back_color));
        }

        Log.i("miccheckmain"," "+micCheck);
        audioOverLayLL.setVisibility(View.GONE);
        subjectNameTV = findViewById(R.id.subjectNameTV);
        subjectNameTV.setSingleLine(true);
        subjectNameTV.setImeOptions(EditorInfo.IME_ACTION_DONE);


       subjectNameTV.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {

               fooNext();
                    subjectNameTV.setText("");
                }
                return false;
            }
        });



        rippleBackground = findViewById(R.id.content);
        SocketService.setSocketListener(this, this);
        if (!CommonUtils.getActiveInternet(context)) {
            onDisConnected();
        } else {
            onConnected();
        }
 /*       findViewById(R.id.twitterLL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        findViewById(R.id.facebookLL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        findViewById(R.id.linkedinLL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
//
//        gif = findViewById(R.id.gif1);
//        gif2 = findViewById(R.id.gif2);

        Boolean isFirstTime;

    /*    SharedPreferences app_preferences = PreferenceManager
                .getDefaultSharedPreferences(MainActivity.this);

        SharedPreferences.Editor editor = app_preferences.edit();

        isFirstTime = app_preferences.getBoolean("isFirstTime", true);*/

//        if (isFirstTime) {
////
////            gif.setVisibility(View.VISIBLE);
////            gif2.setVisibility(View.VISIBLE);
//            editor.putBoolean("isFirstTime", false);
//            editor.apply();
//
//        } else {
//            gif.setVisibility(View.GONE);
//            gif2.setVisibility(View.GONE);
//        }

        micBTN.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                micBTN.setAnimation(null);
//                micBTN.getAnimation().setAnimationListener(null);
                //   micBTN.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.mic_icon));
                Drawable micicon = getResources().getDrawable(R.drawable.mic_icon_small);
                final Bitmap bmap = ((BitmapDrawable) micBTN.getDrawable()).getBitmap();
                final Bitmap myLogo = ((BitmapDrawable) micicon).getBitmap();
                Drawable keyboardicon = getResources().getDrawable(R.drawable.keyboardlarge);
                final Bitmap bitmapkeyboard = ((BitmapDrawable) keyboardicon).getBitmap();

                Drawable miciconon = getResources().getDrawable(R.drawable.mic_icon_on);
                final Bitmap bmapmicon = ((BitmapDrawable) micBTN.getDrawable()).getBitmap();
                final Bitmap bitmapicon = ((BitmapDrawable) miciconon).getBitmap();

                Drawable keyboardactive = getResources().getDrawable(R.drawable.keyboard);
                final Bitmap keyboardActiveIcon = ((BitmapDrawable) micBTN.getDrawable()).getBitmap();
                final Bitmap keyboardActiveBitmap = ((BitmapDrawable) keyboardactive).getBitmap();

                if (bmap.sameAs(myLogo)) {
                    micBTN.setAnimation(null);
                    mainactivityrecycler.setVisibility(View.GONE);
                    subjectNameTV.setText("");
                    micBTN.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.mic_icon_on));
                    rippleBackground.startRippleAnimation();
                    subjectNameTV.setFocusable(false);
                    subjectNameTV.setVisibility(View.VISIBLE);
                    edittextContainer.setVisibility(View.VISIBLE);

                    sharedPreferancesMic(3);
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

                    startSpeech();


                } else if (bmapmicon.sameAs(bitmapicon)) {
                    CommonUtils.ZoomOut(MainActivity.this,micBTN);
                    micBTN.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.mic_icon_small));
                    mainactivityrecycler.setVisibility(View.VISIBLE);
                   /* mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);*/
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    rippleBackground.stopRippleAnimation();
                    check = true;
                    speech.stopListening();
               /*     subjectNameTV.setHint("Enter List Name");
                    subjectNameTV.setFocusableInTouchMode(true);
                    subjectNameTV.setFocusable(true);*/
                    subjectNameTV.setVisibility(View.GONE);
                    subjectNameTV.setFocusable(false);
                    edittextContainer.setVisibility(View.GONE);

                    sharedPreferancesMic(1);
                    //next_tv.setVisibility(View.VISIBLE);
                } else if (bmap.sameAs(bitmapkeyboard)) {
                    micBTN.setAnimation(null);
                    mainactivityrecycler.setVisibility(View.GONE);
                    audioOverLayLL.setVisibility(View.VISIBLE);
                    subjectNameTV.requestFocus();
                    subjectNameTV.setFocusable(true);
                    subjectNameTV.setFocusableInTouchMode(true);
                    subjectNameTV.setVisibility(View.VISIBLE);
                    edittextContainer.setVisibility(View.VISIBLE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(MainActivity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                    rippleBackground.stopRippleAnimation();
                    micBTN.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.keyboard));

                    sharedPreferancesMic(4);
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                } else if (keyboardActiveIcon.sameAs(keyboardActiveBitmap)) {
                    micBTN.setAnimation(null);
                    mainactivityrecycler.setVisibility(View.VISIBLE);
                    audioOverLayLL.setVisibility(View.GONE);
                    subjectNameTV.setVisibility(View.GONE);
                    edittextContainer.setVisibility(View.GONE);
                    subjectNameTV.setFocusable(false);
                /*    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);*/
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    micBTN.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.keyboardlarge));

                    sharedPreferancesMic(2);
                }


            }
        });

        // for app icon control for nav drawer
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.empty_icon,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                getActionBar().setTitle(mTitle);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                getActionBar().setTitle(mDrawerTitle);

            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // enable ActionBar app icon to behave as action to toggle nav drawer
        //  getActionBar().setDisplayHomeAsUpEnabled(true);

        /*if (savedInstanceState == null) {
            // on first time display view for first nav item
            selectItem(0);
        }*/

        opendrawerLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });
        mainactivityrecycler = findViewById(R.id.recyclerView);

        mainactivityrecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        mainactivityrecycler.setHasFixedSize(true);

        foo_waiting();

        if (StaticData.id != null) {
            ApiNarrator.retrieveList(context);
            long yourmilliseconds = System.currentTimeMillis();
            final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM");
            final Date resultdate = new Date(yourmilliseconds);
            ApiNarrator.getUserInfo(new BirthdayListener() {
                @Override
                public void onSuccess(String url) {
                    String birthday = "";
                    try{
                        if (!url.equals("null")) {
                            System.out.println();
                            Log.i("systemdate", "" + sdf.format(resultdate) + "url " + url);
                            birthday = url.substring(0, 5);
                            Log.i("systemdate2",""+birthday+"   "+url);
                        } else {
                            Log.i("birthday", "Birthday is empty");
                        }
                    }
                    catch (Exception e)
                    {
                        Log.i("birthdayerror", e.toString());
                    }

                    if (birthday.equals(sdf.format(resultdate))) {
                        foo_dialog();
                    } else {
                        Log.i("birthdayerror2", "cant create dialog for birthday");
                    }
                }

                @Override
                public void onFailure(String error) {

                }
            });

            mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
                @Override
                public void onDrawerSlide(View drawerView, float slideOffset) {

                }

                @Override
                public void onDrawerOpened(View drawerView) {
                    InputMethodManager imm = (InputMethodManager)MainActivity.this.getSystemService(Service.INPUT_METHOD_SERVICE);

                    imm.hideSoftInputFromWindow(subjectNameTV.getWindowToken(), 0);
                   micBTN.setClickable(false);
                    ImageView imageView=findViewById(R.id.openedgif);
                    Glide.with(MainActivity.this)
                            .asGif()
                            .load(R.drawable.icon)
                            .into(imageView);

               /*     mainactivityrecycler.setClickable(false);
                    mainactivityrecycler.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            return true;
                        }
                    });
                    mainactivityrecycler.setClickable(false);*/
                    edittextContainer.setVisibility(View.GONE);
                 mainactivityrecycler.setVisibility(View.GONE);
                    micBTN.setVisibility(View.GONE);
                    line.setVisibility(View.GONE);
                }

                @Override
                public void onDrawerClosed(View drawerView) {

               /*     mainactivityrecycler.setClickable(true);
                    micBTN.setClickable(true);
                    mainactivityrecycler.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            return false;
                        }
                    });
                    mainactivityrecycler.setClickable(true);*/
                    micBTN.setClickable(true);
                micBTN.setVisibility(View.VISIBLE);
                    mainactivityrecycler.setVisibility(View.VISIBLE);
                    line.setVisibility(View.VISIBLE);
                }

                @Override
                public void onDrawerStateChanged(int newState) {

                }
            });
        }

        bottomPanel.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onRightToLeftSwipe() {
            /*    audioOverLayLL.setVisibility(View.VISIBLE);
                subjectNameTV.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(MainActivity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                rippleBackground.stopRippleAnimation();*/
            }
        });

//        txt_size.setOnClickListener(this);

       // CommonUtils.changeTextSize(mDrawerLayout, getFontSize(MainActivity.this));
        txtSize.setText(getFontText(MainActivity.this));
        Log.i("logac", "" + getTimer(MainActivity.this));
        txtSpeed.setText("" + getTimer(MainActivity.this));

        if (adp != null) {
            adp.changeSize(getFontSize(MainActivity.this), getTimer(MainActivity.this));
        }

        String fcmToken= FirebaseInstanceId.getInstance().getToken();
        Log.i("fcmtoken","Device Token: "+fcmToken);


        StaticData.Token=fcmToken;
        ApiNarrator.registerToken(fcmToken);

    }

    private boolean fooListCheck(String list)
    {

        if(!ApiNarrator.lists.isEmpty())
        {
            for(int i=0;i<ApiNarrator.lists.size();i++)
            {
                if(ApiNarrator.lists.get(i).getListName().contains(list))
                {
                    fooListCheck=true;
                    break;
                }
                else{
                    fooListCheck=false;
                }
            }
        }
        else{
            Log.i("List","List is Empty");
        }


        return fooListCheck;
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        ApiNarrator.retrieveList(context);
        rippleBackground.stopRippleAnimation();
        mainactivityrecycler.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // to change up caret
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (speech != null) {
            speech.stopListening();
            speech = null;
        }

    }

    @Override
    public void onReadyForSpeech(Bundle params) {
        Log.e("onReadyForSpeech: ", "In here");
        subjectNameTV.setHint("Listening...");
        micBTN.setImageResource(R.drawable.mic_icon_on);
        micBTN.setVisibility(View.VISIBLE);

    }

    @Override
    public void onBeginningOfSpeech() {
        Log.e("onBeginningOfSpeech: ", "In here");
        micBTN.setImageResource(R.drawable.mic_icon_on);
    }

    @Override
    public void onRmsChanged(float rmsdB) {
        if (onResult) {
            Log.e("onRmsChanged: ", "" + rmsdB);

        }
    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.e("onBufferReceived: ", "In here");
    }

    @Override
    public void onEndOfSpeech() {
        Log.e("onEndOfSpeech: ", "In here");
    }

    @Override
    public void onError(int error) {
        Log.e("onError: ", "In here");
        String errorMessage = getErrorText(error);
        subjectNameTV.setHint(errorMessage);
    }

    @Override
    public void onResults(final Bundle results) {
        Log.e("onResults: ", "In here");

        if (timer == 2) {
            timerCheck = 1000;
        } else if (timer == 3) {
            timerCheck = 2000;
        } else if (timer == 4) {
            timerCheck = 3000;
        }
        final Handler handler2 = new Handler();

        handler2.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                subjectNameTV.setHint(matches.get(0));
                micBTN.setImageResource(R.drawable.mic_icon_on);
                onResult = false;

                sharedPreferancesMic(1);

                if(fooListCheck(matches.get(0)))
                {
                    Toast.makeText(MainActivity.this,"List Already Exist",Toast.LENGTH_LONG).show();

                }
                else if(!fooListCheck(matches.get(0)))
                {
                    audioOverLayLL.setVisibility(View.GONE);
                    Intent call_SubjectDataActivity = new Intent(MainActivity.this, SubjectDataActivity.class);
                    call_SubjectDataActivity.putExtra("SUBJECT", (matches.get(0) + ""));
                    call_SubjectDataActivity.putExtra("statusCode", 2);
                    call_SubjectDataActivity.putExtra("sendCheck", 1);
                    call_SubjectDataActivity.putExtra("micCheck",1);
                    call_SubjectDataActivity.putExtra("txtsize", getFontSize(MainActivity.this));
                    startActivity(call_SubjectDataActivity);
                }


            }
        }, timerCheck);


    }

    @Override
    public void onPartialResults(Bundle partialResults) {
        Log.e("onPartialResults: ", "In here");
    }
/*

    @TargetApi(23)
    public boolean checkForPermission() {

        String hasVoiceRecordingPermission = android.Manifest.permission.RECORD_AUDIO;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(hasVoiceRecordingPermission) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{hasVoiceRecordingPermission}, 010);
            } else {
                return true;
            }
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 010:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startSpeech();
                } else {
                    Toast.makeText(MainActivity.this, "Permission Not Granted.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
*/

    @Override
    public void onEvent(int eventType, Bundle params) {
        Log.e("onEvent: ", "In here");
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        mainactivityrecycler.setVisibility(View.VISIBLE);
        try {
            if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
                super.onBackPressed();
            } else {
                //    mainactivityrecycler.setVisibility(View.VISIBLE);
                speech.stopListening();
                if (micCheck == 1) {
                    micBTN.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.mic_icon_small));
                } else {
                    micBTN.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.keyboardlarge));
                }
                audioOverLayLL.setVisibility(View.GONE);
                Toast.makeText(getBaseContext(), "Press once again to exit!",
                        Toast.LENGTH_SHORT).show();
            }
            back_pressed = System.currentTimeMillis();
        } catch (Exception e) {
            Log.e("speech.startListening", "" + e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        CommonUtils.ZoomOut(MainActivity.this,micBTN);

        audioOverLayLL.setVisibility(View.GONE);

        if (getMic(this) == 1) {
            micBTN.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.mic_icon_small));
            relMic.setBackgroundColor(getResources().getColor(R.color.crossoff_register_midleText_color));
        } else if (getMic(this)== 2) {
            micBTN.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.keyboardlarge));
            relKeyboard.setBackgroundColor(getResources().getColor(R.color.crossoff_register_midleText_color));
            relMic.setBackgroundColor(getResources().getColor(R.color.crossoff_menu_settings_back_color));
        }
        else if(getMic(this)==3)
        {
            mainactivityrecycler.setVisibility(View.GONE);
            audioOverLayLL.setVisibility(View.VISIBLE);
            subjectNameTV.setText("");
            micBTN.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.mic_icon_on));
            rippleBackground.startRippleAnimation();
            subjectNameTV.setFocusable(false);
            subjectNameTV.setVisibility(View.VISIBLE);
            edittextContainer.setVisibility(View.VISIBLE);
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

            if(subjectNameTV.getVisibility()==View.VISIBLE)
            {
                Log.i("viewsub","View is visible");
            }
            else{
                Log.i("viewsub","View is not visible");
            }
            startSpeech();

        }
        else if (getMic(this)==4)
        {
            micBTN.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.keyboard));
            relKeyboard.setBackgroundColor(getResources().getColor(R.color.crossoff_register_midleText_color));
            relMic.setBackgroundColor(getResources().getColor(R.color.crossoff_menu_settings_back_color));
            mainactivityrecycler.setVisibility(View.GONE);
            audioOverLayLL.setVisibility(View.VISIBLE);
            subjectNameTV.requestFocus();
            subjectNameTV.setFocusable(true);
            subjectNameTV.setFocusableInTouchMode(true);
            subjectNameTV.setVisibility(View.VISIBLE);
            edittextContainer.setVisibility(View.VISIBLE);
            InputMethodManager imm = (InputMethodManager) getSystemService(MainActivity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }

        ///// Resume Checks /////


        //////////////



      Log.i("resumecheck",""+StaticData.MicCheck);
        if (speech == null) {
            speech = SpeechRecognizer.createSpeechRecognizer(this);
            speech.setRecognitionListener(this);
          //  audioOverLayLL.setVisibility(View.GONE);

       /*     if ( StaticData.MicCheck == 1) {
                micBTN.setImageResource(R.drawable.mic_icon);
            } else {
                micBTN.setImageResource(R.drawable.keyboardlarge);
            }*/
            micBTN.setVisibility(View.VISIBLE);
            //  subjectNameTV.setText(R.string.subjectName);
            recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, this.getPackageName());
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 0);
        }
        check = false;
    }

    private void startSpeech() {
        try {
            speech.startListening(recognizerIntent);
            onResult = true;
            audioOverLayLL.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            Log.e("speech.startListening", "" + e.getMessage());
        }
    }


//    public static void maindialog(boolean value) {
//
//        if (value) {
//
//            internetcon.setVisibility(View.GONE);
//            rel_main_nav.setVisibility(View.VISIBLE);
//        } else {
//            internetcon.setVisibility(View.VISIBLE);
//            rel_main_nav.setVisibility(View.GONE);
//        }
//
//    }

    public void swipeListener() {
        swipe = new Swipe();
        swipe.setListener(new SwipeListener() {

            @Override
            public void onSwipingLeft(MotionEvent event) {
            }

            @Override
            public void onSwipedLeft(MotionEvent event) {

//                gif.setVisibility(View.GONE);
//                gif2.setVisibility(View.GONE);
             /*   mainactivityrecycler.setVisibility(View.GONE);
                audioOverLayLL.setVisibility(View.VISIBLE);
                next_tv.setVisibility(View.VISIBLE);
                subjectNameTV.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(MainActivity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                   adp.linear.setVisibility(View.GONE);
                rippleBackground.stopRippleAnimation();*/
                adpLayoutCheck();
                sizeAndspeed();
                mDrawerLayout.closeDrawers();
                /*if(mDrawerLayout.isDrawerOpen(GravityCompat.START)==true)
                {
                    mDrawerLayout.closeDrawers();
                 mainactivityrecycler.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            return false;
                        }
                    });
                }

                else {
                    mainactivityrecycler.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            return true;
                        }
                    });
                }*/

            }

            @Override
            public void onSwipingRight(MotionEvent event) {

                adpLayoutCheck();
/*
                mainactivityrecycler.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return true;
                    }
                });*/




        }

            @Override
            public void onSwipedRight(MotionEvent event) {
            }

            @Override
            public void onSwipingUp(MotionEvent event) {
                adpLayoutCheck();
            }

            @Override
            public void onSwipedUp(MotionEvent event) {
            }

            @Override
            public void onSwipingDown(MotionEvent event) {

                adpLayoutCheck();
            }

            @Override
            public void onSwipedDown(MotionEvent event) {
            }
        });
    }

    public void adpLayoutCheck() {
        if (adp != null) {
            adp.resetList();
//            adp.linear.setVisibility(View.GONE);
        }
       /* if(adp.linear.getVisibility()==View.VISIBLE)
        {
            adp.relative.setVisibility(View.VISIBLE);

        }*/
    }


//    private void registerNetworkBroadcastForNougat() {
//        registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//    }

//    protected void unregisterNetworkChanges() {
//        try {
//            unregisterReceiver(mNetworkReceiver);
//        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
//        }
//    }

    private void fooNext()
    {
        listName=subjectNameTV.getText().toString().replace(" ","");


        if(fooListCheck(listName))
        {
            Toast.makeText(MainActivity.this,"List Already Exist",Toast.LENGTH_LONG).show();

        }
        else if (!fooListCheck(listName)){

            String data = subjectNameTV.getText().toString().trim();
            Intent call_SubjectDataActivity;
            if (!data.isEmpty()) {

                sharedPreferancesMic(2);
                call_SubjectDataActivity = new Intent(MainActivity.this, SubjectDataActivity.class);
                call_SubjectDataActivity.putExtra("SUBJECT", (data + ""));
                call_SubjectDataActivity.putExtra("sendCheck", 1);
                call_SubjectDataActivity.putExtra("statusCode", 2);
                call_SubjectDataActivity.putExtra("micCheck",2);
                call_SubjectDataActivity.putExtra("txtsize", getFontSize(MainActivity.this));
                startActivity(call_SubjectDataActivity);
            } else {
                Toast.makeText(getApplicationContext(), "Please Enter Item Name", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        swipe.dispatchTouchEvent(event);
        return super.dispatchTouchEvent(event);
    }

    void onConnected() {
        internetcon.setVisibility(View.GONE);
        rel_main_nav.setVisibility(View.VISIBLE);
    }

    void onDisConnected() {
        internetcon.setVisibility(View.VISIBLE);
        rel_main_nav.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeConnectionListner();
        SocketService.removeSocketListner();
        stopService(new Intent(getBaseContext(), SocketService.class));
    }

    public void foo_waiting() {
        waiting = new Dialog(this);
        waiting.requestWindowFeature(Window.FEATURE_NO_TITLE);
        waiting.setContentView(R.layout.waiting_screen);
        waiting.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
 ImageView waitingtxt = waiting.findViewById(R.id.waitingtxt);
        CommonUtils.bottomup(getApplicationContext(), waitingtxt);
        waiting.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        waiting.setCancelable(false);
        waiting.show();
        Timer timer = new Timer();
        timer.schedule(new DialogBoxTimer(waiting), 7000);
    }

    public void initializeAdapter() {
        Collections.reverse(ApiNarrator.lists);
        adp = new MainActivityAdapter(ApiNarrator.lists, context);
        adp.changeSize(getFontSize(MainActivity.this), getTimer(MainActivity.this));
//        mainactivityrecycler.scrollToPosition(ApiNarrator.lists.size() - 1);
        mainactivityrecycler.setAdapter(adp);
//        mainactivityrecycler.scrollToPosition(ApiNarrator.lists.size() - 1);

    }

    public void foo_dialog() {

        dialog_birthday = new Dialog(this);
        dialog_birthday.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_birthday.setContentView(R.layout.popup_birthday);
        dialog_birthday.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        cancel_dialog = dialog_birthday.findViewById(R.id.tv_cancel_dialog);
        img_birthday = dialog_birthday.findViewById(R.id.img_birthday);
        dialog_birthday.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog_birthday.setCancelable(false);
        dialog_birthday.show();

        cancel_dialog.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dialog_birthday.dismiss();
                return false;
            }
        });

        ApiNarrator.birthday(new BirthdayListener() {
            @Override
            public void onSuccess(String url) {


                com.squareup.picasso.Picasso.with(context).
                        load(String.valueOf(url)).
                        placeholder(R.mipmap.ic_launcher).
                        into(img_birthday);
            }

            @Override
            public void onFailure(String error) {

            }
        });


    }

    // wifi listner
    @Override
    public void onWifiConnected() {
        onConnected();
    }

    @Override
    public void onWifiDisconnected() {
        onDisConnected();
    }

    @Override
    public void onMobileDataConnected() {
        onConnected();
    }

    @Override
    public void onMobileDataDisconnected() {
        onDisConnected();
    }

    // Socket Listner
    @Override
    public void onConnect(String data) {
    }

    @Override
    public void onDisconnect(String data) {
    }

    @Override
    public void onMessage(String data) {
        if(sendContacts){
            sendContacts = false;
            ServerSocket.sendContactsToServer();
        }
    }

    @Override
    public void onCrossOff(StrikeItem strikeItem) {
    }

    @Override
    public void onPhoneCheck(ArrayList<SyncContact> syncContacts) {
    }

    private void sharedPreferances(int size, String txtcheck) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putInt(PREFS_KEY, size);
        editor.putString(PREFS_KEY_2, txtcheck);

        editor.commit();
    }

    private void sharedPreferancesMic(int micCheck) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putInt(PREFS_KEY_3, micCheck);

        editor.apply();
    }

    private void sharedPreferancesTimer(int micCheck) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putInt(PREFS_KEY_4, micCheck);

        editor.commit();
    }



    public void TouchGestures() {
        relSizeContainer.setOnTouchListener(new OnCustomTouchListener(this) {

                                                public void onSwipeUp() {

                                                    if (txtSize.getText().toString().equals("Small")) {
                                                        txtSize.setText("Medium");
                                                    } else if (txtSize.getText().toString().equals("Medium")) {
                                                        txtSize.setText("Large");
                                                    }

                                                    if (relSizeContainer.getVisibility() == View.VISIBLE) {
                                                        int size = 14;
                                                        String txtCheck = txtSize.getText().toString();
                                                        if (txtCheck.equals("Small")) {
                                                            size = 16;
                                                        } else if (txtCheck.equals("Medium")) {
                                                            size = 20;
                                                        } else if (txtCheck.equals("Large")) {
                                                            size = 25;
                                                        }

                                                        Log.i("txtcheck", txtCheck);
                                                        //  CommonUtils.changeTextSize(mDrawerLayout, size);
                                                        sharedPreferances(size, txtSize.getText().toString());
                                                        if (adp != null) {
                                                            adp.changeSize(size, getTimer(MainActivity.this));
                                                        }
//                                                        CommonUtils.rightToLeft(MainActivity.this, relSizeContainer, mDrawerLayout);
//                                                        relSizeContainer.setVisibility(View.GONE);
                                                        //   mDrawerLayout.closeDrawers();
                                                    } else {
                                                        relSizeContainer.setVisibility(View.VISIBLE);
                                                        CommonUtils.leftToRight(MainActivity.this, relSizeContainer, mDrawerLayout, relSpeedContainer);

                                                        if (relSpeedContainer.getVisibility() == View.VISIBLE) {
                                                            relSpeedContainer.setVisibility(View.GONE);
                                                            CommonUtils.leftToRight2(MainActivity.this, relSpeedContainer);
                                                        }

                                                    }
                                                }

                                                public void onSwipeDown() {

                                                    if (txtSize.getText().toString().equals("Medium")) {
                                                        txtSize.setText("Small");
                                                    } else if (txtSize.getText().toString().equals("Large")) {
                                                        txtSize.setText("Medium");
                                                    }
                                                    if (relSizeContainer.getVisibility() == View.VISIBLE) {
                                                        int size = 14;
                                                        String txtCheck = txtSize.getText().toString();
                                                        if (txtCheck.equals("Small")) {
                                                            size = 16;
                                                        } else if (txtCheck.equals("Medium")) {
                                                            size = 20;
                                                        } else if (txtCheck.equals("Large")) {
                                                            size = 25;
                                                        }

                                                        Log.i("txtcheck", txtCheck);
                                                        //  CommonUtils.changeTextSize(mDrawerLayout, size);
                                                        sharedPreferances(size, txtSize.getText().toString());
                                                        if (adp != null) {
                                                            adp.changeSize(size, getTimer(MainActivity.this));
                                                        }
//                                                        CommonUtils.rightToLeft(MainActivity.this, relSizeContainer, mDrawerLayout);
//                                                        relSizeContainer.setVisibility(View.GONE);
                                                        //   mDrawerLayout.closeDrawers();
                                                    } else {
                                                        relSizeContainer.setVisibility(View.VISIBLE);
                                                        CommonUtils.leftToRight(MainActivity.this, relSizeContainer, mDrawerLayout, relSpeedContainer);

                                                        if (relSpeedContainer.getVisibility() == View.VISIBLE) {
                                                            relSpeedContainer.setVisibility(View.GONE);
                                                            CommonUtils.leftToRight2(MainActivity.this, relSpeedContainer);
                                                        }

                                                    }

                                                }


                                            }

        );

        relSpeedContainer.setOnTouchListener(new OnCustomTouchListener(this) {

                                                 public void onSwipeUp() {

                                                     if (txtSpeed.getText().toString().equals("2")) {
                                                         txtSpeed.setText("3");
                                                     }else if (txtSpeed.getText().toString().equals("3")) {
                                                         txtSpeed.setText("4");
                                                     }else if (txtSpeed.getText().toString().equals("4")) {
                                                         txtSpeed.setText("5");
                                                     }else if (txtSpeed.getText().toString().equals("5")) {
                                                         txtSpeed.setText("6");
                                                     }


                                                 }

                                                 public void onSwipeDown() {


                                                     if (txtSpeed.getText().toString().equals("3")) {
                                                         txtSpeed.setText("2");
                                                     }
                                                     else if (txtSpeed.getText().toString().equals("4")) {
                                                         txtSpeed.setText("3");
                                                     } else if (txtSpeed.getText().toString().equals("5")) {
                                                         txtSpeed.setText("4");
                                                     } else if (txtSpeed.getText().toString().equals("6")) {
                                                         txtSpeed.setText("5");
                                                     }


                                                 }


                                             }
        );

        relSpeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (relSpeedContainer.getVisibility() == View.VISIBLE) {

                    String timerStr = txtSpeed.getText().toString();
                    if (timerStr.equals("2")) {
                        timer = 2;
                    } else if (timerStr.equals("3")) {
                        timer = 3;
                    } else if (timerStr.equals("4")) {
                        timer = 4;
                    } else if (timerStr.equals("5")) {
                        timer = 5;
                    } else if (timerStr.equals("6")) {
                        timer = 6;
                    }
                    sharedPreferancesTimer(timer);
                    relSpeedContainer.setVisibility(View.GONE);
                    CommonUtils.rightToLeft(MainActivity.this, relSpeedContainer, mDrawerLayout);
                    //  mDrawerLayout.closeDrawers();
                } else {
                    relSpeedContainer.setVisibility(View.VISIBLE);
                    CommonUtils.leftToRight(MainActivity.this, relSpeedContainer, mDrawerLayout, relSizeContainer);

                }

               if (relSizeContainer.getVisibility() == View.VISIBLE) {
                    relSizeContainer.setVisibility(View.GONE);
                    CommonUtils.leftToRight2(MainActivity.this, relSizeContainer);
                }

            }
        });

        relTextSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (relSizeContainer.getVisibility() == View.VISIBLE) {
                    int size = 14;
                    String txtCheck = txtSize.getText().toString();
                    if (txtCheck.equals("Small")) {
                        size = 16;
                    } else if (txtCheck.equals("Medium")) {
                        size = 20;
                    } else if (txtCheck.equals("Large")) {
                        size = 25;
                    }

                    Log.i("txtcheck", txtCheck);
                  //  CommonUtils.changeTextSize(mDrawerLayout, size);
                    sharedPreferances(size, txtSize.getText().toString());
                    if (adp != null) {
                        adp.changeSize(size, getTimer(MainActivity.this));
                    }
                    CommonUtils.rightToLeft(MainActivity.this, relSizeContainer, mDrawerLayout);
                    relSizeContainer.setVisibility(View.GONE);
                    //   mDrawerLayout.closeDrawers();
                } else {
                    relSizeContainer.setVisibility(View.VISIBLE);
                    CommonUtils.leftToRight(MainActivity.this, relSizeContainer, mDrawerLayout, relSpeedContainer);

                   if (relSpeedContainer.getVisibility() == View.VISIBLE) {
                        relSpeedContainer.setVisibility(View.GONE);
                        CommonUtils.leftToRight2(MainActivity.this, relSpeedContainer);
                    }

                }


            }
        });

        relMic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonUtils.ZoomOut(MainActivity.this,micBTN);
                sizeAndspeed();
                relSpeedContainer.setClickable(true);
                relKeyboard.setBackgroundColor(getResources().getColor(R.color.crossoff_menu_settings_back_color));
                relMic.setBackgroundColor(getResources().getColor(R.color.crossoff_register_midleText_color));
                micBTN.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.mic_icon_small));
                sharedPreferancesMic(1);
                micCheck=1;
                StaticData.AdapterMicCheck=micCheck;
                subjectNameTV.setVisibility(View.GONE);
            }
        });

        relKeyboard.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View view) {
                                               micBTN.setAnimation(null);
sizeAndspeed();
                                               relSpeedContainer.setClickable(false);
                                               relMic.setBackgroundColor(getResources().getColor(R.color.crossoff_menu_settings_back_color));
                                               relKeyboard.setBackgroundColor(getResources().getColor(R.color.crossoff_register_midleText_color));
                                               micBTN.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.keyboardlarge));
                                               rippleBackground.stopRippleAnimation();

                                               audioOverLayLL.setVisibility(View.GONE);
                                               speech.stopListening();
                                               sharedPreferancesMic(2);
                                               micCheck=2;
                                               StaticData.AdapterMicCheck=micCheck;
                                               subjectNameTV.setVisibility(View.GONE);
                                           }
                                       }
        );

        relFb.setOnClickListener(new View.OnClickListener() {
                                     @Override
                                     public void onClick(View view) {
                                         socialAccounts("http://www.facebook.com/crossoff.us");

                                     }
                                 }
        );

        relTwitter.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
                                              socialAccounts("https://www.twitter.com/crossoffus");
                                          }
                                      }
        );

        relInstagram.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                socialAccounts("https://www.instagram.com/crossoff.us");
                                            }
                                        }


        );

    }


    public void sizeAndspeed()
    {
        if (relSpeedContainer.getVisibility() == View.VISIBLE) {
            relSpeedContainer.setVisibility(View.GONE);
            CommonUtils.leftToRight2(MainActivity.this, relSpeedContainer);
        }
        else if (relSizeContainer.getVisibility() == View.VISIBLE) {
                relSizeContainer.setVisibility(View.GONE);
                CommonUtils.leftToRight2(MainActivity.this, relSizeContainer);
            }


    }
    public void socialAccounts(String url) {
        try {
            Intent i = new Intent("android.intent.action.MAIN");
            i.setComponent(ComponentName.unflattenFromString("com.android.chrome/com.android.chrome.Main"));
            i.addCategory("android.intent.category.LAUNCHER");
            i.setData(Uri.parse(url));
            startActivity(i);
        } catch (ActivityNotFoundException e) {
            // Chrome is not installed
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(i);
        }
    }


}
