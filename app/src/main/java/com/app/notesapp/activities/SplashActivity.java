package com.app.notesapp.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.noteapp.R;
import com.app.notesapp.models.Auth;
import com.app.notesapp.utils.CommonUtils;
import com.crashlytics.android.Crashlytics;
import com.github.pwittchen.swipe.library.Swipe;
import com.github.pwittchen.swipe.library.SwipeListener;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends Activity {
    private Swipe swipe;
    private MediaPlayer splashSound;
    private static final int PERMISSION_REQUEST_CODE = 101;
    String[] permissions = {Manifest.permission.READ_SMS,Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.GET_ACCOUNTS, Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_CONTACTS};

    public static final String PREFS_NAME = "Splash";
    public static final String PREFS_KEY = "splashCheck";
    public boolean isFirstTime;

    protected void onCreate(Bundle splashBundle) {
        super.onCreate(splashBundle);
        Fabric.with(this, new Crashlytics());
     /*   Boolean isFirstTime;

        SharedPreferences app_preferences = PreferenceManager
                .getDefaultSharedPreferences(SplashActivity.this);

        SharedPreferences.Editor editor = app_preferences.edit();

        isFirstTime = app_preferences.getBoolean("isFirstTimeSplash", true);

        if (isFirstTime) {

            setContentView(R.layout.splash_video);
            Handler handler = new Handler();
            RelativeLayout internetcon = findViewById(R.id.internet);
            splashSound = MediaPlayer.create(SplashActivity.this,
                    R.raw.santamusic);
            splashSound.start();  //<<<play sound on Splash Screen
            handler.postDelayed(runnable, 16500);

            swipeListener();
            editor.putBoolean("isFirstTimeSplash", false);
            editor.apply();

        } else {
            permissionsCheck();
        }*/

        isFirstTime=getboolean(SplashActivity.this);
     sharedPreferances(false);



        if (isFirstTime) {

            setContentView(R.layout.splash_video);
            Handler handler = new Handler();
            RelativeLayout internetcon = findViewById(R.id.internet);
            splashSound = MediaPlayer.create(SplashActivity.this,
                    R.raw.santamusic);
            splashSound.start();  //<<<play sound on Splash Screen
            handler.postDelayed(runnable, 16500);
            swipeListener();

            isFirstTime=getboolean(SplashActivity.this);

        } else {
            permissionsCheck();
        }
    }

    private void sharedPreferances(boolean splash)
    {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putBoolean(PREFS_KEY,splash);
        editor.commit();
    }
    public static boolean getboolean(Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(PREFS_KEY,true);

    }
    void moveToNextActivity() {
        if (Auth.hasLogin(SplashActivity.this)) {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            finish();
        } else {
            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            finish();
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            permissionsCheck();
        }
    };

    public void swipeListener() {
        swipe = new Swipe();
        swipe.setListener(new SwipeListener() {

            @Override
            public void onSwipingLeft(MotionEvent event) {
             /*   if (Auth.hasLogin(SplashActivity.this)) {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
                splashSound.stop();
*/
            }

            @Override
            public void onSwipedLeft(MotionEvent event) {

            }

            @Override
            public void onSwipingRight(MotionEvent event) {
                Log.i("Test", "Working");
            }

            @Override
            public void onSwipedRight(MotionEvent event) {
                Log.i("Test", "Working");
            }


            @Override
            public void onSwipingUp(MotionEvent event) {
                Log.i("Test", "Working");
            }

            @Override
            public void onSwipedUp(MotionEvent event) {
                Log.i("Test", "Working");
            }


            @Override
            public void onSwipingDown(MotionEvent event) {
                Log.i("Test", "Working");
            }

            @Override
            public void onSwipedDown(MotionEvent event) {
                Log.i("Test", "Working");
            }


        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        swipe.dispatchTouchEvent(event);
        return super.dispatchTouchEvent(event);
    }

    private void permissionsCheck() {

        if (!CommonUtils.hasPermissions(this, permissions)) {
            ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE);
        } else {
            moveToNextActivity();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE && permissions.length > 0 && grantResults.length > 0) {
            if (!CommonUtils.hasPermissions(this, permissions)) {
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show();
                finishActivity();
            } else {
                moveToNextActivity();
            }
        } else {
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show();
            finishActivity();
        }
    }

    private void finishActivity() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 2000);
    }

    void getKeyHash() {
        try {
            @SuppressLint("PackageManagerGetSignatures") PackageInfo info = getPackageManager().getPackageInfo("com.app.notesapp", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String sign = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("MY KEY HASH:", sign);
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("nope", "nope");
        } catch (NoSuchAlgorithmException e) {
        }
    }
}