package com.app.notesapp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.app.noteapp.R;
import com.app.notesapp.api.ApiNarrator;

/**
 * Created by Danish on 8/2/2017.
 */

public class ListItemAcitivity extends Activity implements View.OnClickListener {

    RecyclerView mrecyclerview;
    Context context = this;
    ImageView bk_btn;
    int id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_items);
        mrecyclerview = findViewById(R.id.recyclerView);
        bk_btn = findViewById(R.id.backBTN);
        bk_btn.setOnClickListener(this);
        Intent iin = getIntent();
        Bundle b = iin.getExtras();
        id = b.getInt("ID");

        mrecyclerview.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        mrecyclerview.setHasFixedSize(true);

        ApiNarrator.retrieveItems(context, id);

    }

/*    public void initAdapter() {
        ListItemsAdapter adp = new ListItemsAdapter(ApiNarrator.items_name_list, context);
        mrecyclerview.setAdapter(adp);
    }*/

    @Override
    public void onClick(View v) {

        if (v == bk_btn) {
            super.onBackPressed();
        }
    }
}
