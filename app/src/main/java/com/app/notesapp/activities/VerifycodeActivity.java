package com.app.notesapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.app.noteapp.R;
import com.app.notesapp.api.ApiNarrator;
import com.app.notesapp.interfaces.ForgoPasswordListener;

/**
 * Created by Danish on 8/11/2017.
 */

public class VerifycodeActivity extends Activity {

    EditText et_verify;
    Button verification_Btn;
    String email;
    int uid;
    int code=0;
    ImageView back_img;
    int check_code;

  String verify_str;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_code);
        Intent iin = getIntent();
        Bundle b = iin.getExtras();
        email=b.getString("Email");

        et_verify=findViewById(R.id.verifyNumber);
        verification_Btn=findViewById(R.id.verifyBtn);
        back_img=findViewById(R.id.backbtn);



        verification_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                verify_str=et_verify.getText().toString();
                if (!verify_str.isEmpty()&&verify_str.length()==5)
                {
                    try
                    {
                        code=Integer.parseInt(verify_str);

                    }
                    catch (NumberFormatException ex)
                    {
                        Log.i("Number Exception",ex.toString());
                    }
                    Log.i("Code uSer","abc"+code);
                    ApiNarrator.forgotpass_2(email, code, new ForgoPasswordListener() {
                                @Override
                                public void OnSuccess(int userid,int code) {

                                    check_code=code;
                                    uid=userid;

                                    if(check_code==200)
                                    {
                                        Intent in = new Intent(VerifycodeActivity.this,ChangePasswordActivity.class);
                                        in.putExtra("uid",uid);
                                        startActivity(in);
                                    }
                                }

                                @Override
                                public void OnFail(String error) {

                                }
                            }
                    );
                }
                else
                {
                    et_verify.setText("");
                    et_verify.setHint("Enter Code");
                }

            }
        });

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
