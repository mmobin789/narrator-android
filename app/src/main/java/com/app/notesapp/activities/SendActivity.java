package com.app.notesapp.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.noteapp.R;
import com.app.notesapp.api.ApiNarrator;
import com.app.notesapp.interfaces.ForgoPasswordListener;
import com.app.notesapp.models.Contact;
import com.app.notesapp.utils.StaticData;

import java.util.ArrayList;

/**
 * Created by Danish on 7/31/2017.
 */

public class SendActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {

    ListView listView;
    ArrayAdapter<String> arrayAdapter;
    ArrayList<Contact> contacts;
    public static final int RequestPermissionCode = 1;
    PopupMenu popup;
    ImageView bk_img, img_send;
    ArrayList<String> wordsList;
    TextView btn_email, btn_msg, topTextView;
    String list_name, items_name;
    int list_id;
    EditText et_search;
    Dialog emailDialog;
    TextView tv_send, tv_cancel;
    EditText et_email;
    String email;
    private Context context = this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initviews();
    }

    public void initviews() {
        setContentView(R.layout.activity_send);


        Intent iin = getIntent();
        Bundle b = iin.getExtras();
        list_name = b.getString("List_Name");
        items_name = b.getString("Items_Name");
        list_id = b.getInt("list_id");

        Log.i("List_ItemsSend", items_name);
        listView = findViewById(R.id.listview1);
        bk_img = findViewById(R.id.backBTN);
        btn_email = findViewById(R.id.btn_email);
        btn_msg = findViewById(R.id.btn_msg);
        et_search = findViewById(R.id.et_search);
        topTextView = findViewById(R.id.toptextmainTV);
        et_search.setFocusable(false);

        bk_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_search.setFocusableInTouchMode(true);
                et_search.setFocusable(true);
            }
        });
        GetContactsIntoArrayList();
        ArrayList<String> names = new ArrayList<>();
        for (Contact contact : contacts) {
            names.add(contact.getName());
        }
        arrayAdapter = new ArrayAdapter<>(
                SendActivity.this,
                R.layout.activity_send_row,
                R.id.textView, names);

        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getApplicationContext(), view.getId()+" "+id+" " + contacts.get(position).getName() + "\n" + contacts.get(position).getNumber(), Toast.LENGTH_SHORT).show();

                String phone = contacts.get(position).getNumber().trim();

                if (!phone.startsWith("+92") && !phone.contains("+44")) {
                    phone = phone.replaceFirst("0", "");
                    phone = "+92" + phone;
                }
                if (!phone.startsWith("+44") && !phone.contains("+92")) {
                    phone = phone.replaceFirst("0", "");
                    phone = "+44" + phone;
                }

                Log.i("phone", phone);
                ApiNarrator.shareList(Integer.parseInt(StaticData.id), phone, list_id, new ForgoPasswordListener() {
                    @Override
                    public void OnSuccess(int data, int code) {

                        if (code == 1) {
                            Toast.makeText(SendActivity.this, "List Sent", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SendActivity.this, "List not Sent", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void OnFail(String error) {

                    }
                });


            }
        });

        bk_img.setOnClickListener(this);


        //    wordsList = (ArrayList<String>) getIntent().getSerializableExtra("wordsList");
        btn_email.setOnClickListener(this);

        et_search.addTextChangedListener(new TextWatcher() {
                                             @Override
                                             public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                                 SendActivity.this.arrayAdapter.getFilter().filter(charSequence);
                                                 arrayAdapter.notifyDataSetChanged();
                                             }

                                             @Override
                                             public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                             }

                                             @Override
                                             public void afterTextChanged(Editable editable) {

                                             }
                                         }
        );

        et_search.setFocusable(false);
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_search.setFocusableInTouchMode(true);
                et_search.setFocusable(true);
            }
        });

      /*  btn_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fooMsgDialog();
            }
        });*/
    }

    public void GetContactsIntoArrayList() {
        Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        contacts = new ArrayList<>();
        if (cursor != null) {
            while (cursor.moveToNext()) {

                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String phonenumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                contacts.add(new Contact(name, phonenumber));
            }
            cursor.close();
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //   Toast.makeText(getApplicationContext(), parent.getId()+" "+id+" " + contacts.get(position).getName() + "\n" + contacts.get(position).getNumber(), Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onClick(View v) {

        if (v == bk_img) {
            onBackPressed();
        } else if (v == btn_email) {
            email_api();

        }
    }


    public void sendEmail() {

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_SUBJECT, "List Name" + "\n" + list_name + "\n" + "Item Names" + "\n" + items_name);

        startActivity(intent);

    }

    private void email_api() {
        emailDialog = new Dialog(this);
        emailDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        emailDialog.setContentView(R.layout.email_dialog);
        emailDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        et_email = emailDialog.findViewById(R.id.et_email_dialog);
        tv_cancel = emailDialog.findViewById(R.id.btn_cancel);
        tv_send = emailDialog.findViewById(R.id.btn_send);
        emailDialog.show();


        tv_cancel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                emailDialog.dismiss();
                return false;
            }
        });

        tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email = et_email.getText().toString();

                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    et_email.setText("");
                    et_email.setHint("Email Format is Wrong");
                } else {
                    ApiNarrator.emailShare(Integer.parseInt(StaticData.id), email, list_id, new ForgoPasswordListener() {
                        @Override
                        public void OnSuccess(int data, int code) {

                            if (code == 1) {
                                emailDialog.dismiss();
                                Toast.makeText(SendActivity.this, "List Sent", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(SendActivity.this, "List not Sent", Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void OnFail(String error) {

                        }
                    });
                }
            }
        });
    }
}
