package com.app.notesapp.activities;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.noteapp.R;
import com.app.notesapp.Sockets.SocketService;
import com.app.notesapp.adapter.ListItemsAdapter;
import com.app.notesapp.adapter.MainActivityAdapter;
import com.app.notesapp.api.ApiNarrator;
import com.app.notesapp.floating_button.PromotedActionsLibrary;
import com.app.notesapp.interfaces.ConnectionListner;
import com.app.notesapp.interfaces.DeleteListListener;
import com.app.notesapp.interfaces.SocketListener;
import com.app.notesapp.models.GsonListItems;
import com.app.notesapp.models.StrikeItem;
import com.app.notesapp.models.SyncContact;
import com.app.notesapp.utils.CommonUtils;
import com.app.notesapp.utils.StaticData;
import com.app.notesapp.vo.SubjectsDataVO;
import com.app.notesapp.vo.WordsDataVO;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.skyfishjy.library.RippleBackground;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

public class SubjectDataActivity extends BaseActivity implements RecognitionListener, SocketListener, ConnectionListner {

    public static final String BASE_URL = "http://138.197.100.210:3002/saveuserlist";
    //    public int holderPosition = -1;
    public int Mainadapterholderposition = -1;
    private ImageView micBTN;
    String subject;
    private TextView toptextmainTV, tv_cancel, btn_ok, btn_cancel;
    private ArrayList<WordsDataVO> wordsList;
    private RecyclerView itemlistLV;
    boolean onResult = false, showAnimation = false;
    private Integer[] buttonsArray;
    private EditText et_dialog;
    private String item;
    private Dialog dialog, dialog_delete, dialogMsg;
    private boolean check = true;
    private RippleBackground rippleBackground;
    private PromotedActionsLibrary promotedActionsLibrary;
    //    private InternetMonitor monitor;
    private RelativeLayout internetcon;
    private LinearLayout nav_bar;
    private TextView btn_msg_send, btn_msg_cancel;
    private EditText et_number;
    private String number;
    private String json;
    private ImageView imgEdit;
    int id = -1;
    LinearLayout linearBottom;
    ListItemsAdapter adp;
    StringBuilder items_name;
    MainActivityAdapter madp;
    Context context = this;
    private Intent recognizerIntent;
    private SpeechRecognizer speech = null;
    private int sendCheck = 0, sendChecker = 0;
    private int statusCode;
    public int txtSize, listeningSpeed, speedCheck;
    LinearLayout drawerLayout;
    View lineBottom, lineBottom2;
    boolean keyboardSwapping=true;
    int micCheck=0;
    Drawable micicon;
    Drawable keyboardIcon;
    Drawable miciconoff;
    Drawable keyboardiconOn;
    LinearLayout linear;
    int height;
    LinearLayout relItemEditTextContainer;
    EditText etItems;
   ImageView btnCancel;


    public static String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      /*  int height = getWindowManager().getDefaultDisplay().getHeight();
        Log.i("screenheight",""+height);*/



        setContentView(R.layout.activity_subject_data);
        SocketService.setSocketListener(this, this);
        setConnectionListner(this);
//        monitor = new InternetMonitor(this);
//        registerReceiver(monitor, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        FrameLayout frameLayout = findViewById(R.id.floatingButton);
        promotedActionsLibrary = new PromotedActionsLibrary();
        promotedActionsLibrary.setup(SubjectDataActivity.this, frameLayout);
        buttonsArray = new Integer[]{0, 0, 0, 0};
        toptextmainTV = findViewById(R.id.toptextmainTV);
        itemlistLV = findViewById(R.id.itemlistLV);
        linearBottom = findViewById(R.id.linear_bottom);
        internetcon = findViewById(R.id.internet);
        nav_bar = findViewById(R.id.rel_nav_bar);
        rippleBackground = findViewById(R.id.content);
        micBTN = findViewById(R.id.micBTN);
        drawerLayout = findViewById(R.id.drawer_layout);
        imgEdit = findViewById(R.id.imgEdit);
        lineBottom = findViewById(R.id.lineBottom);
        lineBottom2 = findViewById(R.id.lineBottom1);
        lineBottom2.setVisibility(View.GONE);
        imgEdit.setVisibility(View.GONE);

        linear=findViewById(R.id.relLinearRecycler);
        relItemEditTextContainer=findViewById(R.id.rel_itemedittext);
        etItems=findViewById(R.id.et_items);
        btnCancel=findViewById(R.id.btn_cancel);

        //CommonUtils.setFontAwesome(new View[]{msg_icon});



        Intent iin = getIntent();
        Bundle b = iin.getExtras();
        if (b != null) {
            id = b.getInt("ID");
        }


        Log.i("abcd", "" + id);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        itemlistLV.setLayoutManager(layoutManager);
        itemlistLV.setHasFixedSize(true);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            subject = extras.get("SUBJECT").toString();
            sendCheck = extras.getInt("sendCheck");
            statusCode = extras.getInt("statusCode");
            txtSize = extras.getInt("txtsize");
            micCheck=extras.getInt("micCheck");
            listeningSpeed = extras.getInt("listeningSpeed");
            toptextmainTV.setText(subject.toUpperCase());
            toptextmainTV.setPaintFlags(toptextmainTV.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        StaticData.MicCheck=micCheck;
        Log.i("textsizeerror", "" + txtSize);
       // CommonUtils.changeTextSize(drawerLayout, txtSize);
StaticData.DeleteCheck=statusCode;


if(statusCode==2)
{
    imgEdit.setVisibility(View.VISIBLE);
    lineBottom2.setVisibility(View.VISIBLE);

    Handler handler = new Handler();
    handler.postDelayed(new Runnable() {
        @Override
        public void run() {

            Log.i("height1",""+linear.getHeight());
            Log.i("height3",""+linear.getHeight()/4);
            height=linear.getHeight()/4;

        }
    },1);

}
else{
    imgEdit.setVisibility(View.GONE);
    lineBottom2.setVisibility(View.GONE);
    LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT,
            1.0f
    );
    linear.setLayoutParams(param);

    Handler handler = new Handler();
    handler.postDelayed(new Runnable() {
        @Override
        public void run() {

            Log.i("height2",""+linear.getHeight());
            Log.i("height4",""+linear.getHeight()/5);
            height=linear.getHeight()/5;


        }
    },1);
}
        imgEdit.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        imgEdit.setVisibility(View.GONE);
                        lineBottom2.setVisibility(View.GONE);
                        linearBottom.setVisibility(View.VISIBLE);
                        lineBottom.setVisibility(View.VISIBLE);

                    }
                });

        if (id > 0) {
            Log.i("working", "" + id);
            linearBottom.setVisibility(View.INVISIBLE);
            lineBottom.setVisibility(View.INVISIBLE);
            ApiNarrator.retrieveItems(context, id);
        } else {
            Log.i("iderror", "" + id);
            imgEdit.setVisibility(View.GONE);
            lineBottom.setVisibility(View.VISIBLE);
            linearBottom.setVisibility(View.VISIBLE);
        }


        wordsList = new ArrayList<>();
        populateList(wordsList);

        findViewById(R.id.backBTN).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sendChecker != 1) {
                    try {
                        speech.stopListening();
                    } catch (Exception e) {
                        Log.e("speech.stopListening", "" + e.getMessage());
                    }
                    addToSubjectList();
                    SubjectDataActivity.this.finish();
                } else {
                    Toast.makeText(SubjectDataActivity.this, "Please Save List Items", Toast.LENGTH_SHORT).show();
                }


            }
        });

        Log.i("miccheck"," "+micCheck);
        if(micCheck==1)
        {
           // micBTN.setImageResource(R.drawable.mic_icon);
            micBTN.setImageResource(R.drawable.mic_icon_on);
            rippleBackground.startRippleAnimation();


        }
        else if(micCheck==3)
        {

        }
        else if(micCheck==2||micCheck==4)
        {
            micBTN.setImageResource(R.drawable.keyboard_icon);
        }
  /*      micBTN.setImageResource(R.drawable.mic_icon);*/
        micicon = getResources().getDrawable(R.drawable.mic_icon_on);
        keyboardIcon=getResources().getDrawable(R.drawable.keyboard_icon);
        miciconoff=getResources().getDrawable(R.drawable.mic_icon_small);
        keyboardiconOn=getResources().getDrawable(R.drawable.keyboard);
        final Drawable savedrawable=getResources().getDrawable(R.drawable.save);
        final Drawable deleteDrawable=getResources().getDrawable(R.drawable.delete);

        final Bitmap bmapmain = ((BitmapDrawable) micBTN.getDrawable()).getBitmap();
        final Bitmap keyBoard2=((BitmapDrawable)keyboardIcon).getBitmap();
        final Bitmap myLogo2 = ((BitmapDrawable) miciconoff).getBitmap();
        if(bmapmain.sameAs(keyBoard2)||bmapmain.sameAs(myLogo2))
        {
            CommonUtils.ZoomOut(this,micBTN);
        }


        micBTN.setOnClickListener(new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {

                micBTN.setAnimation(null);

                final Bitmap bmap = ((BitmapDrawable) micBTN.getDrawable()).getBitmap();
                final Bitmap myLogo = ((BitmapDrawable) miciconoff).getBitmap();
                final Bitmap keyBoard=((BitmapDrawable)keyboardIcon).getBitmap();
                final Bitmap miciConon=((BitmapDrawable)micicon).getBitmap();
                final Bitmap keyboardICon=((BitmapDrawable)keyboardiconOn).getBitmap();
                final Bitmap saveBitmap=((BitmapDrawable)savedrawable).getBitmap();
                final Bitmap deleteBitmap=((BitmapDrawable)deleteDrawable).getBitmap();

                if (buttonsArray[0] == 1) {

                  //  Toast.makeText(SubjectDataActivity.this, "List Created", Toast.LENGTH_LONG).show();
                    speech.stopListening();
                 //   SendList();
                   // SubjectDataActivity.this.finish();
                } /*else if (buttonsArray[1] == 1) {

                    rippleBackground.stopRippleAnimation();
                    speech.stopListening();
                    if (statusCode == 2) {
                        foo_DialogBox();
                    } else {
                        Toast.makeText(SubjectDataActivity.this, "you don't have access to modify this list", Toast.LENGTH_LONG).show();
                    }

                    //   Toast.makeText(SubjectDataActivity.this, "KEYBOARD BUTTON", Toast.LENGTH_LONG).show();

                }*/
                else if(bmap.sameAs(keyBoard))
                {
                    micBTN.clearAnimation();
                    rippleBackground.stopRippleAnimation();
                    micBTN.setImageResource(R.drawable.keyboard);
                    speech.stopListening();
                    if (statusCode == 2) {
                addItems();
                    } else {
                        Toast.makeText(SubjectDataActivity.this, "you don't have access to modify this list", Toast.LENGTH_LONG).show();
                    }

                }

                else if (buttonsArray[3] == 1) {


                    StringBuilder arrayList = new StringBuilder();
                    StringBuilder arrayList2 = new StringBuilder();
                    for (int i = 0; i < ApiNarrator.items_name_list.size(); i++) {
                        arrayList.append("\n");
                        arrayList.append(ApiNarrator.items_name_list.get(i).getItemName());

                    }

                    for (int i = 0; i < wordsList.size(); i++) {
                        arrayList2.append("\n");
                        arrayList2.append(wordsList.get(i).getlist_item());
                    }
                  /*  Toast.makeText(SubjectDataActivity.this, "SEND BUTTON", Toast.LENGTH_LONG).show();*/

                    if (sendCheck != 1 && sendChecker != 1) {
                        if (wordsList != null) {
                            Intent intent = new Intent(SubjectDataActivity.this, SendActivityRecyclerview.class);

                            Log.i("arraylist", id + " " + String.valueOf(arrayList));
                            if (id == -1) {

                                intent.putExtra("Items_Name", (Serializable) arrayList2);
                            } else if (id >= 0) {
                                intent.putExtra("Items_Name", (Serializable) arrayList);

                            }

                            intent.putExtra("list_id", id);
                            intent.putExtra("List_Name", subject);
                            startActivity(intent);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Please Save List to Send", Toast.LENGTH_SHORT).show();
                    }

                } /*else if (buttonsArray[2] == 1) {

                    foo_DialogBox_Delete();
                }
                */
                else if(bmap.equals(deleteBitmap))
                {
                    foo_DialogBox_Delete();
                }
                else if (bmap.sameAs(myLogo)) {

                    if (check) {

                        Log.i("asdf", "" + statusCode);
                        if (statusCode == 2) {
                            CommonUtils.bounce(SubjectDataActivity.this, micBTN);
                            rippleBackground.startRippleAnimation();
                            //CommonUtils.bottomup(getApplicationContext(),micBTN);
                            micBTN.setImageResource(R.drawable.mic_icon_on);
                            check = false;

                            if (checkForPermission()) {

                                startMic();
                            } else {
                                Toast.makeText(SubjectDataActivity.this, "Please Grant asked permissions", Toast.LENGTH_LONG).show();
                            }
                            Handler micCheckHandler = new Handler();
                            micCheckHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    rippleBackground.stopRippleAnimation();
                                    micBTN.setImageResource(R.drawable.mic_icon_small);
                                }
                            },12000);
                        } else {
                            Toast.makeText(SubjectDataActivity.this, "you don't have access to modify this list", Toast.LENGTH_LONG).show();
                        }

                    } else if (!check) {
                        rippleBackground.stopRippleAnimation();
                        micBTN.setImageResource(R.drawable.mic_icon_small);
                        check = true;
                        speech.stopListening();
                    }

                }
                //Keyboard Button

             /*   else if (bmap.sameAs(keyBoard)) {
                    rippleBackground.stopRippleAnimation();
                    speech.stopListening();
                    micBTN.setImageResource(R.drawable.keyboard);
                    if (statusCode == 2) {
                        foo_DialogBox();
                    } else {
                        Toast.makeText(SubjectDataActivity.this, "you don't have access to modify this list", Toast.LENGTH_LONG).show();
                    }


                }*/
                else if(bmap.sameAs(miciConon))
                {
                    micBTN.setImageResource(R.drawable.mic_icon_small);
                    speech.stopListening();
                    rippleBackground.stopRippleAnimation();

                }
                else if(bmap.sameAs(keyboardICon))
                {
                    micBTN.setImageResource(R.drawable.keyboard_icon);

                }
            }
        });
        View.OnClickListener onClickListenerSave = new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                // Toast.makeText(SubjectDataActivity.this, "SAVE BUTTON", Toast.LENGTH_LONG).show();
                rippleBackground.stopRippleAnimation();
                speech.stopListening();
                if (buttonsArray[0] == 1) {
                    //micBTN.setImageResource(R.drawable.mic_icon);
                    CommonUtils.bounce(SubjectDataActivity.this, micBTN);
                    micBTN.setImageResource(R.drawable.mic_icon_small);
                    promotedActionsLibrary.resetAllItems();
                    resetButtonArray();
                } else {
                    //  micBTN.setImageResource(R.drawable.mic_icon);
                    resetButtonArray();
                    buttonsArray[0] = 1;
                    // micBTN.setImageResource(R.drawable.save_icon);
                    CommonUtils.bounce(SubjectDataActivity.this, micBTN);
                    micBTN.setImageResource(R.drawable.save);
                    promotedActionsLibrary.resetAllItems();
                    promotedActionsLibrary.getItem(0, R.drawable.mic_icon_small, true);
                    promotedActionsLibrary.getItem(1, R.drawable.keyboard_icon, true);
             showProgress();

                }
            }
        };
        View.OnClickListener onClickListenerKeyboard = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
           /*     rippleBackground.stopRippleAnimation();
                speech.stopListening();
                if (buttonsArray[1] == 1) {
                    CommonUtils.bounce(SubjectDataActivity.this, micBTN);
                    micBTN.setImageResource(R.drawable.mic_icon);
                    promotedActionsLibrary.resetAllItems();
                    resetButtonArray();
                } else {
                    resetButtonArray();
                    buttonsArray[1] = 1;
                    CommonUtils.bounce(SubjectDataActivity.this, micBTN);
                    micBTN.setImageResource(R.drawable.keyboard);
                    // micBTN.setImageResource(R.drawable.keyboard_icon);
                    promotedActionsLibrary.resetAllItems();
                    promotedActionsLibrary.getItem(1, R.drawable.mic_icon, true);
                }*/

                rippleBackground.stopRippleAnimation();
                speech.stopListening();
                if (buttonsArray[1] == 1) {


                    buttonsArray[1] = 1;
                    CommonUtils.bounce(SubjectDataActivity.this, micBTN);
                    micBTN.setImageResource(R.drawable.keyboard);
                    // micBTN.setImageResource(R.drawable.keyboard_icon);
                    promotedActionsLibrary.resetAllItems();

                } else {
                    CommonUtils.bounce(SubjectDataActivity.this, micBTN);
                    micBTN.setImageResource(R.drawable.keyboard_icon);
                    CommonUtils.ZoomOut(SubjectDataActivity.this,micBTN);
                    promotedActionsLibrary.getItem(1, R.drawable.mic_icon_small, true);
                   // promotedActionsLibrary.resetAllItems();
                    //  resetButtonArray();
                }
            }
        };

        //MIC LISTENER
        View.OnClickListener onClickListenerMic= new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rippleBackground.stopRippleAnimation();
                speech.stopListening();
                if (buttonsArray[1] == 1) {

                    resetButtonArray();
                    buttonsArray[1] = 1;
                    CommonUtils.bounce(SubjectDataActivity.this, micBTN);
                    micBTN.setImageResource(R.drawable.keyboard);
                    // micBTN.setImageResource(R.drawable.keyboard_icon);
                    promotedActionsLibrary.resetAllItems();
                    Log.i("working123","aadasfsf");

                } else if(keyboardSwapping){
                    CommonUtils.bounce(SubjectDataActivity.this, micBTN);
                    micBTN.setImageResource(R.drawable.mic_icon_small);
                    CommonUtils.bounce(SubjectDataActivity.this, micBTN);
                    promotedActionsLibrary.resetAllItems();
                    resetButtonArray();
                    promotedActionsLibrary.getItem(1, R.drawable.keyboard_icon, true);

                    Log.i("working1234===","aadasfsf");
                    keyboardSwapping=false;
                }
                else{
                    CommonUtils.bounce(SubjectDataActivity.this, micBTN);
                    micBTN.setImageResource(R.drawable.keyboard_icon);
                    CommonUtils.ZoomOut(SubjectDataActivity.this, micBTN);
                    promotedActionsLibrary.resetAllItems();
                    resetButtonArray();
                    promotedActionsLibrary.getItem(1, R.drawable.mic_icon_small, true);

                    Log.i("working12345===","aadasfsf");
                    keyboardSwapping=true;
                }
            }
        };

        View.OnClickListener onClickListenerDelete = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rippleBackground.stopRippleAnimation();
                speech.stopListening();
                if (buttonsArray[2] == 1) {
                    CommonUtils.bounce(SubjectDataActivity.this, micBTN);
                    micBTN.setImageResource(R.drawable.mic_icon_small);
                    CommonUtils.ZoomOut(SubjectDataActivity.this,micBTN);
                    promotedActionsLibrary.resetAllItems();
                    resetButtonArray();
                    promotedActionsLibrary.getItem(1, R.drawable.keyboard_icon, true);
                } else {
                    resetButtonArray();
                    buttonsArray[2] = 1;
                    CommonUtils.bounce(SubjectDataActivity.this, micBTN);
                    micBTN.setImageResource(R.drawable.delete);
                    // micBTN.setImageResource(R.drawable.delete_icon);
                    promotedActionsLibrary.resetAllItems();
                    promotedActionsLibrary.getItem(0, R.drawable.mic_icon_small, false);
                    promotedActionsLibrary.getItem(1, R.drawable.keyboard_icon, true);
                }
            }
        };
        View.OnClickListener onClickListenerSend = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rippleBackground.stopRippleAnimation();
                speech.stopListening();
                if (buttonsArray[3] == 1) {
                    CommonUtils.bounce(SubjectDataActivity.this, micBTN);
                    micBTN.setImageResource(R.drawable.mic_icon_small);
                    CommonUtils.ZoomOut(SubjectDataActivity.this,micBTN);
                    promotedActionsLibrary.resetAllItems();
                    resetButtonArray();
                    promotedActionsLibrary.getItem(1, R.drawable.keyboard_icon, true);
                } else {
                    resetButtonArray();
                    buttonsArray[3] = 1;
                    CommonUtils.bounce(SubjectDataActivity.this, micBTN);
                    micBTN.setImageResource(R.drawable.send);
                    //   micBTN.setImageResource(R.drawable.send_icon);
                    promotedActionsLibrary.resetAllItems();
                    promotedActionsLibrary.getItem(1, R.drawable.mic_icon_small, false);
                    promotedActionsLibrary.getItem(1, R.drawable.keyboard_icon, true);
                }

            }
        };
        promotedActionsLibrary.addItem(getResources().getDrawable(R.drawable.save_icon), onClickListenerSave);

        Log.i("micCheck2",micCheck+"\t\t"+StaticData.MicCheck);
        if(micCheck==2||micCheck==4)
        {
            promotedActionsLibrary.addItem(getResources().getDrawable(R.drawable.mic_icon_small), onClickListenerMic);
           micCheck=1;
            keyboardSwapping=false;

        }
    else if(micCheck==1||micCheck==3)
        {
            keyboardSwapping=false;
            micCheck=2;
            promotedActionsLibrary.addItem(getResources().getDrawable(R.drawable.keyboard_icon), onClickListenerMic);
        }


        promotedActionsLibrary.addItem_(getResources().getDrawable(R.drawable.delete_icon), onClickListenerDelete);
        promotedActionsLibrary.addItem_(getResources().getDrawable(R.drawable.send_icon), onClickListenerSend);

       /* msg_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fooMsgDialog();
            }
        });*/

      /* if(adp!=null)
       {

       }
       else{
           Log.i("error","while setting text size");
       }*/
    }

    @Override
    public void onReadyForSpeech(Bundle params) {
        Log.e("onReadyForSpeech: ", "In here");
        micBTN.setImageResource(R.drawable.mic_icon_on);
    }

    @Override
    public void onBeginningOfSpeech() {
        Log.e("onBeginningOfSpeech: ", "In here");
        micBTN.setImageResource(R.drawable.mic_icon_on);
    }

    @Override
    public void onRmsChanged(float rmsdB) {
        if (onResult) {
            Log.e("onRmsChanged: ", "" + rmsdB);
        }
    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.e("onBufferReceived: ", "In here");
    }

    @Override
    public void onEndOfSpeech() {
        Log.e("onEndOfSpeech: ", "In here");
        onBeginningOfSpeech();
    }

    @Override
    public void onError(int error) {
        Log.e("onError: ", "In here");
    }

    @Override
    public void onResults(final Bundle results) {
        Log.e("onResults: ", "In here");

        if (listeningSpeed == 1) {
            speedCheck = 1000;
        } else if (listeningSpeed == 2) {
            speedCheck = 2000;
        } else if (listeningSpeed == 3) {
            speedCheck = 3000;
        }
        else if (listeningSpeed == 4) {
            speedCheck = 4000;
        }
        else if (listeningSpeed == 5) {
            speedCheck = 5000;
        }
        else if (listeningSpeed == 6) {
            speedCheck = 6000;
        }
        final Handler handler2 = new Handler();
        handler2.postDelayed(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                onResult = false;
                StaticData.words.add(matches.get(0) + "");
                wordsList.add(new WordsDataVO("1", matches.get(0) + ""));
                populateList(wordsList);
                startAgainListener();
            }
        }, speedCheck);


    }

    @Override
    public void onPartialResults(Bundle partialResults) {
        Log.e("onPartialResults: ", "In here");
    }

    @Override
    public void onEvent(int eventType, Bundle params) {
        Log.e("onEvent: ", "In here");
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (speech != null) {
            speech.stopListening();
            speech = null;
        }
    }

    private void stopListener() {
        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
                //do nothing, just let it tick
            }

            public void onFinish() {
                rippleBackground.stopRippleAnimation();
                //  speech.stopListening();
                micBTN.setImageResource(R.drawable.mic_icon_small);
                micBTN.setVisibility(View.VISIBLE);
                Log.e("stopListener", "onFinish");
                try {
                    speech.stopListening();
                } catch (Exception e) {
                    Log.e("speech.stopListening", "" + e.getMessage());
                }
            }
        }.start();
    }

    private void startAgainListener() {
        rippleBackground.startRippleAnimation();
        micBTN.setImageResource(R.drawable.mic_icon_on);
        micBTN.setVisibility(View.VISIBLE);
        Log.e("startAgainListener", "onFinish");
        onResult = true;
        try {
            speech.startListening(recognizerIntent);
        } catch (Exception e) {
            Log.e("speech.stopListening", "" + e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        micBTN.setImageResource(R.drawable.mic_icon_small);
        micBTN.setVisibility(View.VISIBLE);
        try {
            speech.stopListening();
        } catch (Exception e) {
            Log.e("speech.stopListening", "" + e.getMessage());
        }
        addToSubjectList();
        super.onBackPressed();
    }

    public void populateList(ArrayList<WordsDataVO> list) {
        if (adp != null) {
            Log.i("this2working","working");
            GsonListItems gsonListItems = new GsonListItems();
            gsonListItems.setItemName(list.get(list.size() - 1).getlist_item());
            ApiNarrator.items_name_list.add(gsonListItems);
          adp.addList(ApiNarrator.items_name_list);
            itemlistLV.scrollToPosition(list.size() - 1);
            height=linear.getHeight()/4;
            adp.cardViewHeight(height);
            adp.notifyDataSetChanged();
            sendChecker = 1;
        }else{
            Log.i("thisworking","working");
            ApiNarrator.items_name_list = new ArrayList<>();
            adp = new ListItemsAdapter(ApiNarrator.items_name_list, context);
            itemlistLV.setAdapter(adp);
            adp.cardViewHeight(height);
            adp.changeSize(txtSize);
            adp.notifyDataSetChanged();
        }
    }

    public void apiCall() {
        adp = new ListItemsAdapter(ApiNarrator.items_name_list, context);
        itemlistLV.scrollToPosition(ApiNarrator.items_name_list.size() - 1);
        itemlistLV.setAdapter(adp);
        adp.cardViewHeight(height);
        adp.changeSize(txtSize);
    }

    @Override
    protected void onResume() {
        super.onResume();

        initSpeech();
    }

    private void addToSubjectList() {

        if (wordsList != null && wordsList.size() > 0) {
            if (StaticData.SAVED_LETTERS_DATA.SAVED_WORDES != null && StaticData.SAVED_LETTERS_DATA.SAVED_WORDES.size() > 0) {
                StaticData.SAVED_LETTERS_DATA.SAVED_WORDES.put(subject, new SubjectsDataVO(wordsList, "NONE", "NONE", "NONE", "NONE", subject.toLowerCase()));
            } else {
                StaticData.SAVED_LETTERS_DATA.SAVED_WORDES = new HashMap<>();
                StaticData.SAVED_LETTERS_DATA.SAVED_WORDES.put(subject, new SubjectsDataVO(wordsList, "NONE", "NONE", "NONE", "NONE", subject.toLowerCase()));
            }
        }
    }

    private void startMic() {
        initSpeech();
        if (!showAnimation) {
            showAnimation = true;
            promotedActionsLibrary.addMainItem(getResources().getDrawable(R.drawable.empty));
        }
        onResult = true;
        try {
            speech.startListening(recognizerIntent);
        } catch (Exception e) {
            Log.e("speech.stopListening", "" + e.getMessage());
        }
    }

    private void initSpeech() {
        if (speech == null) {
            promotedActionsLibrary.addMainItem(getResources().getDrawable(R.drawable.empty));
            speech = SpeechRecognizer.createSpeechRecognizer(this);
            speech.setRecognitionListener(this);
            recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, this.getPackageName());
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 0);
            try {
                speech.startListening(recognizerIntent);
            } catch (Exception e) {
                Log.e("speech.stopListening", "" + e.getMessage());
            }
        }
    }

    private boolean checkButtonsArray() {
        return buttonsArray[0] == 0 && buttonsArray[1] == 0 && buttonsArray[2] == 0 && buttonsArray[3] == 0;
    }

    private void resetButtonArray() {
        buttonsArray = new Integer[]{0, 0, 0, 0};
    }

    @TargetApi(23)
    public boolean checkForPermission() {

        String hasVoiceRecordingPermission = android.Manifest.permission.RECORD_AUDIO;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(hasVoiceRecordingPermission) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{hasVoiceRecordingPermission}, 010);
            } else {
                return true;
            }
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 010:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startMic();
                } else {
                    Toast.makeText(SubjectDataActivity.this, "Permission Not Granted.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

  /*  public void foo_DialogBox() {

        final InputMethodManager imm = (InputMethodManager)this.getSystemService(Service.INPUT_METHOD_SERVICE);
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogsubdata);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        tv_cancel = dialog.findViewById(R.id.tv_cancel);
        et_dialog = dialog.findViewById(R.id.et_dialog);
        et_dialog.setSingleLine(true);
        et_dialog.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        et_dialog.setFocusable(false);
        dialog.setCancelable(false);

        et_dialog.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent event) {

                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (i == EditorInfo.IME_ACTION_NEXT)) {
                    item = et_dialog.getText().toString().trim();
                    if (!item.isEmpty()) {
                        wordsList.add(new WordsDataVO("1", item + ""));
                        populateList(wordsList);
                        et_dialog.setText("");
                        sendChecker = 1;
                    }

                }
                return true;
            }
        });
        dialog.show();

        tv_cancel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                imm.hideSoftInputFromWindow(et_dialog.getWindowToken(), 0);
                micBTN.setImageResource(R.drawable.keyboard_icon);
                dialog.dismiss();
                return false;
            }
        });
     *//*   add_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });*//*
        et_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_dialog.setFocusableInTouchMode(true);
             et_dialog.setFocusable(true);
            }
        });


    }
*/
    public void addItems()
    {

        itemlistLV.setVisibility(View.GONE);
        final InputMethodManager imm = (InputMethodManager)this.getSystemService(Service.INPUT_METHOD_SERVICE);
        relItemEditTextContainer.setVisibility(View.VISIBLE);
        btnCancel.setVisibility(View.VISIBLE);

        etItems.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent event) {

                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (i == EditorInfo.IME_ACTION_NEXT)) {
                    item = etItems.getText().toString().trim();
                    if (!item.isEmpty()) {
                        wordsList.add(new WordsDataVO("1", item + ""));
                        populateList(wordsList);
                     etItems.setText("");
                        sendChecker = 1;
                    }

                }
                return true;
            }
        });
    etItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              etItems.setFocusableInTouchMode(true);
            etItems.setFocusable(true);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                relItemEditTextContainer.setVisibility(View.GONE);
                btnCancel.setVisibility(View.GONE);
                imm.hideSoftInputFromWindow(etItems.getWindowToken(), 0);
                itemlistLV.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.GONE);

                micBTN.setImageResource(R.drawable.keyboard_icon);
            }
        });
    }


    public void foo_DialogBox_Delete() {
  /*      dialog_delete = new Dialog(this);
        dialog_delete.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_delete.setContentView(R.layout.warning_dialog);
        dialog_delete.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        btn_ok = dialog_delete.findViewById(R.id.btn_ok);
        TextView deleteText=dialog_delete.findViewById(R.id.deletTxt);

    deleteText.setTypeface(CommonUtils.getInstance().getCosmicSansFont(SubjectDataActivity.this));
        btn_cancel = dialog_delete.findViewById(R.id.btn_cancel);
        dialog_delete.setCancelable(false);
        dialog_delete.show();

        btn_cancel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dialog_delete.dismiss();
                return false;
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {*/


                ApiNarrator.deleteList(context, id, new DeleteListListener() {
                    @Override
                    public void OnDeleteList() {
                        try {
                            madp.notifyItemRemoved(Mainadapterholderposition);
                            ApiNarrator.lists.remove(Mainadapterholderposition);

                        } catch (NullPointerException ex) {
                            Log.i("Null Pointer Excception", ex.toString());
                        }

                    }

                    @Override
                    public void OnDeleteListFailure() {

                    }
                });
                Toast.makeText(getApplicationContext(), "List Deleted", Toast.LENGTH_SHORT).show();
                onBackPressed();

            }

    void onDataConnected() {
        internetcon.setVisibility(View.GONE);
        nav_bar.setVisibility(View.VISIBLE);
    }

    void onDataDisConnected() {
        internetcon.setVisibility(View.VISIBLE);
        nav_bar.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeConnectionListner();
//        unregisterReceiver(monitor);
        SocketService.removeSocketListner();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void SendList() {
        if (StaticData.id != null) {
            JSONArray array = new JSONArray();
            try {
                for (int i = 0; i < wordsList.size(); i++) {
                    array.put(new JSONObject(wordsList.get(i).toString()));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            JSONObject data = new JSONObject();
            try {
                if (id >= 0) {
                    data.put("listid", id);
                }
                data.put("save", 1);
                data.put("uid", Integer.parseInt(StaticData.id));
                data.put("list_name", subject);
                data.put("list_item", array);
                Log.i("Json ", "" + array);
                Log.i("Data ", "" + data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            params.put("data", data);
            Log.i("ParamsAct", params.toString());
            client.post(BASE_URL, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.i("Test", "response" + response);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.i("Testt", "" + throwable);
                }
            });
        } else {
            Log.i("uid", "id is null");
        }
    }

    /*  public void fooMsgDialog() {

          items_name = new StringBuilder();
          if (ApiNarrator.items_name_list != null) {

              for (int i = 0; i < ApiNarrator.items_name_list.size(); i++) {
                  items_name.append("\n");
                  items_name.append(ApiNarrator.items_name_list.get(i).getItemName());
                  Log.i("Array", ApiNarrator.items_name_list.get(i).getItemName());
              }

          }
          dialogMsg = new Dialog(this);
          dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
          dialogMsg.setContentView(R.layout.msg_dialog);
          dialogMsg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
          btn_msg_send = dialogMsg.findViewById(R.id.btn_send);
          btn_msg_cancel = dialogMsg.findViewById(R.id.btn_cancel);
          et_number= dialogMsg.findViewById(R.id.et_msg_dialog);
          dialogMsg.setCancelable(false);
          dialogMsg.show();

          btn_msg_cancel.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  dialogMsg.dismiss();
              }
          });

          btn_msg_send.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View view) {
                                                  number=et_number.getText().toString().trim();

                                                  if(!number.isEmpty()&&id>0)
                                                  {

                                                      ApiNarrator.shareList(Integer.parseInt(StaticData.id),number, id, new ForgoPasswordListener() {
                                                          @Override
                                                          public void OnSuccess(int data, int code) {
                                                              dialogMsg.dismiss();
                                                              Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                                              smsIntent.setType("vnd.android-dir/mms-sms");
                                                              smsIntent.putExtra("address", number);
                                                              smsIntent.putExtra("sms_body", "List Name" + "\n" + subject + "\n" + "Item Names" + "\n" + items_name);
                                                              context.startActivity(smsIntent);
                                                              Log.i("list3", "" + items_name);
                                                          }

                                                          @Override
                                                          public void OnFail(String error) {
                                                              Toast.makeText(SubjectDataActivity.this,"List not Sent",Toast.LENGTH_SHORT).show();
                                                          }
                                                      });


                                                  }
                                                  else
                                                  {
                                                      et_number.setText("");
                                                      et_number.setHint("Please Enter Number Here");
                                                  }


                                              }
                                          }
          );
      }*/
    @Override
    public void onWifiConnected() {
        onDataConnected();
    }

    @Override
    public void onWifiDisconnected() {
        onDataDisConnected();
    }

    @Override
    public void onMobileDataConnected() {
        onDataConnected();
    }

    @Override
    public void onMobileDataDisconnected() {
        onDataDisConnected();
    }

    /// Socket listner

    @Override
    public void onConnect(String data) {
    }

    @Override
    public void onDisconnect(String data) {
    }

    @Override
    public void onMessage(String data) {
    }

    @Override
    public void onCrossOff(StrikeItem strikeItem) {
        if (adp != null) {
            if (strikeItem.getCrossOffBy() != Integer.parseInt(StaticData.id))
                adp.onCrossOffAction(strikeItem);
        }
    }

    @Override
    public void onPhoneCheck(ArrayList<SyncContact> syncContacts) {
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showProgress() {

        SendList();
        final ProgressDialog dialog = new ProgressDialog(SubjectDataActivity.this);
        dialog.setMessage("List is Saving...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(false);
        dialog.show();

        Handler handler1=new Handler();
        handler1.postDelayed(new Runnable() {
            @Override
            public void run() {

                dialog.dismiss();
                Toast.makeText(SubjectDataActivity.this, "List Created", Toast.LENGTH_LONG).show();
                SubjectDataActivity.this.finish();
            }
        },1500);
    }

}