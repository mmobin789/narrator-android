package com.app.notesapp.api;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.app.notesapp.Sockets.SocketService;
import com.app.notesapp.activities.LoginActivity;
import com.app.notesapp.activities.MainActivity;
import com.app.notesapp.activities.SubjectDataActivity;
import com.app.notesapp.enums.AuthType;
import com.app.notesapp.interfaces.BirthdayListener;
import com.app.notesapp.interfaces.CopyListener;
import com.app.notesapp.interfaces.DeleteItemListner;
import com.app.notesapp.interfaces.DeleteListListener;
import com.app.notesapp.interfaces.ForgoPasswordListener;
import com.app.notesapp.models.Auth;
import com.app.notesapp.models.GsonListItems;
import com.app.notesapp.models.MainActivityList;
import com.app.notesapp.models.User;
import com.app.notesapp.utils.StaticData;
import com.app.notesapp.utils.StoragePreference;
import com.app.notesapp.vo.WordsDataVO;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import cz.msebera.android.httpclient.Header;

/**
 * Created by TreeHousestudio on 7/21/2017.
 */

public class ApiNarrator {

    private static String baseUrl = "http://138.197.100.210:3002/";

    public static MainActivityList activityList;
    public static JSONArray list;
    public static ArrayList<MainActivityList> lists;
    public static ArrayList<GsonListItems> items_name_list;
    public static JSONArray items_list;
    public static ArrayList<GsonListItems> smsList = null;

    public static ArrayList<WordsDataVO> wordsList;
    static int userid;
    static String user_id;
    static int response_code;
    static int data;
    static String imgurl;
    static int count;
    static  String dob;

    private static int i=0;

    public static void auth(final OnAuthListener listener, User user, AuthType authType) {

        AsyncHttpClient loginObj = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        String url = "";
        switch (authType) {
            case LOGIN:
                url = "login";
                params.put("phone", user.phone);
                params.put("password", user.password);
                i=1;
                break;
            case REGISTER:
                i=2;
                url = "insertuser";
                params.put("name", user.name);
                params.put("email", user.email);
                params.put("phone", user.phone);
                params.put("password", user.password);
                params.put("dob",user.dob);


                Log.i("dob",params.toString());
                break;

            case SOCIAL:
                url = "sociallogininsert";
                params.put("username", User.socialUser.first_name);
                params.put("email", User.socialUser.email);
                params.put("socialid", User.socialUser.id);
                break;

        }

        loginObj.post(baseUrl + url, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.i("testing", "onSuccess: " + response);
                try{

                    if(i==1)
                    {
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(String.valueOf(response));
                            obj = obj.getJSONObject("Response");
                            JSONObject array = obj.getJSONObject("Userdata");
                            StaticData.id = String.valueOf(array.getInt("id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    if(i==2)
                    {
                        JSONObject obj;
                        try {
                            obj = new JSONObject(String.valueOf(response));
                            obj = obj.getJSONObject("Response");
                            JSONObject array = obj.getJSONObject("Userdata");
                            StaticData.id = String.valueOf(array.getInt("insertId"));

                            Log.i("Static Data",""+StaticData.id);
                        } catch (JSONException e) {
                            Log.i("Exception",""+e.toString());
                        }

                    }

                    Auth auth = new Gson().fromJson(response.toString(), Auth.class);
                    listener.onAuthSuccess(auth);


                }
                catch (IllegalStateException | JsonSyntaxException exception)
                {
                    LoginActivity.login.setText("Login Failed");
                }



            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                listener.OnError(throwable.toString());

            }


        });

    }

    public static void retrieveList(final Context context) {
        lists = new ArrayList<>();
        if (!StaticData.id.isEmpty()&&Integer.parseInt(StaticData.id)!=0) {
            StoragePreference.setUserId(context,StaticData.id);
            context.startService(new Intent(context, SocketService.class));
            AsyncHttpClient client = new AsyncHttpClient();
            client.setMaxRetriesAndTimeout(0,3000);
            RequestParams params = new RequestParams();

            params.put("uid", StaticData.id);
            Log.i("testing", String.valueOf(params));
            client.post(baseUrl + "getuserlist", params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.i("Retrieve List", "data" + response);
                    try {
                        JSONObject res = response.getJSONObject("Response");
                        list = res.getJSONArray("data");
                        for (int i = 0; i < list.length(); i++) {
                            Gson gson = new Gson();
                            lists.add(gson.fromJson(list.get(i).toString(), MainActivityList.class));
                        }

                        Collections.sort(lists, new Comparator<MainActivityList>() {

                            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                            @Override
                            public int compare(MainActivityList o1, MainActivityList o2) {
                                int res = Integer.compare(o1.getId(), o2.getId());
                                if(res == 0)
                                    return o1.getId()-o2.getId();
                                return res;
                            }
                        });

                        for(MainActivityList list:lists)
                        {
                            Log.i("iterator",""+list);
                        }

                        ((MainActivity) context).initializeAdapter();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.i("Testt", "" + throwable);
                }
            });

        }

    }

    public static void retrieveItems(final Context context, int list_id) {
        items_name_list = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        params.put("listid", list_id);
        Log.i("testing", String.valueOf(params));
        client.post(baseUrl + "getuserlistitems", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.i("Test", "" + response);
                try {
                    JSONObject res = response.getJSONObject("Response");
                    items_list = res.getJSONArray("list_items");
                    for (int i = 0; i < items_list.length(); i++) {
                        Gson gson = new Gson();
                        items_name_list.add(gson.fromJson(items_list.get(i).toString(), GsonListItems.class));

                    }
                    //     ((ListItemAcitivity)context).initAdapter();
                    ((SubjectDataActivity) context).apiCall();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.i("Testt", "" + throwable);
            }
        });


    }

    public static void retrieveItems2(final Context context, int list_id) {
        items_name_list = new ArrayList<>();
        smsList=new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        params.put("listid", list_id);
        Log.i("testing", String.valueOf(params));
        client.post(baseUrl + "getuserlistitems", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.i("Test", "" + response);
                try {
                    JSONObject res = response.getJSONObject("Response");
                    items_list = res.getJSONArray("list_items");
                    smsList = new ArrayList<>();
                    for (int i = 0; i < items_list.length(); i++) {
                        Gson gson = new Gson();
                        smsList.add(gson.fromJson(items_list.get(i).toString(), GsonListItems.class));
                    }
                    //     ((ListItemAcitivity)context).initAdapter();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.i("Testt", "" + throwable);
            }
        });


    }

    public static void deleteList(final Context context, final int list_id, final DeleteListListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("id", list_id);
        client.post(baseUrl + "deletelist", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        listener.OnDeleteList();
                        Log.i("DeleteResponse", "response " + response);

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        listener.OnDeleteListFailure();
                        Log.i("DeleteFailure", "" + throwable);

                    }
                }


        );

    }

    public static void deleteListItem(final int item_id, final DeleteItemListner deleteItemListner) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("id", item_id);
        client.post(baseUrl + "deleteitem", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        deleteItemListner.onItemDeleted();
                        Log.i("DeleteResponse", "response " + response);

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);

                        Log.i("DeleteFailure", "" + throwable);
                        deleteItemListner.onItemDeletedFailure();
                    }
                }


        );

    }

    public static void getUserInfo(final BirthdayListener listener)
    {
        AsyncHttpClient client=new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("uid",StaticData.id);

        client.post(baseUrl+"getuserinfo",params,new JsonHttpResponseHandler()
        {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                JSONObject obj;
                try {
                    obj = new JSONObject(String.valueOf(response));
                    obj=obj.getJSONObject("Response");
                    JSONArray array = obj.getJSONArray("data");
                    JSONObject object = (JSONObject) array.get(0);
                    dob=object.getString("dob");
                    Log.i("date","static date"+dob);
                    listener.onSuccess(dob);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                Log.i("user_info",""+String.valueOf(response));

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }
        });
    }
    public static void birthday(final BirthdayListener listener)
    {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(baseUrl+"getbirthdayimage",new JsonHttpResponseHandler()
        {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    JSONObject obj= new JSONObject(String.valueOf(response));
                    obj=obj.getJSONObject("Response");
                    JSONArray array = obj.getJSONArray("data");
                    JSONObject object = (JSONObject) array.get(0);
                    imgurl=object.getString("image");
                    imgurl="http://128.199.199.239/narrator/"+imgurl;

                    Log.i("image",""+imgurl);
                    listener.onSuccess(imgurl);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void check_phone(final String phone_number, final ForgoPasswordListener listener)
    {

        Log.i("Number",""+phone_number);
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("number",phone_number);
        client.post(baseUrl+"checknumber",params,new JsonHttpResponseHandler()
                {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);

                        JSONObject obj;
                        try {
                            obj = new JSONObject(String.valueOf(response));
                            obj=obj.getJSONObject("Response");
                            JSONArray array = obj.getJSONArray("data");
                            JSONObject object = (JSONObject) array.get(0);
                            count=object.getInt("count");
                            listener.OnSuccess(count,0);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.i("numberresponse","Count "+count+"   "+response.toString());
                    }
                }
        );


    }

    public static void forgotpass_1(final String email, final ForgoPasswordListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("email", email);

        Log.i("api_email",params.toString());
        client.post(baseUrl + "forgetpasswordone", params, new JsonHttpResponseHandler() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    JSONObject object = new JSONObject(String.valueOf(response));
                    object = object.getJSONObject("Response");
                    data = object.getInt("data");
                    response_code = object.getInt("Code");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                listener.OnSuccess(data, response_code);

                Log.i("Responseforgot", response.toString());
                Log.i("DATAT", "datatext" + data);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                listener.OnFail(throwable.toString());

            }
        });
    }


    public static void forgotpass_2(final String email, final int code, final ForgoPasswordListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("email", email);
        params.put("code", code);

        Log.i("api_email_2",params.toString());
        client.post(baseUrl + "forgetpasswordtwo", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                /*    Log.i("User res",response.toString());
                    JSONObject respon = response.getJSONObject("Response");
                    JSONObject userObj = (JSONObject) response.getJSONArray("id").get(0);
                    Log.i("User",userObj.toString());
                    userid = userObj.getInt("id");
                    response_code = respon.getInt("Code");*/


                    JSONObject obj = new JSONObject(String.valueOf(response));
                    obj = obj.getJSONObject("Response");
                    JSONArray array = obj.getJSONArray("id");
                    JSONObject object = (JSONObject) array.get(0);
                    userid = object.getInt("id");
                    Log.i("userid_api", "" + userid);
                    response_code = obj.getInt("Code");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                listener.OnSuccess(userid, response_code);

                Log.i("Responseforgot", response.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                listener.OnFail(throwable.toString());

            }
        });
    }

    public static void forgotpass_3(final int uid, final String password, final ForgoPasswordListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("id", uid);
        params.put("password", password);
        Log.i("Password_Api", params.toString());
        client.post(baseUrl + "forgetpasswordthree", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    JSONObject obj = new JSONObject(String.valueOf(response));
                    obj = obj.getJSONObject("Response");
                    response_code = obj.getInt("Code");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                listener.OnSuccess(data, response_code);

                Log.i("Responseforgot", response.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                listener.OnFail(throwable.toString());

            }
        });
    }

    public static void shareList(final int uid,final String phone_number,final int list_id, final ForgoPasswordListener listener)
    {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("send_from",uid);
        params.put("phone",phone_number);
        params.put("list_id",list_id);
        client.post(baseUrl+"sharebyphone",params,new JsonHttpResponseHandler()
                {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);

                        try {
                            JSONObject obj = new JSONObject(String.valueOf(response));
                            obj = obj.getJSONObject("Response");
                            response_code = obj.getInt("Status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        listener.OnSuccess(data,response_code);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        listener.OnFail(throwable.toString());
                    }
                }


        );
    }
    public static void statusList(final int listID, final int UserID, final ForgoPasswordListener listener) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("list_id", listID);
        params.put("user_id", UserID);
        client.post(baseUrl + "changeliststatus", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            Log.d("test check",obj.toString());
                            if(obj.has("Response")){
                                int listStatus = obj.getInt("Response");
                                listener.OnSuccess(0, listStatus);
                            }else {
                                listener.OnSuccess(0, -1);
                            }
                            Log.i("statusresponse", response.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        Log.i("statusreponse", throwable.toString());
                    }
                }


        );
    }
    public static void emailShare(final int uid,final String email,final int list_id, final ForgoPasswordListener listener)
    {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("send_from",uid);
        params.put("email",email);
        params.put("list_id",list_id);
        client.post(baseUrl+"sharebyemail",params,new JsonHttpResponseHandler()
                {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);

                        try {
                            JSONObject obj = new JSONObject(String.valueOf(response));
                            obj = obj.getJSONObject("Response");
                            response_code = obj.getInt("Status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        listener.OnSuccess(data,response_code);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        listener.OnFail(throwable.toString());
                    }
                }


        );
    }

public static void registerToken(String Token)
    {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("tokken",Token);
        params.put("id",StaticData.id);

        Log.i("tokeen","abc"+Token);
    client.post(baseUrl+"inserttokken",params,new JsonHttpResponseHandler()
            {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);

                    Log.i("responseeeee",""+response.toString());

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                }

            }
    );
    }

    public static void copyList(final int list_id, final String listName, final JSONArray array, final CopyListener listener)
    {
        RequestParams params = new RequestParams();
        JSONObject data = new JSONObject();
        try {
            data.put("listid", list_id);
            data.put("save", 1);
            data.put("uid", Integer.parseInt(StaticData.id));
            data.put("list_name", listName);
            data.put("list_item", array);
            Log.i("Json123", "" + array);
            Log.i("Data123", "" + data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        params.put("data2", array);
        params.put("data", data);
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(baseUrl+"copylist",params,new JsonHttpResponseHandler()
                {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);

                        Log.i("responseeeee",""+response.toString());
                        listener.onCopySuccess();

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        listener.onCopyFailed();
                    }

                }
        );
    }

 /*   public static void retrieveCopyItems(final int list_id) {
        items_name_list = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        params.put("listid", list_id);
        Log.i("testing", String.valueOf(params));
        client.post(baseUrl + "getuserlistitems", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.i("Test", "" + response);
                try {
                    JSONObject res = response.getJSONObject("Response");
                    items_list = res.getJSONArray("list_items");
                    for (int i = 0; i < items_list.length(); i++) {
                        Gson gson = new Gson();
                        items_name_list.add(gson.fromJson(items_list.get(i).toString(), GsonListItems.class));

                    }
                    //     ((ListItemAcitivity)context).initAdapter();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.i("Testt", "" + throwable);
            }
        });


    }

*/
    public interface OnAuthListener {
        void onAuthSuccess(Auth auth);

        void OnError(String error);
    }

    public interface OnPhoneVerifyListener {
        void OnPhoneVerificationSuccess(String userPhoneNo);

        void OnPhoneVerificationFailed(String error);
    }

}
