package com.app.notesapp.interfaces;

/**
 * Created by Danish on 8/12/2017.
 */

public interface OnLoginListener {
    void OnSuccessLogin();
    void OnFailLogin();
}
