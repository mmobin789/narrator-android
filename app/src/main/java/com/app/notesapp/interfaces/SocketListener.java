package com.app.notesapp.interfaces;

import com.app.notesapp.models.StrikeItem;
import com.app.notesapp.models.SyncContact;

import java.util.ArrayList;

/**
 * Created by Danish on 8/15/2017.
 */

public interface SocketListener {

    void onConnect(String data);
    void onDisconnect(String data);
    void onMessage(String data);
    void onCrossOff(StrikeItem strikeItem);
    void onPhoneCheck(ArrayList<SyncContact> syncContacts);
}
