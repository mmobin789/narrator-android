package com.app.notesapp.interfaces;

/**
 * Created by Danish on 7/29/2017.
 */

public interface OnInternetListener {
    void OnInternetAvailable(String internetType);
    void OnInternetFail();
}