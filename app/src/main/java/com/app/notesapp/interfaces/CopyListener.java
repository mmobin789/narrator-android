package com.app.notesapp.interfaces;

/**
 * Created by Danish on 9/19/2017.
 */

public interface CopyListener {
    void onCopySuccess();
    void onCopyFailed();
}
