package com.app.notesapp.interfaces;

/**
 * Created by Danish on 8/7/2017.
 */

public interface DeleteListListener {
    void OnDeleteList();
    void OnDeleteListFailure();
}
