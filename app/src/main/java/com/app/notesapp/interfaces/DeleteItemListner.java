package com.app.notesapp.interfaces;

/**
 * Created by Danish on 8/7/2017.
 */

public interface DeleteItemListner {
    void onItemDeleted();
    void onItemDeletedFailure();
}
