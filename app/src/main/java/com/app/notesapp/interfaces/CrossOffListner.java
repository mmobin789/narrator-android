package com.app.notesapp.interfaces;

import com.app.notesapp.models.StrikeItem;

/**
 * Created by Abdul Ghani on 8/18/2017.
 */

public interface CrossOffListner {
    void onCrossOffAction(StrikeItem strikeItem);
}
