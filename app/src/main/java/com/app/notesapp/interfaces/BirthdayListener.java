package com.app.notesapp.interfaces;

/**
 * Created by Danish on 8/17/2017.
 */

public interface BirthdayListener {

    void onSuccess(String url);
    void onFailure(String error);
}
