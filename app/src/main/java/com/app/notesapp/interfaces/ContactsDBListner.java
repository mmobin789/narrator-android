package com.app.notesapp.interfaces;

import com.app.notesapp.models.SyncContact;

import java.util.ArrayList;

/**
 * Created by Abdul Ghani on 9/12/2017.
 */

public interface ContactsDBListner {
    void onDataPopulated(ArrayList<SyncContact> syncContacts);
}
