package com.app.notesapp.interfaces;

import com.app.notesapp.models.SocialUser;

/**
 * Created by TreeHousestudio on 7/23/2017.
 */

public interface OnSocialLoginListener {
    void OnSocialLoginSuccess(SocialUser user);
    void OnSocialLoginError(String error);
}
