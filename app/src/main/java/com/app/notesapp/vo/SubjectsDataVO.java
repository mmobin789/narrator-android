package com.app.notesapp.vo;

import java.util.ArrayList;

public class SubjectsDataVO {



    ArrayList<WordsDataVO> WORDES_LIST;
    String SUBJECT;
    String SAVED_DATE;
    String SENT_BY;
    String SEND_THROUGH;
    String STATUS;

    public SubjectsDataVO(ArrayList<WordsDataVO> WORDES_, String SAVED_DATE_, String SENT_BY_, String SEND_THROUGH_,
                          String STATUS_, String SUBJECT_) {

        this.WORDES_LIST = new ArrayList<>();
        this.WORDES_LIST.addAll(WORDES_);
        this.SAVED_DATE = SAVED_DATE_;
        this.SENT_BY = SENT_BY_;
        this.SEND_THROUGH = SEND_THROUGH_;
        this.STATUS = STATUS_;
        this.SUBJECT = SUBJECT_;
    }

    public ArrayList<WordsDataVO> getWORDES_LIST() {
        return WORDES_LIST;
    }

    public void setWORDES_LIST(ArrayList<WordsDataVO> WORDES_LIST) {
        this.WORDES_LIST = WORDES_LIST;
    }

    public String getSAVED_DATE() {
        return SAVED_DATE;
    }

    public void setSAVED_DATE(String SAVED_DATE) {
        this.SAVED_DATE = SAVED_DATE;
    }

    public String getSENT_BY() {
        return SENT_BY;
    }

    public void setSENT_BY(String SENT_BY) {
        this.SENT_BY = SENT_BY;
    }

    public String getSEND_THROUGH() {
        return SEND_THROUGH;
    }

    public void setSEND_THROUGH(String SEND_THROUGH) {
        this.SEND_THROUGH = SEND_THROUGH;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getSUBJECT() {
        return SUBJECT;
    }

    public void setSUBJECT(String SUBJECT) {
        this.SUBJECT = SUBJECT;
    }
}
