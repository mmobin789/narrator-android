package com.app.notesapp.vo;

public class WordsDataVO {

    String item_name;
    String SAVED_DATE;
    String SENT_BY;
    String SEND_THROUGH;
    String status;

    public WordsDataVO(String STATUS_,String WORDES_) {
        this.item_name= WORDES_;
        this.status = STATUS_;
    }

    public String getlist_item() {
        return item_name;
    }

    public void setlist_item(String list_item) {
        this.item_name = list_item;
    }

    public String getSAVED_DATE() {
        return SAVED_DATE;
    }

    public void setSAVED_DATE(String SAVED_DATE) {
        this.SAVED_DATE = SAVED_DATE;
    }

    public String getSENT_BY() {
        return SENT_BY;
    }

    public void setSENT_BY(String SENT_BY) {
        this.SENT_BY = SENT_BY;
    }

    public String getSEND_THROUGH() {
        return SEND_THROUGH;
    }

    public void setSEND_THROUGH(String SEND_THROUGH) {
        this.SEND_THROUGH = SEND_THROUGH;
    }

    public String getSTATUS() {
        return status;
    }

    public void setSTATUS(String STATUS) {
        this.status = STATUS;
    }

    @Override
    public String toString() {
        return "{" +
                "item_name='" + item_name + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
