package com.app.notesapp.floating_button;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.app.noteapp.R;
import com.app.notesapp.utils.StaticData;

import java.util.ArrayList;


public class PromotedActionsLibrary {

    Context context;

    FrameLayout frameLayout;

   // ImageButton mainImageButton;

    RotateAnimation rotateOpenAnimation;

    RotateAnimation rotateCloseAnimation;

    ArrayList<ImageView> promotedActions;
    ArrayList<ImageView> promotedActions_;

    ObjectAnimator objectAnimator[];
    ObjectAnimator objectAnimator_[];

    private int px, px_;

    private static final int ANIMATION_TIME = 0;

    private boolean isMenuOpened;

    public void setup(Context activityContext, FrameLayout layout) {
        context = activityContext;
        promotedActions = new ArrayList<ImageView>();
        promotedActions_ = new ArrayList<ImageView>();
        frameLayout = layout;
        px = (int) context.getResources().getDimension(R.dimen.dim56dp) + 35;
        px_ = (int) context.getResources().getDimension(R.dimen.dim56dp) + 20;
        openRotation();
        closeRotation();
    }

    
    public ImageView addMainItem(Drawable drawable) {

        ImageView button = (ImageView) LayoutInflater.from(context).inflate(R.layout.main_promoted_action_button, frameLayout, false);

        button.setImageDrawable(drawable);

        /*if (isMenuOpened) {
            closePromotedActions().start();
            isMenuOpened = false;
        } else {*/
            isMenuOpened = true;
            openPromotedActions().start();
            openPromotedActions_().start();
       // }

        /*button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isMenuOpened) {
                    closePromotedActions().start();
                    isMenuOpened = false;
                } else {
                    isMenuOpened = true;
                    openPromotedActions().start();
                }
            }
        });*/

        frameLayout.addView(button);

      //  mainImageButton = button;

        return button;
    }

    public void getItem(int index, int drawable, boolean sideLeft){
        if(sideLeft){
            promotedActions.get(index).setImageResource(drawable);
        }else{
            promotedActions_.get(index).setImageResource(drawable);
        }
    }

    public void resetAllItems(){
        promotedActions.get(0).setImageResource(R.drawable.save_icon);

        if(StaticData.MicCheck==1)
        {
            promotedActions.get(1).setImageResource(R.drawable.mic_icon_small);
        }
        else if(StaticData.MicCheck==2)
        {
            promotedActions.get(1).setImageResource(R.drawable.keyboard_icon);
        }

        promotedActions_.get(0).setImageResource(R.drawable.delete_icon);
        promotedActions_.get(1).setImageResource(R.drawable.send_icon);
    }

    public void addItem(Drawable drawable, View.OnClickListener onClickListener) {

        ImageView button = (ImageView) LayoutInflater.from(context).inflate(R.layout.promoted_action_button, frameLayout, false);
        button.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        button.setImageDrawable(drawable);

        button.setOnClickListener(onClickListener);

        promotedActions.add(button);

        frameLayout.addView(button);

        return;
    }
    public void addItem_(Drawable drawable, View.OnClickListener onClickListener) {

        ImageView button = (ImageView) LayoutInflater.from(context).inflate(R.layout.promoted_action_button, frameLayout, false);
        button.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        button.setImageDrawable(drawable);

        button.setOnClickListener(onClickListener);

        promotedActions_.add(button);

        frameLayout.addView(button);

        return;
    }

    /**
     * Set close animation for promoted actions
     */
    public AnimatorSet closePromotedActions() {

        if (objectAnimator == null){
            objectAnimatorSetup();
        }

        AnimatorSet animation = new AnimatorSet();

        for (int i = 0; i < promotedActions.size(); i++) {

            objectAnimator[i] = setCloseAnimation(promotedActions.get(i), i);
        }

        if (objectAnimator.length == 0) {
            objectAnimator = null;
        }

        animation.playTogether(objectAnimator);
        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
               // mainImageButton.startAnimation(rotateCloseAnimation);
            //    mainImageButton.setClickable(false);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
            //    mainImageButton.setClickable(true);
                hidePromotedActions();
            }

            @Override
            public void onAnimationCancel(Animator animator) {
             //   mainImageButton.setClickable(true);
            }

            @Override
            public void onAnimationRepeat(Animator animator) {}
        });

        return animation;
    }

    public AnimatorSet openPromotedActions() {

        if (objectAnimator == null){
            objectAnimatorSetup();
        }



        AnimatorSet animation = new AnimatorSet();

        for (int i = 0; i < promotedActions.size(); i++) {

            objectAnimator[i] = setOpenAnimation(promotedActions.get(i), i);
        }

        if (objectAnimator.length == 0) {
            objectAnimator = null;
        }

        animation.playTogether(objectAnimator);
        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
               // mainImageButton.startAnimation(rotateOpenAnimation);
            //    mainImageButton.setClickable(false);
                showPromotedActions();
            }

            @Override
            public void onAnimationEnd(Animator animator) {
          //      mainImageButton.setClickable(true);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
           //     mainImageButton.setClickable(true);
            }

            @Override
            public void onAnimationRepeat(Animator animator) {}
        });


        return animation;
    }

    public AnimatorSet openPromotedActions_() {

        if (objectAnimator_ == null){
            objectAnimatorSetup_();
        }



        AnimatorSet animation = new AnimatorSet();

        for (int i = 0; i < promotedActions_.size(); i++) {

            objectAnimator_[i] = setOpenAnimation_(promotedActions_.get(i), i);
        }

        if (objectAnimator_.length == 0) {
            objectAnimator_ = null;
        }

        animation.playTogether(objectAnimator_);
        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
               // mainImageButton.startAnimation(rotateOpenAnimation);
            //    mainImageButton.setClickable(false);
                showPromotedActions_();
            }

            @Override
            public void onAnimationEnd(Animator animator) {
          //      mainImageButton.setClickable(true);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
           //     mainImageButton.setClickable(true);
            }

            @Override
            public void onAnimationRepeat(Animator animator) {}
        });


        return animation;
    }

    private void objectAnimatorSetup() {

      objectAnimator = new ObjectAnimator[promotedActions.size()];
    }
    private void objectAnimatorSetup_() {

     objectAnimator_ = new ObjectAnimator[promotedActions_.size()];
    }


    /**
     * Set close animation for single button
     *
     * @param promotedAction
     * @param position
     * @return objectAnimator
     */
    private ObjectAnimator setCloseAnimation(ImageView promotedAction, int position) {

        ObjectAnimator objectAnimator;

        if(context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {

            objectAnimator = ObjectAnimator.ofFloat(promotedAction, View.TRANSLATION_Y, -px * (promotedActions.size() - position), 0f);
            objectAnimator.setRepeatCount(0);
            objectAnimator.setDuration(ANIMATION_TIME * (promotedActions.size() - position));

        } else {

            objectAnimator = ObjectAnimator.ofFloat(promotedAction, View.TRANSLATION_X, -px * (promotedActions.size() - position), 0f);
            objectAnimator.setRepeatCount(0);
            objectAnimator.setDuration(ANIMATION_TIME * (promotedActions.size() - position));

            /*objectAnimator = ObjectAnimator.ofFloat(promotedAction, View.TRANSLATION_X, -px * (promotedActions.size() - position), 0f);
            objectAnimator.setRepeatCount(0);
            objectAnimator.setDuration(ANIMATION_TIME * (promotedActions.size() - position));*/
        }

        return objectAnimator;
    }

    /**
     * Set open animation for single button
     *
     * @param promotedAction
     * @param position
     * @return objectAnimator
     */
    private ObjectAnimator setOpenAnimation(ImageView promotedAction, int position) {

        ObjectAnimator objectAnimator;

        if(context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {

            objectAnimator = ObjectAnimator.ofFloat(promotedAction, View.TRANSLATION_Y, 0f, -px * (promotedActions.size() - position));
            objectAnimator.setRepeatCount(0);
        objectAnimator.setDuration(ANIMATION_TIME * (promotedActions.size() - position));

        } else {
            objectAnimator = ObjectAnimator.ofFloat(promotedAction, View.TRANSLATION_X, 0f, -px * (promotedActions.size() - position));
            if(position == 0){
                objectAnimator = ObjectAnimator.ofFloat(promotedAction, View.TRANSLATION_X, 0f, -px_ * (promotedActions.size() - position));
            }
            objectAnimator.setRepeatCount(0);
            objectAnimator.setDuration(ANIMATION_TIME * (promotedActions.size() - position));

        }

        return objectAnimator;
    }

    private ObjectAnimator setOpenAnimation_(ImageView promotedAction, int position) {

        ObjectAnimator objectAnimator;

        if(context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {

            objectAnimator = ObjectAnimator.ofFloat(promotedAction, View.TRANSLATION_Y, 0f, -px * (promotedActions_.size() - position));
            objectAnimator.setRepeatCount(0);
            objectAnimator.setDuration(ANIMATION_TIME * (promotedActions_.size() - position));

        } else {

            objectAnimator = ObjectAnimator.ofFloat(promotedAction, View.TRANSLATION_X, 0f, px * (promotedActions_.size() - position));
            if(position == 0){
                objectAnimator = ObjectAnimator.ofFloat(promotedAction, View.TRANSLATION_X, 0f, px_ * (promotedActions_.size() - position));
            }

            objectAnimator.setRepeatCount(0);
            objectAnimator.setDuration(ANIMATION_TIME * (promotedActions_.size() - position));

        /*  objectAnimator = ObjectAnimator.ofFloat(promotedAction, View.TRANSLATION_X, 0f, -px * (promotedActions.size() - position));
            objectAnimator.setRepeatCount(0);
            objectAnimator.setDuration(ANIMATION_TIME * (promotedActions.size() - position));*/
        }

        return objectAnimator;
    }

    private void hidePromotedActions() {

        for (int i = 0; i < promotedActions.size(); i++) {
            promotedActions.get(i).setVisibility(View.GONE);
        }
    }

    private void showPromotedActions() {

        for (int i = 0; i < promotedActions.size(); i++) {
            promotedActions.get(i).setVisibility(View.VISIBLE);
        }
    }

    private void showPromotedActions_() {

        for (int i = 0; i < promotedActions_.size(); i++) {
            promotedActions_.get(i).setVisibility(View.VISIBLE);
        }
    }

    private void openRotation() {

        rotateOpenAnimation = new RotateAnimation(0, 45, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        rotateOpenAnimation.setFillAfter(true);
        rotateOpenAnimation.setFillEnabled(true);
        rotateOpenAnimation.setDuration(ANIMATION_TIME);
    }

    private void closeRotation() {

        rotateCloseAnimation = new RotateAnimation(45, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateCloseAnimation.setFillAfter(true);
        rotateCloseAnimation.setFillEnabled(true);
        rotateCloseAnimation.setDuration(ANIMATION_TIME);
    }
}
