package com.app.notesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Danish on 8/3/2017.
 */

public class GsonListItems implements Serializable {

    private int id;
    @SerializedName("listid")
    private int listId;
    @SerializedName("item_name")
    private String itemName;
    private int status;
    @SerializedName("crossoffby")
    private int crossOffBy;

    public GsonListItems() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCrossOffBy() {
        return crossOffBy;
    }

    public void setCrossOffBy(int crossOffBy) {
        this.crossOffBy = crossOffBy;
    }
}
