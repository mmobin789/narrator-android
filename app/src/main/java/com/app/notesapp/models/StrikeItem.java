package com.app.notesapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Abdul Ghani on 8/17/2017.
 */
public class StrikeItem {
    @SerializedName("msg")
    private int message;
    @SerializedName("listid")
    private int listId;
    @SerializedName("itemid")
    private int itemId;
    @SerializedName("crossoffby")
    private int crossOffBy;

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getCrossOffBy() {
        return crossOffBy;
    }

    public void setCrossOffBy(int crossOffBy) {
        this.crossOffBy = crossOffBy;
    }

    @Override
    public String toString() {
        return "StrikeItem{" +
                "message=" + message +
                ", listId=" + listId +
                ", itemId=" + itemId +
                ", crossOffBy=" + crossOffBy +
                '}';
    }
}
