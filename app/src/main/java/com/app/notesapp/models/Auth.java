package com.app.notesapp.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.app.notesapp.utils.StaticData;
import com.google.gson.annotations.SerializedName;

/**
 * Created by TreeHousestudio on 7/21/2017.
 */

public class Auth {
    @SerializedName("Response")
    public Response response;

    public class Response {
        public int data;
        public String Status;
        public int Code;
        @SerializedName("Userdata")
        public User user;

    }

    public static boolean hasLogin(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        StaticData.name = preferences.getString(StaticData.key_name, "");
        StaticData.id = preferences.getString(StaticData.key_uid, null);
        boolean flag;
        flag = !(TextUtils.isEmpty(StaticData.id) || TextUtils.isEmpty(StaticData.name));
        return flag;
    }

   public static void clearLogin(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear().apply();
    }

    public static void saveUser(Context context, User user) {
        String name, id;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        if (user != null) {
            name = user.name;
            id = StaticData.id;

        } else {
            name = User.socialUser.first_name;
            id = User.socialUser.id;
        }

        editor.putString(StaticData.key_name, name);
        editor.putString(StaticData.key_uid, id);
        editor.apply();

        hasLogin(context);


    }

}
