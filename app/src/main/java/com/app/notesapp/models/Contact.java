package com.app.notesapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Abdul Ghani on 8/18/2017.
 */

public class Contact {
    @SerializedName("username")
    private String name;
    @SerializedName("phone")
    private String number;

    @SerializedName("email")
    private String email;
    public Contact() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Contact(String name, String number, String email) {
        this.name = name;
        this.number = number;
        this.email=email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
