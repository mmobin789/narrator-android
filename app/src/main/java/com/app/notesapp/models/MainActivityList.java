package com.app.notesapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Danish on 8/2/2017.
 */

public class MainActivityList {

    int id;
    int save;
    @SerializedName("uid")
    int userId;
    @SerializedName("list_name")
    String listName;
    String message_count, time_stamp;
    @SerializedName("status")
    int listStatus = -1; // 0/1 may not then -1 (seen or not)
    @SerializedName("shared_method")
    int sharedCount = -1; // if 1 then shared by phone if 0 then shared by email
    @SerializedName("phone_count")
    int phoneCount = -1; // no of sahre by phone
    @SerializedName("email_count")
    int emailCount = -1; // no of share by email
    @SerializedName("time")
    String listCreationTime = "";
    @SerializedName("email")
    String shareEmail = ""; //email id of creator
    @SerializedName("crossoff")
    int crossOffStatus=-1;
    int isOptionLayout = -1;

    @SerializedName("seen")
    int Seen=0;
    public int getCrossOffStatus() {
        return crossOffStatus;
    }

    public int getSeen() {
        return Seen;
    }

    public void setSeen(int seen) {
        Seen = seen;
    }

    public void setCrossOffStatus(int crossOffStatus) {
        this.crossOffStatus = crossOffStatus;
    }

    public int getIsOptionLayout() {
        return isOptionLayout;
    }

    public void setIsOptionLayout(int isOptionLayout) {
        this.isOptionLayout = isOptionLayout;
    }

    public String getShareEmail() {
        return shareEmail;
    }

    public void setShareEmail(String shareEmail) {
        this.shareEmail = shareEmail;
    }

    public MainActivityList() {
    }

    public String getListCreationTime() {
        return listCreationTime;
    }

    public void setListCreationTime(String listCreationTime) {
        this.listCreationTime = listCreationTime;
    }

    public int getPhoneCount() {
        return phoneCount;
    }

    public void setPhoneCount(int phoneCount) {
        this.phoneCount = phoneCount;
    }

    public int getEmailCount() {
        return emailCount;
    }

    public void setEmailCount(int emailCount) {
        this.emailCount = emailCount;
    }

    public String getMessage_count() {
        return message_count;
    }

    public void setMessage_count(String message_count) {
        this.message_count = message_count;
    }

    public String getTime_stamp() {
        return time_stamp;
    }

    public void setTime_stamp(String time_stamp) {
        this.time_stamp = time_stamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSave() {
        return save;
    }

    public void setSave(int save) {
        this.save = save;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }


    public int getSharedCount() {
        return sharedCount;
    }

    public void setSharedCount(int sharedCount) {
        this.sharedCount = sharedCount;
    }

    public int getListStatus() {
        return listStatus;
    }

    public void setListStatus(int listStatus) {
        this.listStatus = listStatus;
    }

    @Override
    public String toString() {
        return "MainActivityList{" +
                "id=" + id +
                ", save=" + save +
                ", userId=" + userId +
                ", listName='" + listName + '\'' +
                ", message_count='" + message_count + '\'' +
                ", time_stamp='" + time_stamp + '\'' +
                '}';
    }

}
