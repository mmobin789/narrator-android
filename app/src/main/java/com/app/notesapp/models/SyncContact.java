package com.app.notesapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Abdul Ghani on 8/23/2017.
 */

public class SyncContact {
    @SerializedName("phone")
    private String number;
    @SerializedName("status")
    private int status;
    @SerializedName("username")
    private String name="";
    @SerializedName("email")
    private String email="";



//    public SyncContact(String name,int status,String email) {
//        this.status = status;
//        this.name = name;
//        this.email=email;
//    }

    public SyncContact(String name, String number, int status,String email) {
        this.number = number;
        this.status = status;
        this.name = name;
        this.email=email;

    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SyncContact{" +
                "number='" + number + '\'' +
                ", status=" + status +
                ", name='" + name + '\'' +
                '}';
    }
}
