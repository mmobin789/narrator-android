package com.app.notesapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TreeHousestudio on 7/21/2017.
 */

public class User {
    public String email, password, phone="", avatar, role, name,dob;

    public int Status;
    @SerializedName("insertId")
    public String id;

    public static SocialUser socialUser;

    public User(String password, String phone) {
        this.password = password;
        this.phone = phone;
    }

    public User() {
    }
}
