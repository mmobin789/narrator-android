package com.app.notesapp.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.noteapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Danish on 7/24/2017.
 */

@RequiresApi(api = Build.VERSION_CODES.M)
public class PagerFragment extends Fragment implements View.OnScrollChangeListener{
    public ImageView img;
    public  static List<ImageView> list=new ArrayList<ImageView>();
 static PagerFragment fragment;
    private static final String ARG_PAGE_NUMBER = "pageNumber";
  public static int  page;
    public static PagerFragment create(int pageNumber) {
        fragment = new PagerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_PAGE_NUMBER, pageNumber);
        fragment.setArguments(bundle);
        Log.i("vii",""+pageNumber);
        return fragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pager, container, false);
        img= rootView.findViewById(R.id.img1);
        final Bundle arguments = getArguments();
        page = arguments.getInt(ARG_PAGE_NUMBER,-1);
        Log.i("page",""+page);

        if(page==0)
        {

            img.setImageResource(R.drawable.ic_launcher);
            list.add(page,img);

        }

        else if(page==1)
        {
            img.setImageResource(R.drawable.user_image);
            list.add(page,img);
        }

        else if(page==2)
        {
            img.setImageResource(R.drawable.audi1);
            list.add(page,img);
        }

        else if(page==3)
        {
            img.setImageResource(R.drawable.audi2);
            list.add(page,img);
        }

        else if(page==4)
        {
            img.setImageResource(R.drawable.bag);
            list.add(page,img);
        }

        else if(page==5)
        {
            img.setImageResource(R.drawable.ic_launcher);
            list.add(page,img);
        }
        else if(page==6)
        {
            img.setImageResource(R.drawable.user_image);
            list.add(page,img);
        }

        img.setOnScrollChangeListener(this);
        return rootView;

    }

    public void foo(int page)
    {

    }
    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

        Log.i("pagee1",""+v.getVerticalScrollbarPosition());
        Log.i("pagee2","12"+scrollX);
        Log.i("pagee4","12"+oldScrollX);
    }
}