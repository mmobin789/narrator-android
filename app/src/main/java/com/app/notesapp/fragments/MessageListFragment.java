package com.app.notesapp.fragments;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.noteapp.R;
import com.app.notesapp.utils.StaticData;
import com.app.notesapp.vo.SubjectsDataVO;

import java.util.ArrayList;
import java.util.HashMap;

public class MessageListFragment extends Fragment {

    IndexMiddleAdapter indexMiddleAdapter;
    ListView wordslistLV;
    ArrayList<String> tempSubjectsKeys;


    public MessageListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_message_list, container, false);

        wordslistLV = rootView.findViewById(R.id.wordslistLV);

        populateList(StaticData.SAVED_LETTERS_DATA.SAVED_WORDES);

        return rootView;
    }


    private void populateList(HashMap<String, SubjectsDataVO> list) {

        if (list != null && list.size() > 0) {
            tempSubjectsKeys = new ArrayList<>();
            for (String key : list.keySet()) {
                tempSubjectsKeys.add(key);
            }

            indexMiddleAdapter = new IndexMiddleAdapter(getActivity(), list, tempSubjectsKeys);
            wordslistLV.setAdapter(indexMiddleAdapter);
        } /*else {
            ArrayAdapter<String> emptyCase = new ArrayAdapter<String>(getActivity(), R.layout.list_items_index, R.id.textTV, new String[]{"Press and speak to add"});
            wordslistLV.setAdapter(emptyCase);
        }*/
    }


    public class IndexMiddleAdapter extends BaseAdapter {

        Context context;
        LayoutInflater inflater;
        ArrayList<String> tempSujectList;
        HashMap<String, SubjectsDataVO> subjectDataVOArrayList;

        public IndexMiddleAdapter(Context context, HashMap<String, SubjectsDataVO> subjectDataVOArrayList_, ArrayList<String> tempSujectList_) {
            this.context = context;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.subjectDataVOArrayList = new HashMap<>();
            this.tempSujectList = new ArrayList<>();
            this.subjectDataVOArrayList.putAll(subjectDataVOArrayList_);
            this.tempSujectList.addAll(tempSujectList_);
        }

        @Override
        public int getCount() {
            return tempSujectList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final Holder holder;
            View view = convertView;
            if (view == null) {
                view = inflater.inflate(R.layout.main_list_items_index, null);
                holder = new Holder();
                holder.p_NameTV = view.findViewById(R.id.textTV);
                view.setTag(holder);
            } else {
                holder = (Holder) view.getTag();
            }

            holder.p_NameTV.setText(getStringName(subjectDataVOArrayList.get(tempSujectList.get(position)).getSUBJECT()).toUpperCase());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showProgressWheel(context);
                  //  startActivity(new Intent(context, PopupActivity.class));
                    Log.e("ProductCardsAdapter", "" + position);
                }
            });

            return view;
        }

        class Holder {
            TextView p_NameTV;
        }

        private String getStringName(String text) {
            if (text.length() > 20) {
                String text_ = text.substring(0, 18);
                text = text_ + "..";
            }
            return text;
        }

        private void showProgressWheel(Context mContext) {
            final Dialog dialog = new Dialog(mContext, R.style.Theme_UserDialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.option_popup);
            dialog.setCancelable(true);

            LinearLayout messageLL = dialog.findViewById(R.id.messageLL);
            LinearLayout sendLL = dialog.findViewById(R.id.sendLL);
            LinearLayout deleteLL = dialog.findViewById(R.id.deleteLL);

            messageLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "MESSAGE BUTTON", Toast.LENGTH_LONG).show();
                }
            });
            sendLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "SEND BUTTON", Toast.LENGTH_LONG).show();
                }
            });
            deleteLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "DELETE BUTTON", Toast.LENGTH_LONG).show();
                }
            });
            dialog.show();

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        populateList(StaticData.SAVED_LETTERS_DATA.SAVED_WORDES);
    }
}
