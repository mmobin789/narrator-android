package com.app.notesapp.Sockets;


import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.app.notesapp.interfaces.SocketListener;
import com.app.notesapp.models.Contact;
import com.app.notesapp.models.StrikeItem;
import com.app.notesapp.models.SyncContact;
import com.app.notesapp.utils.ContactsDB;
import com.app.notesapp.utils.StaticData;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by Danish on 8/15/2017.
 */

public class ServerSocket {

    private static Socket socket = null;
    private static final String serverUri = "http://138.197.100.210:3002";
    private static final String LOGIN_TO_CONNECT = "logintoconnect";
    private static final String MESSAGE = "message";
    private static final String CROSS_OFF = "crossoff";
    private static final String CHECK_CONTACT = "contactcheck";
    private static final String CROSS_OF_LIST = "crossoff-list";
    private static final String PHONE_CHECK = "phonecheck";
    private static final String uId = "uid";
    private static final String listNumber = "listnumber";
    private static final String itemNumber = "itemnumber";
    private static final String status = "status";
    public static final String TAG = "ServerSocket";
    private static Context context;
    private Gson gson;
    private SocketListener socketListner;
    private String userId;

    public ServerSocket(Context context, String userId, SocketListener listener) {
        ServerSocket.context = context;
        if (socket == null) {
            try {
                socket = IO.socket(serverUri);
                this.userId = userId;
                this.socketListner = listener;
                gson = new Gson();
                setSocketListner();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }


    }

    public void connect() {
        socket.connect();
    }

    public void disConnect() {
        socket.disconnect();
    }

    public boolean isConnected() {
        return socket.connected();
    }

    private void setSocketListner() {
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                socketListner.onConnect(gson.toJson(args));
                emitDataonConnect();
            }
        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                socketListner.onDisconnect(gson.toJson(args));
            }
        }).on(ServerSocket.MESSAGE, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                socketListner.onMessage(gson.toJson(args));
            }
        }).on(ServerSocket.CROSS_OF_LIST, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject jsonObject = new JSONObject(gson.toJson(args).replace("[", "").replace("]", ""));
                    JSONObject namePairValues = jsonObject.getJSONObject("nameValuePairs");
                    Log.d("Crossof", namePairValues.toString());
                    Gson gson = new Gson();
                    StrikeItem strikeItem = gson.fromJson(namePairValues.toString(), StrikeItem.class);
                    socketListner.onCrossOff(strikeItem);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).on(ServerSocket.PHONE_CHECK, new Emitter.Listener() {
            @Override
            public void call(Object... args) {

                Log.i("emitterworking"," abc");
                try {
                    String jsonString = gson.toJson(args);
                    Log.i("contacts", jsonString);
                    JSONObject jsonObject = new JSONObject(jsonString.substring(1, jsonString.length() - 1));
                    JSONArray valuesArray = jsonObject.getJSONArray("values");
                    ArrayList<SyncContact> syncContacts = new ArrayList<>();
                    for (int i = 0; i < valuesArray.length(); i++) {
                        JSONObject json = (JSONObject) valuesArray.get(i);
                        syncContacts.add(gson.fromJson(json.getJSONObject("nameValuePairs").toString(), SyncContact.class));
                    }
                    saveContactsToDb(syncContacts);
                    socketListner.onPhoneCheck(syncContacts);
                } catch (Exception e) {
                    Log.i("gsonexception"," "+e.toString());
                }
            }

        });
    }

    private void emitDataonConnect() {
        try {
            JSONObject object = new JSONObject();
            object.put(uId, Integer.parseInt(userId));
            socket.emit(ServerSocket.LOGIN_TO_CONNECT, object);
        } catch (JSONException e) {
            Log.i(TAG, e.toString());
        }
    }

    public void listToCrossOf(String id, String listNumber, String itemNumber, int crossOffAction) {
        try {
            JSONObject object = new JSONObject();
            object.put(ServerSocket.uId, id);
            object.put(ServerSocket.listNumber, listNumber);
            object.put(ServerSocket.itemNumber, itemNumber);
            object.put(ServerSocket.status, crossOffAction);
            Log.d("strike cross", object.toString());
            socket.emit(ServerSocket.CROSS_OFF, object);
        } catch (JSONException e) {
            Log.i(TAG, e.toString());
        }
    }

  /*  public void syncContacts(JSONObject jsonObject) {
        socket.emit(ServerSocket.CHECK_CONTACT,jsonObject);
    }*/
  public void syncContacts(JSONObject jsonObject) {
      socket.io().timeout(100000);
      socket.emit(ServerSocket.CHECK_CONTACT,jsonObject);
  }
    private void saveContactsToDb(ArrayList<SyncContact> syncContacts) {
        if (syncContacts != null) {
            if (syncContacts.size() > 0) {
                ContactsDB contactsDB = new ContactsDB(context);
                contactsDB.setContacts(syncContacts);
            }
            Log.i("contactsDb2","ac"+syncContacts.size());
        }
        Log.i("contactsDb","ac"+syncContacts.size());
    }

    // send contacts to server for checking

  /* private static ArrayList<Contact> getContacts() {

        Cursor cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
       // Cursor cursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.Contacts.DISPLAY_NAME + " ASC");
       Cursor emails = context.getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID, null, null);

       ArrayList<Contact> contacts = null;
        HashMap<String,Contact> filterContacts = null;
        if (cursor != null) {
            cursor.moveToFirst();
            contacts = new ArrayList<>();
            filterContacts = new HashMap<>();
            while (emails.moveToNext()) {
                String email = emails.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                Log.i("emailaddress3", "" + email);

                while (cursor.moveToNext()) {
                    String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String phonenumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    //String email = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));

                    Log.i("emailaddress", "" + email);
//                contacts.add(new Contact(name, phonenumber));
                    filterContacts.put(phonenumber, new Contact(name, phonenumber, email));
                    if (filterContacts.size() == 300) {
                        break;
                    }
                }
            }
            for (Contact contact: filterContacts.values()) {
               contacts.add(contact);
            }
            cursor.close();

        }
        return contacts;
    }
*/

   private static ArrayList<Contact> getContacts() {
    //    Cursor cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        // Cursor cursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.Contacts.DISPLAY_NAME + " ASC");
     //   Cursor emails = context.getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID, null, null);
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        ArrayList<Contact> contacts = null;
        HashMap<String,Contact> filterContacts = null;
        if (cursor != null&&cursor.moveToFirst()) {

            contacts = new ArrayList<>();
            filterContacts = new HashMap<>();
            do {
                // get the contact's information
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                Integer hasPhone = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                // get the user's email address
                String email = null;
                Cursor ce = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                if (ce != null && ce.moveToNext()) {
                    email = ce.getString(ce.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    ce.close();
                }

                Log.i("Email123"," "+email);
                // get the user's phone number
                String phone = null;

                Cursor cp = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                if (cp != null && cp.moveToNext()) {
                    phone = cp.getString(cp.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    cp.close();

                }

                // if the user user has an email or phone then add it to contacts
                if(email==null)
                {
                    email="No Email";
                }
                filterContacts.put(phone, new Contact(name, phone, email));
                if (filterContacts.size() == 180) {
                    break;
                }


            } while (cursor.moveToNext());

            for (Contact contact: filterContacts.values()) {
                contacts.add(contact);
            }
            cursor.close();

        }
        return contacts;
    }



 /*   private static ArrayList<Contact> getContacts() {
        ArrayList<Contact> contacts = new ArrayList<>();
        HashMap<String,Contact> filterContacts = new HashMap<>();
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {

            do {
                // get the contact's information
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                Integer hasPhone = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                // get the user's email address
                String email = null;
                Cursor ce = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                if (ce != null && ce.moveToNext()) {
                    email = ce.getString(ce.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    ce.close();
                }

                Log.i("Email123"," "+email);
                // get the user's phone number
                String phone = null;

                    Cursor cp = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    if (cp != null && cp.moveToNext()) {
                        phone = cp.getString(cp.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        cp.close();

                }

                // if the user user has an email or phone then add it to contacts
                if(email==null)
                {
                    email="No Email";
                }
                filterContacts.put(phone, new Contact(name, phone, email));
              if (filterContacts.size() == 300) {
                    break;
                }


            } while (cursor.moveToNext());


            cursor.close();
        }
        for (Contact contact: filterContacts.values()) {
            contacts.add(contact);
        }
        return contacts;
    }
*/
    /*
 private static ArrayList<Contact> getContacts()
    {
        ArrayList<Contact> contacts = null;
        HashMap<String,Contact> filterContacts = null;
        String displayName = null,emailAddress = null,phoneNumber=null;
        ContentResolver cr =context.getContentResolver();
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null,null);

        if(cursor!=null) {
            cursor.moveToFirst();
            contacts = new ArrayList<>();
            filterContacts = new HashMap<>();
            displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            Cursor emails = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + id, null, null);

            emails.moveToFirst();
            while (cursor.moveToNext()) {

              while (emails.moveToNext()) {
                    emailAddress = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    Log.i("emailaddress2",""+emailAddress);
                    break;
                }

                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        phoneNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        break;
                    }
                    pCur.close();
                }
                filterContacts.put(phoneNumber, new Contact(displayName, phoneNumber, emailAddress));
                if (filterContacts.size() == 300) {
                    break;
                }
                contacts.add(new Contact(displayName, phoneNumber, emailAddress));
            }

            */
/*for (Contact contact: filterContacts.values()) {

                Log.i("emailaddress",""+filterContacts);
                contacts.add(contact);
            }*//*

            emails.close();
            cursor.close();
        }
        return contacts;
    }

*/


    private static JSONObject getJsonFromContacts(ArrayList<Contact> contacts) {
        JSONObject contactJson = new JSONObject();
        Gson gson = new Gson();
        try {
            contactJson.put("uid", StaticData.id);
            contactJson.put("number", new JSONArray(gson.toJson(contacts)));
          //  contactJson.put("email",new JSONArray(gson.toJson(contacts)));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("contactJson","abc"+contactJson.toString());
        return contactJson;
    }

    public static void sendContactsToServer() {
        if (SocketService.getServerSocket() != null) {
            if (SocketService.getServerSocket().isConnected()) {
                SocketService.getServerSocket().syncContacts(getJsonFromContacts(getContacts()));

            }
        }
    }

}
