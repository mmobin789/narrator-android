package com.app.notesapp.Sockets;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.app.noteapp.R;
import com.app.notesapp.activities.SplashActivity;
import com.app.notesapp.interfaces.SocketListener;
import com.app.notesapp.models.StrikeItem;
import com.app.notesapp.models.SyncContact;
import com.app.notesapp.utils.StoragePreference;

import java.util.ArrayList;


/**
 * Created by Abdul Ghani on 8/18/2017.
 */

public class SocketService extends Service implements SocketListener {

    private static ServerSocket serverSocket;
    public static final String TAG = "SocketService";
    private static Context context;
    private static SocketListener listenerS;
    private static String userId;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public SocketService() {
    }

    public static void setSocketListener(SocketListener listener, Context context) {
        SocketService.context = context;
        listenerS = listener;
    }

    public static void removeSocketListner() {
        listenerS = null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //TODO do something useful

        userId = StoragePreference.getUserId(this);
        serverSocket = new ServerSocket(context,userId, this);
        serverSocket.connect();
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        serverSocket.disConnect();
        Log.d(TAG, "Destroy");
    }

    @Override
    public IBinder onBind(Intent intent) {
        //TODO for communication return IBinder implementation
        return null;
    }

    @Override
    public void onConnect(String data) {
        if (listenerS != null)
            listenerS.onConnect(data);
        Log.i(TAG, "onConnect " + data);
    }

    @Override
    public void onDisconnect(String data) {
        if (listenerS != null)
            listenerS.onDisconnect(data);
        Log.i(TAG, "onDisconnect " + data);
    }

    @Override
    public void onMessage(String data) {
        if (listenerS != null)
            listenerS.onMessage(data);
        Log.i(TAG, "onMessage " + data);
    }

    @Override
    public void onCrossOff(StrikeItem strikeItem) {
        if (listenerS != null)
            listenerS.onCrossOff(strikeItem);
        Log.i(TAG, "onCrossOff " + strikeItem.toString());
        notifiyCrossOff(strikeItem);
    }

    @Override
    public void onPhoneCheck(ArrayList<SyncContact> syncContacts) {
        if (listenerS != null)
            listenerS.onPhoneCheck(syncContacts);
        Log.i(TAG, "onPhoneCheck " + syncContacts);
    }

    public static ServerSocket getServerSocket() {
        return serverSocket;
    }

    void notifiyCrossOff(StrikeItem strikeItem) {
        String crossString;
        if(strikeItem!=null) {
            if(strikeItem.getCrossOffBy() != Integer.parseInt(userId)){
                if (strikeItem.getMessage() == 1) {
                    crossString = "List Cross Off";
                } else {
                    crossString = "List Un-Cross";
                }
                sendNotification(crossString);
            }
        }
    }

    private void sendNotification(String messageBody) {

        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setVibrate(new long[] { 1000, 100, 1000, 100})
                .setLights(Color.BLUE, 500, 500)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
