package com.app.notesapp.enums;

/**
 * Created by TreeHousestudio on 7/24/2017.
 */

public enum AuthType {
    REGISTER, LOGIN, SOCIAL
}
