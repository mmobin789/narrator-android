package com.app.notesapp.Recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.app.notesapp.interfaces.OnInternetListener;

/**
 * Created by Danish on 7/29/2017.
 */

public class InternetMonitor extends BroadcastReceiver {
    OnInternetListener listener;

    public InternetMonitor() {
    }

    public InternetMonitor(OnInternetListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetInfo != null) {
            if (listener != null)
                listener.OnInternetAvailable(activeNetInfo.getTypeName());
            // Toast.makeText(context, "Active Network Type : " + activeNetInfo.getTypeName(), Toast.LENGTH_SHORT).show();
        } else {
            if (listener != null)
                listener.OnInternetFail();
        }

    }
}