package com.app.notesapp.Recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by Danish on 8/19/2017.
 */

public class IncomingMessage extends BroadcastReceiver {

    final SmsManager sms = SmsManager.getDefault();

    public void onReceive(Context context, Intent intent) {

        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[0]);
               StringBuilder message=new StringBuilder();
                String phoneNumber = currentMessage.getOriginatingAddress();

                String senderNum = phoneNumber;
                for (int i = 0; i < pdusObj.length; i++) {




                    currentMessage=SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    message.append(currentMessage.getMessageBody());
                   // message = message.substring(0, message.length()-1);

                } // end for loop
                Log.i("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);
                Intent myIntent = new Intent("otp");
                myIntent.putExtra("message",message.toString());
                LocalBroadcastManager.getInstance(context).sendBroadcast(myIntent);
            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);

        }
    }
}
